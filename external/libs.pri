INCLUDEPATH += $$PWD

# resolve dependencies

contains(CONFIG, lib-common) {
    CONFIG *= mstream-lib pnp-server mlinkip
}

contains(CONFIG, mstream-lib) {
    CONFIG *= metric-sender
}

contains(CONFIG, mlinkip) {
    CONFIG *= device-discover
}

contains(CONFIG, device-discover) {
    CONFIG *= multicast-listener
}

contains(CONFIG, qxw) {
    CONFIG *= qwt
}

# includes

contains(CONFIG, lib-common) {
    INCLUDEPATH *= $$PWD/lib-common
    include($$PWD/lib-common/lib-common.pri)
}

contains(CONFIG, device-discover) {
    INCLUDEPATH *= $$PWD/device-discover
    include($$PWD/device-discover/device-discover.pri)
}

contains(CONFIG, flash-prog) {
    INCLUDEPATH *= $$PWD/flash-prog
    include($$PWD/flash-prog/flash-prog.pri)
}

contains(CONFIG, mlinkip) {
    INCLUDEPATH *= $$PWD/mlinkip/src
    include($$PWD/mlinkip/src/mlinkip.pri)
}

contains(CONFIG, metric-sender) {
    INCLUDEPATH *= $$PWD/metric-sender
    include($$PWD/metric-sender/metric-sender.pri)
}

contains(CONFIG, mongo) {
    INCLUDEPATH *= $$PWD/mongo
    include($$PWD/mongo/mongo.pri)
}

contains(CONFIG, mstream-lib) {
    INCLUDEPATH *= $$PWD/mstream-lib
    include($$PWD/mstream-lib/mstream-lib.pri)
}

contains(CONFIG, multicast-listener) {
    INCLUDEPATH *= $$PWD/multicast-listener
    include($$PWD/multicast-listener/multicast-listener.pri)
}

contains(CONFIG, pnp-server) {
    QT *= xml
    INCLUDEPATH *= $$PWD/pnp-server
    include($$PWD/pnp-server/pnp-server.pri)
}

contains(CONFIG, remote-control-server) {
    INCLUDEPATH *= $$PWD/remote-control-server
    include($$PWD/remote-control-server/remote-control-server.pri)
}

contains(CONFIG, qwt) {
    INCLUDEPATH *= $$PWD/qwt5/include
    include($$PWD/qwt5/qwt_inc.pri)
}

contains(CONFIG, qxw) {
    INCLUDEPATH *= $$PWD/qxw/src
    include($$PWD/qxw/src/qxw_inc.pri)
}
