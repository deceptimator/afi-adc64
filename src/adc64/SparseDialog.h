#ifndef SPARSEDIALOG_H
#define SPARSEDIALOG_H

#include <QDialog>

#include "Config.h"
#include "mldiscover/DeviceIndex.h"

namespace Ui {
class SparseDialog;
}

class SparseDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SparseDialog(Config * Config, QWidget *parent = nullptr);
    ~SparseDialog() override;
    void setOffsetMaximum(const DeviceIndex &index, quint16 max);
    void blockOffset(const DeviceIndex &index, bool blockOffset);

Q_SIGNALS:
    void setupChanged();

private Q_SLOTS:
    void onDeviceChange(const DeviceIndex &di);
    void on_checkBoxEnable_toggled(bool checked);
    void on_spinBoxReadCellNum_valueChanged(int arg1);
    void on_spinBoxOffset_valueChanged(int arg1);
    void on_spinBoxPeriod_valueChanged(int arg1);
    void on_radioButtonOpt1_toggled(bool checked);
    void on_radioButtonOpt2_toggled(bool checked);
    void on_radioButtonOpt3_toggled(bool checked);
    void on_radioButtonOpt4_toggled(bool checked);

private:
    void restoreFormSettings();
    void updateEnabling();
    void block_gui_signals(bool block);

    Ui::SparseDialog * const ui;
    QList<QWidget*> block_signal_widgets_list;
    Config * const config;
    DeviceIndex selectedDevice;
    QMap<DeviceIndex, quint16> devMaxSampleCnt;
    QSet<DeviceIndex> devBlockOffset;
};

#endif // SPARSEDIALOG_H
