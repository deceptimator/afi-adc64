//
//    Copyright 2011 Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "EventData.h"

#include <QDebug>
#include <QStringList>

QString EventData::getEventNumberStr() const
{
    if (isEmpty())
        return QString();

    if (eventNumbersMatch())
        return QString("%1").arg(constBegin()->EventNumber);

    QStringList sl;
    Q_FOREACH(const DominoData &dominoData, *this) {
        sl.push_back(QString("%1").arg(dominoData.EventNumber));
    }
    return sl.join(", ");
}

bool EventData::eventNumbersMatch() const
{
    if (isEmpty())
        return true;
    QMap<quint64, int> eventNumberMap;
    Q_FOREACH(const DominoData &r, *this) {
        eventNumberMap[r.serial_id] = r.EventNumber;
    }
    // check if all event numbers match
    int eventNumber = constBegin()->EventNumber;
    bool match = true;
    Q_FOREACH(int n, eventNumberMap) {
        if (n != eventNumber) {
            match = false;
            break;
        }
    }
    return match;
}

bool EventData::timestampTrigMatch() const
{
    if (isEmpty())
        return true;
    QMap<quint64, int> timestampMap;
    Q_FOREACH(const DominoData &r, *this) {
        timestampMap[r.serial_id] = r.trig_ts;
    }
    // check if all event numbers match
    int trig_ts = constBegin()->trig_ts;
    bool match = true;
    Q_FOREACH(int n, timestampMap) {
        if (n != trig_ts) {
            match = false;
            break;
        }
    }
    if (!match)
        qDebug() << "TimestampTrig: " << timestampMap;
    return match;
}
