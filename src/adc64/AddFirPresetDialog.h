#ifndef ADDFIRPRESETDIALOG_H
#define ADDFIRPRESETDIALOG_H

#include <QDialog>

#include "Config.h"

namespace Ui {
class AddFirPresetDialog;
}

class AddFirPresetDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddFirPresetDialog(Config *config, QWidget *parent = nullptr);
    ~AddFirPresetDialog() override;
    QVector<qint32> getFir() const;

private slots:
    void on_spinBox_valueChanged(int num);
    void on_lineEdit_textChanged(const QString &arg1);

    void on_button_box_accepted();

private:
    bool check_input_validity(const QString& check_this);

    Ui::AddFirPresetDialog * const ui;
    Config *const config;
    QString preset_name;
    int coef_count;
};

#endif // ADDFIRPRESETDIALOG_H
