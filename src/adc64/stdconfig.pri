CONFIG (debug, debug|release) {
    unix:OBJECTS_DIR = .obj-debug
    win32:OBJECTS_DIR = _obj-debug
} else {
    unix:OBJECTS_DIR = .obj-release
    win32:OBJECTS_DIR = _obj-release
}

unix {
    UI_DIR      = .uic
    MOC_DIR     = .moc
    RCC_DIR     = .rcc
}

win32 {
    UI_DIR      = _uic
    MOC_DIR     = _moc
    RCC_DIR     = _rcc
}
