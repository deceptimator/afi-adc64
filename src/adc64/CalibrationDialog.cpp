#include "CalibrationDialog.h"

#include <QTableWidget>
#include <QtMath>

#include "Config.h"
#include "EventData.h"
#include "ui_CalibrationDialog.h"

namespace {
const int CAL_LIMIT_STEP = 10000;
const int RMS_LIMIT = 100;
}

CalibrationDialog::CalibrationDialog(Config *config, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CalibrationDialog),
    config(config),
    calLimit(CAL_LIMIT_STEP)
{
    ui->setupUi(this);
    uint devices=0;
    setWindowTitle("Calibration dialog");

    foreach (const DominoSettings &ds, config->device_configs.values()) {
        if(!ds.enabled)
            continue;
        ++devices;
        tabIndexes.push_back(ds.devIndex);
        QTableWidget *table = new QTableWidget(ui->tabWidget);
        ui->tabWidget->insertTab(tabIndexes.indexOf(ds.devIndex), table, ds.devIndex.getSerialStr());
        QStringList headers;
        headers << "Mean" << "RMS" << "Count";
        table->setColumnCount(3);
        table->setHorizontalHeaderLabels(headers);
        table->setRowCount(ds.nch);
        for(int ch=0; ch<ds.nch; ch++){
            table->setVerticalHeaderItem(ch, new QTableWidgetItem(QString::number(ch+1)));
            table->setItem(ch, 0, new QTableWidgetItem(QString::number(ds.chBaseline[ch])));
            table->setItem(ch, 1, new QTableWidgetItem("0"));
            table->setItem(ch, 2, new QTableWidgetItem("0"));
            calData[ds.devIndex][ch].clear();
        }
    }
    ui->labelNoDevices->setVisible(!devices);
    ui->tabWidget->setVisible(devices);
    completed = isCompleted();
//    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
}

CalibrationDialog::~CalibrationDialog()
{
    delete ui;
}

QMap<int, int> CalibrationDialog::getBaseLineByIndex(const DeviceIndex &index) const
{
    QMap<int, int> result;
    if(calResults.contains(index)){
        foreach (int ch, calResults[index].keys()) {
            result[ch] = calResults[index][ch].first;
        }
    }
    return result;
}

void CalibrationDialog::onDataAcquired(const EventData &eventData)
{
    if(completed || !running) return;
    int maxDataSize = 0;

    foreach (const DeviceIndex &i, calData.keys()) {
        if(!eventData.contains(i.getSerial()))
            continue;
        QTableWidget *table = qobject_cast<QTableWidget *>(ui->tabWidget->widget(tabIndexes.indexOf(i)));
        Q_ASSERT(table);
        foreach (int ch, calData[i].keys()) {
            QVector<qint16> &rawData = calData[i][ch];
            if(rawData.size()>=calLimit)
                continue;
            const QVector<qint16> &newRawData = eventData[i.getSerial()].rawData[ch];
            rawData << newRawData;
            maxDataSize = qMax(maxDataSize, rawData.size());
            table->item(ch, 2)->setText(QString::number(rawData.size()));
        }
    }

    if(isCompleted()){
        on_pushButtonStartStop_clicked();
    } else if(maxDataSize < 30000)
        calculateMeanRms();
//    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(maxDataSize>0);
}

void CalibrationDialog::on_pushButtonClear_clicked()
{
    foreach (const DeviceIndex &i, calData.keys()) {
        foreach (int ch, calData[i].keys()) {
            calData[i][ch].clear();
        }
    }
    calLimit = CAL_LIMIT_STEP;
    completed = false;
    calculateMeanRms();
}

void CalibrationDialog::on_pushButtonStartStop_clicked()
{
    if(completed && !running){
        calLimit += CAL_LIMIT_STEP;
        completed = false;
    }

    running = !running;
    ui->pushButtonStartStop->setText(running ? "Stop" : "Continue");
    if(!running)
        calculateMeanRms();
}

bool CalibrationDialog::isCompleted()
{
    completed = true;
    foreach (const DeviceIndex i, calData.keys()) {
        foreach (int ch, calData[i].keys()) {
            if(calData[i][ch].size()<calLimit){
                completed = false;
                break;
            }
        }
        if(!completed)
            break;
    }
    return completed;
}

void CalibrationDialog::calculateMeanRms()
{
    foreach (const DeviceIndex &i, calData.keys()) {
        QTableWidget *table = qobject_cast<QTableWidget *>(ui->tabWidget->widget(tabIndexes.indexOf(i)));
        Q_ASSERT(table);
        foreach (int ch, calData[i].keys()) {
            QVector<qint16> &rawData = calData[i][ch];
            QPair<double, double> &blData = calResults[i][ch];
            blData.first=0;
            blData.second=0;
            if(rawData.size()){
                foreach (const qint16 &val, rawData)
                    blData.first += val;
                blData.first /= rawData.size();
                foreach (const qint16 &val, rawData)
                    blData.second += qPow(blData.first-val, 2);
                blData.second /= rawData.size();
                blData.second = qSqrt(blData.second);
            }
            table->item(ch, 0)->setText(QString::number(blData.first, 'f', 2));
            table->item(ch, 1)->setText(QString::number(blData.second, 'f', 2));
            table->item(ch, 2)->setText(QString::number(rawData.size()));
            if(blData.second > RMS_LIMIT){
//                table->item(ch, 0)->setBackgroundColor(Qt::red);
                table->item(ch, 1)->setBackgroundColor(Qt::red);
//                table->item(ch, 2)->setBackgroundColor(Qt::red);
                ui->tabWidget->tabBar()->setTabTextColor(tabIndexes.indexOf(i), Qt::red);
            }
        }
    }
}
