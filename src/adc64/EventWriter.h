//
//    Copyright 2011 Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef EVENTWRITER_H
#define EVENTWRITER_H

#include <QDateTime>
#include <QElapsedTimer>
#include <QObject>

class EventData;
class QFile;
class QTextStream;
class QTimer;

class EventWriter : public QObject
{
    Q_OBJECT
public:
    explicit EventWriter(QObject *parent = nullptr);
    ~EventWriter() override;
    QString dataFileName;

Q_SIGNALS:
    void writerStatus(QString st);

public Q_SLOTS:
    void setFileName(const QString &str);
    void flush();
    void close();
    void SaveData(const EventData &eventData);

private:
    QFile * const dataFile;
    QTextStream * const dataStream;
    QTimer * const flushTimer;
    QDateTime comp_time;
    QElapsedTimer lastFlashTime;
    qint64 lastSize = 0;
};

#endif // EVENTWRITER_H
