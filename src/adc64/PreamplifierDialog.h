#ifndef PREAMPLIFIERDIALOG_H
#define PREAMPLIFIERDIALOG_H

#include <QDialog>

#include "Config.h"
#include "adc64-common/dominosettings.h"

namespace Ui {
class PreamplifierDialog;
}

class PreamplifierDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PreamplifierDialog(Config *config, QWidget *parent = nullptr);
    ~PreamplifierDialog() override;
    void reset_gui();

signals:
    void config_changed();

private slots:
    void device_changed(DeviceIndex index);
    void on_groupBox_toggled(bool checked);
    void on_radioButtonVersion1_toggled(bool checked);
    void on_radioButtonVersion2_toggled(bool checked);
    void on_spinBoxBaseline_valueChanged(int baseline);
    void on_spinBoxLevel_valueChanged(int level);
    void block_gui_signals(bool block);

private:
    Ui::PreamplifierDialog * const ui;
    QList<QWidget*> block_signal_widgets_list;
    Config * const config;
    DeviceIndex selected_device;
};

#endif // PREAMPLIFIERDIALOG_H
