#ifndef CONFIG_H
#define CONFIG_H

#include <QByteArray>
#include <QMap>

#include "Adc64StartupOptions.h"
#include "adc64-common/dominosettings.h"
#include "mldiscover/DeviceIndex.h"
#include "mongo/BaseConfig.h"

class Config  : BaseConfig
{
public:
    Config(const Adc64StartupOptions& startup_options);
    void read_config();
    void write_config();
    void blockSparseSetup();
    void setRun(bool val);
    bool isRunning() const { return run; }
    void setShowSparseMarkers(bool val) { showSparseMarkers = val; }
    bool isShowSparseMarkers() const { return showSparseMarkers; }
    void setShowPcbCh(bool val) { showPcbCh = val; }
    bool isShowPcbCh() const { return showPcbCh; }

    QString program_type;
    QString program_index;
    QString configuration_name;

    QString window_geometry;
    int clock_delay = 0;
    int data_delay = 0;
    bool preamplifier = false;
    bool run = false;
    bool showSparseMarkers = true;
    bool showPcbCh = false;
    QMap<QString, QVector<qint32>> fir_map;

    QMap<DeviceIndex, DominoSettings> device_configs;
    RootConfig default_root;
private:
    void update_root();
    RootConfig root;
};

#endif // CONFIG_H
