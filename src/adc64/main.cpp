
#include <stdexcept>

#include <QApplication>

#include "Adc64StartupOptions.h"
#include "daq-config/DaqConfig.h"
#include "mainwindow.h"
#include "mongo/BaseConfig.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setOrganizationDomain("local");
    a.setOrganizationName("AFI Electronics");
    a.setApplicationName("ADC64");
    QSettings::setDefaultFormat(QSettings::IniFormat);

    QString program_type = DaqConfig::getTypeName(DaqConfigAdc64);
    #ifdef MONGO_DRIVERS
        mongocxx::instance insta{};
    #endif
    BaseConfig base_config;
    base_config.init(program_type);

    Adc64StartupOptions startup_options(program_type);
    startup_options.parse_options();

    if(startup_options.list){
        return 0;
    }

    int code = 1;
    try {
        MainWindow w(startup_options);
        w.show();
        code = a.exec();
    }
    catch (std::runtime_error &e) {
        qFatal("%s", e.what());
        return 1;
    }
    return code;
}
