#include "ReadoutWindowDialog.h"

#include <QDebug>

#include "ui_ReadoutWindowDialog.h"

namespace {
const int MIN_READ_CNT = 4;
const int MIN_AFTER_TRIG = 5;
}
ReadoutWindowDialog::ReadoutWindowDialog(int _readCount, int _afterTrig, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ReadoutWindowDialog),
    readCount(_readCount),
    afterTrig(_afterTrig),
    leftEdge(0),
    rightEdge(readCount)
{
    ui->setupUi(this);
    ui->spinBoxLeftEdge->blockSignals(true);
    ui->spinBoxRightEdge->blockSignals(true);
    ui->spinBoxLeftEdge->setMinimum(0);
    ui->spinBoxRightEdge->setMaximum(readCount);
    updateLimits();
    ui->spinBoxRightEdge->setValue(rightEdge);
    ui->spinBoxLeftEdge->setValue(leftEdge);
    ui->spinBoxLeftEdge->blockSignals(false);
    ui->spinBoxRightEdge->blockSignals(false);
    updateLabels();
}

ReadoutWindowDialog::~ReadoutWindowDialog()
{
    delete ui;
}

int ReadoutWindowDialog::getReadCount() const
{
    int val = rightEdge-leftEdge;
    if(val&1)
        ++val;
    return val;
}

int ReadoutWindowDialog::getTrigDelay() const
{
    return afterTrig-(readCount-rightEdge);
}

void ReadoutWindowDialog::on_spinBoxLeftEdge_valueChanged(int val)
{
    leftEdge = val;
    updateLimits();
    updateLabels();
}

void ReadoutWindowDialog::on_spinBoxRightEdge_valueChanged(int val)
{
    rightEdge = val;
    updateLimits();
    updateLabels();
}

void ReadoutWindowDialog::updateLimits()
{
    ui->spinBoxLeftEdge->setMaximum(rightEdge-MIN_READ_CNT);
    int val = leftEdge+MIN_READ_CNT;
    val = qMax(readCount-afterTrig+5, val);
    ui->spinBoxRightEdge->setMinimum(val);
}

void ReadoutWindowDialog::updateLabels()
{
    QString msg;
    msg = QString("%1").arg(readCount);
    if(readCount != getReadCount()){
        msg.append(QString(" -> %1").arg(getReadCount()));
    }
    ui->labelReadCnt->setText(msg);

    msg = QString("%1").arg(afterTrig);
    if(afterTrig != getTrigDelay()){
        msg.append(QString(" -> %1").arg(getTrigDelay()));
    }
    ui->labelAfterTrig->setText(msg);
}

void ReadoutWindowDialog::info()
{
    qInfo()<<"Left:"<<ui->spinBoxLeftEdge->minimum()<<ui->spinBoxLeftEdge->value()<<ui->spinBoxLeftEdge->maximum();
    qInfo()<<"Right:"<<ui->spinBoxRightEdge->minimum()<<ui->spinBoxRightEdge->value()<<ui->spinBoxRightEdge->maximum();
}
