#include "mainwindow.h"

#include <stdexcept>

#include <QDebug>
#include <QDesktopServices>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QHostAddress>
#include <QLabel>
#include <QMap>
#include <QMessageBox>
#include <QSettings>
#include <QSplitter>
#include <QStatusBar>
#include <QStringList>
#include <QStyleOptionGroupBox>
#include <QTextCodec>
#include <QTextEdit>
#include <QThread>
#include <QTimer>
#include <QUrl>
#include <QVBoxLayout>

#include "AddFirPresetDialog.h"
#include "CalibrationDialog.h"
#include "CopyConfigDialog.h"
#include "DominoDeviceManager.h"
#include "EventData.h"
#include "EventWriter.h"
#include "PreamplifierDialog.h"
#include "ReadoutWindowDialog.h"
#include "SparseDialog.h"
#include "device-discover/DiscoverDialog.h"
#include "adc64-common/dominodevice.h"
#include "adc64-common/dominosettings.h"
#include "qwt_plot_marker.h"
#include "qxw/WaveformPlot.h"
#include "regio/regiomlink.h"
#include "ui_mainwindow.h"
#include "util/QDateTimeCompat.h"
#include "waitcursor.h"
#include "base/AboutBox.h"

namespace  {
enum {COL_SERIAL, COL_ID, COL_ID_STR, COL_SERIAL_STR, COL_TEMP, COL_WR_TIME, COL_FW, COL_CH, COL_STATUS, COL_ADC_LOCK, COL_ADC_TEST, COL_TOTAL};

const double TEMP_WARN_LIMIT = 60;

const int chMapV2[7][9] = {
    {5,2,1,4,3,29,26,31,30},
    {25,28,27,24,23,22,21,18,17},
    {14,13,10,9,12,11,8,7,6},
    {20,19,64,63,62,61,58,57,60},
    {39,38,37,34,33,36,35,16,15},
    {59,56,55,54,53,49,50,51,52},
    {48,47,46,45,42,41,44,43,40}
};
// <X, <guiCh, pcbCh> >
const QMap<int, QMap<int,int> > chMapV4X
( { {1, QMap<int, int>({{14, 28},{07, 29},{15, 30},{ 8, 31},{16, 32},{26, 33},{19, 34},{25, 35},{18, 36} })},
    {2, QMap<int, int>({{27, 59},{20, 58},{28, 57},{21, 56},{29, 55},{22, 54},{30, 53},{23, 52},{31, 51} })},
    {3, QMap<int, int>({{02, 19},{10, 20},{03, 21},{11, 22},{04, 23},{12, 24},{05, 25},{13, 26},{06, 27} })},
    {4, QMap<int, int>({{24, 50},{32, 49},{49, 48},{57, 47},{50, 46},{58, 45},{51, 44},{59, 43},{52, 42} })},
    {5, QMap<int, int>({{45, 10},{38, 11},{46, 12},{39, 13},{47, 14},{40, 15},{48, 16},{ 1, 17},{ 9, 18} })},
    {6, QMap<int, int>({{60, 41},{53, 40},{61, 39},{54, 38},{62, 37},{63, 63},{55, 62},{64, 61},{56, 60} })},
    {7, QMap<int, int>({{33, 01},{41, 02},{34, 03},{42, 04},{35, 05},{43, 06},{36, 07},{44,  8},{37,  9} })},
    {0, QMap<int, int>({{17, 64}})}
  } );
// <gui, pcbCh>
QMap<int,int> chMapV4;

const QString CH_PREFIX_OCTAL = "Octal";
const QString CH_PREFIX_AMP_X = "Amp X";

const int CH_AMP_X_GROUP_SIZE = 9;
typedef QPair<DeviceIndex, int> AmpGroupIndex;
}

Q_DECLARE_METATYPE(AmpGroupIndex)

MainWindow::MainWindow(const Adc64StartupOptions &startup_options, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    config(startup_options),
    writerLabel(new QLabel(this)),
    evLabel(new QLabel(this)),
    deviceMgr(new DominoDeviceManager(nullptr)),
    lastEventData(new EventData),
    eventWriter (new EventWriter),
    deviceMngThread(new QThread(this)),
    writerThread(new QThread(this)),
    channelsSetupForm(new ChannelsSetupForm(&config, this)),
    sparseDialog(new SparseDialog(&config, this)),
    preamplifier_dialog(new PreamplifierDialog(&config, this)),
    discoverDialog(new DiscoverDialog(this)),
    pcbSwitchingEnabled(startup_options.pcb_ch),
    debug_mode(startup_options.debug_mode)
{

    qRegisterMetaType<DominoSettings>();
    qRegisterMetaType<DominoData>();
    qRegisterMetaType< QVector<DeviceDescription> >();
    qRegisterMetaType<DeviceIndex>();

    {
        std::map<int,int> tmpChMap2;
        for(const auto& m : chMapV4X.values()){
            const auto tmpChMap = m.toStdMap();
            tmpChMap2.insert(tmpChMap.begin(), tmpChMap.end());
        }
        chMapV4 = QMap<int,int>(tmpChMap2);
    }

    if(debug_mode){
        QMap<int, bool> allCh;
        for(int x=0;x<64/CH_AMP_X_GROUP_SIZE;++x){
            for(int y=0; y<CH_AMP_X_GROUP_SIZE; ++y){
                int ch = chMapV2[x][y];
                Q_ASSERT(!allCh.contains(ch));
                allCh[ch] = true;
            }
        }
        Q_ASSERT(allCh.size() == 64/CH_AMP_X_GROUP_SIZE*CH_AMP_X_GROUP_SIZE);
        allCh.clear();
    }

    ui->setupUi(this);

    if(startup_options.pcb_ch){
        enabledPcbSwitching();
    }

    connect(ui->actionQuit, &QAction::triggered, qApp, &QCoreApplication::quit);
    connect(ui->actionAboutQt, &QAction::triggered, qApp, &QApplication::aboutQt);
    statusBar()->addPermanentWidget(writerLabel, 10);
    statusBar()->addPermanentWidget(evLabel, 2);
    ui->actionShow_PCB_ch->setVisible(pcbSwitchingEnabled);

    deviceMngThread->start();
    deviceMngThread->setObjectName("Adc64DevMng");
    deviceMgr->moveToThread(deviceMngThread);
    connect(deviceMgr, SIGNAL(destroyed()), deviceMngThread, SLOT(quit()));
    connect(this, SIGNAL(destroyAll()), deviceMgr, SLOT(deleteLater()));

    connect(this, SIGNAL(clearEventNumber()), deviceMgr, SLOT(clearFullEventNumber()));
    connect(this, SIGNAL(setupChanged(DominoSettings)), deviceMgr, SIGNAL(setupChanged(DominoSettings)));
    connect(this, SIGNAL(deserializeDelayChanged(int,int)), deviceMgr, SIGNAL(deserializeDelayChanged(int,int)));
    connect(this, SIGNAL(deviceListUpdated(QVector<DeviceDescription>)),
            deviceMgr, SLOT(deviceListUpdated(QVector<DeviceDescription>)));
    connect(deviceMgr, SIGNAL(deviceConnected(DeviceIndex,QString)), SLOT(onDeviceConnect(DeviceIndex,QString)));
    connect(deviceMgr, SIGNAL(deviceDisconnected(DeviceIndex)), SLOT(onDeviceDisconnect(DeviceIndex)));
    connect(deviceMgr, SIGNAL(deviceStatusUpdated(DeviceIndex,QString)), SLOT(deviceStatusUpdated(DeviceIndex,QString)));
    connect(deviceMgr, SIGNAL(statusUpdated(DeviceIndex,AdcStatus)), SLOT(updateAdcStatus(DeviceIndex,AdcStatus)));
    connect(deviceMgr, SIGNAL(deviceInfoUpdated(DeviceIndex,int,int,bool)), SLOT(deviceInfoUpdated(DeviceIndex,int,int,bool)));
    connect(deviceMgr, SIGNAL(monitorEventChanged(EventData)), SLOT(DrawScope(EventData)));
    connect(deviceMgr, SIGNAL(adcLockStatusUpdated(DeviceIndex,bool,quint8)),
            SLOT(adcLockStatusUpdated(DeviceIndex,bool,quint8)));
    connect(deviceMgr, SIGNAL(adcRampStatusUpdated(DeviceIndex,bool,quint8)),
            SLOT(adcRampStatusUpdated(DeviceIndex,bool,quint8)));
    connect(ui->actionFindAdcClockDelay, SIGNAL(triggered(bool)), deviceMgr, SLOT(findAdcClockDelay()));

    connect(discoverDialog, SIGNAL(deviceDiscovered(DeviceDescription)), deviceMgr, SIGNAL(deviceUpdated(DeviceDescription)));

    connect(this, SIGNAL(updateChannelsSetupFormSettings()), channelsSetupForm, SLOT(onSetSettings()));
    connect(this, SIGNAL(selectedDeviceChanged(DeviceIndex)), channelsSetupForm, SLOT(onDeviceChange(DeviceIndex)));
    connect(this, SIGNAL(selectedDeviceChanged(DeviceIndex)), sparseDialog, SLOT(onDeviceChange(DeviceIndex)));
    connect(channelsSetupForm, SIGNAL(channelsSetupChanged()), SLOT(channelsSetupFormChanged()));
    connect(sparseDialog, SIGNAL(setupChanged()), SLOT(channelsSetupFormChanged()));
    connect(sparseDialog, SIGNAL(setupChanged()), SLOT(updateSparseMarkers()));

    connect(this, SIGNAL(selectedDeviceChanged(DeviceIndex)), preamplifier_dialog, SLOT(device_changed(DeviceIndex)));
    connect(preamplifier_dialog, SIGNAL(config_changed()), SLOT(channelsSetupFormChanged()));

    block_signal_widgets_list.append(ui->spinBoxClockDelay);
    block_signal_widgets_list.append(ui->spinBoxDataDelay);
    block_signal_widgets_list.append(ui->checkBoxInvertSignal);
    block_signal_widgets_list.append(ui->spinBoxSize);
    block_signal_widgets_list.append(ui->spinBoxLatency);
    block_signal_widgets_list.append(ui->checkBoxTrigTimer);
    block_signal_widgets_list.append(ui->checkBoxTrigThreshold);
    block_signal_widgets_list.append(ui->comboBoxThrTrig);
    block_signal_widgets_list.append(ui->checkBoxTrigLemo);
    block_signal_widgets_list.append(ui->groupBoxDsp);
    block_signal_widgets_list.append(ui->spinBoxFirRoundoff);
    block_signal_widgets_list.append(ui->groupBoxTailCancellation);
    block_signal_widgets_list.append(ui->comboBoxFirPreset);
    block_signal_widgets_list.append(ui->comboBoxDspMafType);
    block_signal_widgets_list.append(ui->comboBoxDspMafSel);
    block_signal_widgets_list.append(ui->spinBoxDspBlcThr);
    block_signal_widgets_list.append(ui->groupBoxZS);
    block_signal_widgets_list.append(ui->comboBoxZSThr);
    block_signal_widgets_list.append(ui->checkBoxSoftwareZS);
    block_signal_widgets_list.append(ui->checkBoxSSM);
    block_signal_widgets_list.append(ui->tableWidgetDevices);
    block_signal_widgets_list.append(ui->groupBoxMaf);

    ui->widget_settings->layout()->setSpacing(20);

    QSplitter *splitter_vertical = new QSplitter;
    splitter_vertical->setOrientation(Qt::Vertical);
    splitter_vertical->addWidget(ui->widget_settings);
    splitter_vertical->addWidget(ui->widget);
    splitter_vertical->addWidget(ui->widget_plot);
    splitter_vertical->setCollapsible(0, false);
    splitter_vertical->setCollapsible(1, true);
    splitter_vertical->setCollapsible(2, false);
    splitter_vertical->setStretchFactor(0,1);
    splitter_vertical->setStretchFactor(1,1);
    splitter_vertical->setStretchFactor(2,10);

    QGridLayout *layout_main = new QGridLayout;
    layout_main->addWidget(splitter_vertical);
    ui->centralWidget->setLayout(layout_main);

    QString gbox_style_1 = (
    "   QGroupBox::title{                   "
    "       background: white;              "
    "       border: 1px solid gray;         "
    "       border-radius: 3px;             "
    "       padding: 2px;                   "
    "   }                                   "
    "   QGroupBox{                          "
    "       background: rgb(216, 231, 235); "
    "       border: 1px solid gray;         "
    "       border-radius: 3px;             "
    "       margin-top: 6px;                "
    "   }                                   "
    );

    QString gbox_style_2 = (
    "   QGroupBox::title{                   "
    "       background: white;              "
    "       border: 1px solid gray;         "
    "       border-radius: 3px;             "
    "       padding: 2px;                   "
    "   }                                   "
    "   QGroupBox{                          "
    "       border: 1px solid gray;         "
    "       border-radius: 3px;             "
    "       margin-top: 6px;                "
    "   }                                   "
    );

    ui->groupBoxTailCancellation->setStyleSheet(gbox_style_1);
    ui->groupBoxMaf->setStyleSheet(gbox_style_1);
    ui->groupBoxDsp->setStyleSheet(gbox_style_2);

    ui->groupBox_ReadoutWindow->setStyleSheet(gbox_style_1);
    ui->groupBox_ExpertSettings->setStyleSheet(gbox_style_1);
    ui->groupBox_ReadoutWindow->setStyleSheet(gbox_style_1);
    ui->groupBoxTrigger->setStyleSheet(gbox_style_1);
    ui->groupBoxZS->setStyleSheet(gbox_style_1);

    QStringList edge = (QStringList() << "Rising" << "Falling");
    block_gui_signals(true);
    ui->comboBoxThrTrig->addItems(edge);
    ui->comboBoxZSThr->addItems(edge);

    ui->comboBoxDspMafType->addItem("None", 0);
    ui->comboBoxDspMafType->addItem("MAF", 1);
    ui->comboBoxDspMafType->addItem("Test", 2);
    ui->comboBoxDspMafType->addItem("MAF+Test", 3);

    block_gui_signals(false);

    QString styleLargeFont = "font-size: 18pt";
    ui->pushButtonStart->setStyleSheet(styleLargeFont);
    ui->pushButtonStop->setStyleSheet(styleLargeFont);
    ui->pushButtonStart->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    ui->pushButtonStop->setIcon(style()->standardIcon(QStyle::SP_MediaStop));

    initTable();
    read_config();

    configuration_manager = new ConfigurationManager(this, startup_options.program_type, config.default_root);
    connect(configuration_manager, SIGNAL(switch_to_config(QString, QString)), this, SLOT(switch_to(QString, QString)));

    Q_EMIT updateChannelsSetupFormSettings();

    if(!debug_mode){
        ui->groupBox_ExpertSettings->hide();
        ui->comboBoxDspMafType->hide();
        ui->label_9->hide();

        for(DeviceIndex index : config.device_configs.keys()){
            config.device_configs[selectedDevice].dspParams.test_enable = false; // FIXME: selectedDevice -> index
        }
    }

    enableRunControlButtons();

    writerThread->start();
    eventWriter->moveToThread(writerThread);
    connect(eventWriter, SIGNAL(destroyed()), writerThread, SLOT(quit()));
    connect(eventWriter, SIGNAL(writerStatus(QString)), SLOT(writerStatusUpdated(QString)));
    connect(this, SIGNAL(destroyAll()), eventWriter, SLOT(deleteLater()));

    foreach (int id, deviceMgr->getSupportedDevices()) {
        discoverDialog->addDeviceIdFilter(id);
    }
    discoverDialog->setSingleMode(false);

    QVector<DeviceDescription> device_description_list;
    for(DeviceIndex device_index  : config.device_configs.keys()){
        DeviceDescription device_description(device_index);
        device_description_list.append(device_description);
    }
    discoverDialog->setSelected(device_description_list);
}

MainWindow::~MainWindow()
{
    config.write_config();

    emit destroyAll();
    QElapsedTimer t;
    t.start();
    do {
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 100);
    } while( (deviceMngThread->isRunning() || writerThread->isRunning())
            && (t.elapsed()<5000) );

    if(deviceMngThread->isRunning()){
        deviceMngThread->terminate();
        if (!deviceMngThread->wait(1000))
            qWarning() << "Device thread aborted";
    }
    if(writerThread->isRunning()){
        writerThread->terminate();
        if (!writerThread->wait(1000))
            qWarning() << "Writer thread aborted";
    }
    delete ui;
}

void MainWindow::switch_to(const QString &program_index, const QString& configuration_name)
{
    configuration_manager->hide();
    config.program_index = program_index;
    config.configuration_name = configuration_name;
    ui->tableWidgetDevices->setRowCount(0);
    read_config();
}

void MainWindow::on_actionConfiguration_manager_triggered()
{
    configuration_manager->show();
    configuration_manager->reset(config.program_index, config.configuration_name);
}


void MainWindow::enabledPcbSwitching()
{
    pcbSwitchingEnabled = true;
    ui->actionShow_PCB_ch->setVisible(pcbSwitchingEnabled);
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    config.write_config();
    QMainWindow::closeEvent(event);
}

void MainWindow::channelsSetupFormChanged()
{
    config.write_config();
    setupChanged();
}

void MainWindow::onDeviceConnect(const DeviceIndex &i, const QString &fw)
{
    const int row = getDeviceRow(i);
    if(row != -1) {
        ui->tableWidgetDevices->item(row, COL_FW)->setText(fw);
        for(int col = 0; col < ui->tableWidgetDevices->columnCount(); ++col)
            ui->tableWidgetDevices->item(row, col)->setBackgroundColor(Qt::transparent);
    }
    enableRunControlButtons();
}

void MainWindow::onDeviceDisconnect(const DeviceIndex &i)
{
    const int row = getDeviceRow(i);
    if(row != -1){
        for(int col = 0; col < ui->tableWidgetDevices->columnCount(); ++col)
            ui->tableWidgetDevices->item(row, col)->setBackgroundColor(Qt::lightGray);
    }
    //    QMessageBox::critical(0, "Error", "Device disconnected");
    enableRunControlButtons();
}

void MainWindow::updateAdcStatus(const DeviceIndex &index, const AdcStatus &status)
{
    int row = getDeviceRow(index);
    if(row == -1)
        return;

    // read temperature sensors
    {
        QTableWidgetItem *item = ui->tableWidgetDevices->item(row, COL_TEMP);
        QStringList sl;
        double maxTemp = -255;
        if (status.temp > maxTemp) maxTemp = status.temp;
        if (status.temp > -255)
            sl += QString("ADC:%1").arg((int)status.temp);
        QMap<int, QString> name;
        name[0] = "PS";
        name[1] = "AMP";
        for (int i=0; i<status.bmStatus.sensors.size(); i++) {
            double t = status.bmStatus.sensors[i].temp;
            if (t > maxTemp) maxTemp = t;
            if (t > -255)
                sl += QString("%1:%2").arg(name[i]).arg((int)t);
        }
        item->setText(sl.join(" "));
        item->setBackgroundColor(maxTemp > TEMP_WARN_LIMIT ? Qt::red : Qt::transparent);
    }

    bool wrTimeValid = false;
    QTableWidgetItem *wrItem = ui->tableWidgetDevices->item(row, COL_WR_TIME);
    const QColor bgColorDefault = ui->tableWidgetDevices->style()->standardPalette().light().color();
    const QColor bgColorWarning = Qt::yellow;
    const QColor bgColorError = Qt::red;
    QStringList strListToolTip;
    strListToolTip << "WhiteRabbit sync";
    strListToolTip << QString("wrLinkError:%1").arg(status.wrEpStatus.wrLinkError);
    strListToolTip << QString("wrCodeError:%1").arg(status.wrEpStatus.wrSyncFail);
    QColor itemColor = bgColorDefault;
    if (status.wrEpStatus.hasWrEpStatus) {
        wrTimeValid = status.wrEpStatus.timeValid();
        const bool wrTimeNull = status.wrEpStatus.timeNull();
        if (wrTimeNull) {
            wrItem->setText(QString());
            itemColor = bgColorWarning;
        } else {
            QDateTime wrTime = QDateTime::fromMSecsSinceEpoch(status.wrEpStatus.mSecsSinceEpoch());

            int wrTimeCheck = status.wrEpStatus.wrTimeCheck();
            qint64 wrTimeOffsetMs = status.wrEpStatus.mSecsSinceEpoch() - status.wrEpStatus.acquiredAtMs;
            if(!wrTimeValid || wrTimeCheck > 1)
                itemColor = bgColorError;
            else if (wrTimeCheck == 1)
                itemColor = bgColorWarning;

            strListToolTip << QString("wrTime:%1").arg(wrTime.toUTC().toString("yyyy-MM-dd hh:mm:ss"));
//            wrItem->setText(wrTime.toUTC().toString("yyyy-MM-dd hh:mm:ss")); // add ".zzz" for ms
            QString strOffset;
            if (wrTimeOffsetMs/1000. > -60 && wrTimeOffsetMs/1000. < 60)
                strOffset = QString(" %1").arg(wrTimeOffsetMs * 1e-3, 6, 'f', 3);
            strListToolTip << QString("Offset:%1").arg(strOffset.trimmed());
        }
        strListToolTip << QString("wrCodeError:%1").arg(status.wrEpStatus.wrCodeError);
        strListToolTip << QString("wrUptime:%1").arg(status.wrEpStatus.wrUptime);
    } else {
            wrTimeValid = status.adcStatus & 0x2;
            itemColor = wrTimeValid ? bgColorDefault : bgColorError;
    }
    if(index.getDevId() == DEVICE_ID_ADC64WR) {
        wrItem->setText(wrTimeValid?"WR Ok":"WR Invalid");
        wrItem->setBackgroundColor(itemColor);
        wrItem->setToolTip(strListToolTip.join('\n'));
    } else {
        wrItem->setText(" - ");
        wrItem->setToolTip("Device doesn't support WhiteRabbit");
    }
    updateTableSize();
}

void MainWindow::deviceStatusUpdated(const DeviceIndex &index, const QString &s) const
{
    int row = getDeviceRow(index);
    if(row == -1)
        return;
    QTableWidgetItem *item = ui->tableWidgetDevices->item(row, COL_STATUS);
    item->setText(s);
    updateTableSize();
}

void MainWindow::adcLockStatusUpdated(const DeviceIndex &index, bool ok, quint8 locked) const
{
    int row = getDeviceRow(index);
    if(row == -1)
        return;
    QTableWidgetItem *item = ui->tableWidgetDevices->item(row, COL_ADC_LOCK);
    if(ok){
        item->setText("Adc lock Ok");
        item->setTextColor(QColor());
    } else {
        item->setText(QString("Adc lock %1").arg(locked, 8, 2, QChar('0')));
        item->setTextColor(Qt::red);
    }
    updateTableSize();
}

void MainWindow::adcRampStatusUpdated(const DeviceIndex &index, bool ok, quint8 rampError) const
{
    int row = getDeviceRow(index);
    if(row == -1)
        return;
    QTableWidgetItem *item = ui->tableWidgetDevices->item(row, COL_ADC_TEST);
    if(ok){
        item->setText("Adc test Ok");
        item->setTextColor(QColor());
    } else {
        item->setText(QString("Adc test %1").arg(rampError, 8, 2, QChar('0')));
        item->setTextColor(Qt::red);
    }
    updateTableSize();
}

void MainWindow::deviceInfoUpdated(DeviceIndex index, int chNum, int maxSampleCnt, bool blockOffset)
{
    if(!config.device_configs.contains(index)){
        qWarning() << "There is no device in settings with index:" << hex << index;
        return;
    }
    DominoSettings &settings = config.device_configs[index];
    settings.setNCh(chNum);
    devMaxSampleCnt[index] = maxSampleCnt;
    sparseDialog->setOffsetMaximum(index, maxSampleCnt);
    sparseDialog->blockOffset(index, blockOffset);
    if(!devNegLat.contains(index) || devNegLat[index] != blockOffset) {
        devNegLat[index] = blockOffset;
        restoreFormValues();
    }

    totalChNum = 0;
    devChOffset.clear();
    foreach (const DominoSettings &ds, config.device_configs.values()) {
        if(!ds.enabled)
            continue;
        const int row = getDeviceRow(ds.devIndex);
        Q_ASSERT(row != -1);
        devChOffset[ds.devIndex] = totalChNum;
        if(ds.nch) {
            ui->tableWidgetDevices->item(row, COL_CH)->setText(QString("%1-%2").arg(totalChNum+1).arg(totalChNum+ds.nch));
            totalChNum += ds.nch;
        } else {
            ui->tableWidgetDevices->item(row, COL_CH)->setText("—");
        }
    }
    updateAdcChGroups();

//    ui->spinBoxWriteCNT->setMaximum(channelMaxSampleCount);
    if(selectedDevice==index && maxSampleCnt)
        ui->spinBoxSize->setMaximum(maxSampleCnt);

    if(ui->spinBoxSize->minimum()<4)
        ui->spinBoxSize->setMinimum(4);
    Q_EMIT updateChannelsSetupFormSettings();
}

void MainWindow::DrawScope(const EventData &eventData, bool force)
{
    if (!force && scopeDisplayTime.isValid() && scopeDisplayTime.elapsed() < 20) // update too fast
        return;

    if (!force && eventData.timestamp.isValid() && eventData.timestamp.elapsed() > 1000) { // data too old
        qWarning() << QString("DrawScope: event %1 dropped (old)").arg(eventData.getEventNumberStr());
        return;
    }

    *lastEventData = eventData;

    WaveformDataAll mvv;
    int adcOctetNum = ui->comboBoxOctalADC->currentIndex()-1;
    const bool displayAll = (adcOctetNum == -1);
//    QVector<double> trig_tai_ts;
    Q_FOREACH(const DominoData &dominoData, eventData) {
        const DominoRawData &dd = dominoData.rawData;
        DominoSettings devSettings;
        foreach (const DominoSettings &ds, config.device_configs) {
            if(ds.devIndex.getSerial()==dominoData.serial_id){
                devSettings = ds;
                break;
            }
        }
        if(devSettings.devIndex.isNull()) continue; // Data from removed device
        Q_ASSERT(devSettings.devIndex.getSerial() == dominoData.serial_id);
//        trig_tai_ts.push_back(dominoData.trigStatus.trig_tai_ts.bit.toSeconds());

//        std::cout << QString("%1").arg(dominoData.trigStatus.trig_in_delay_ns).toStdString() << std::endl;

        int channelOffset = devChOffset.value(devSettings.devIndex, 0);
        Q_FOREACH(int i, dd.keys()) {
            if (!displayAll && !displayEn[channelOffset+i])
                continue;
            if (dd[i].empty())
                continue;
            WaveformData wd;
            wd.c = channelOffset+i;
            if(pcbSwitchingEnabled && config.isShowPcbCh())
                wd.name = QString("pcb%1").arg(chMapV4.value(i+1, 0)).toStdString();
            else
                wd.name = QString("ch%1").arg(channelOffset+i+1).toStdString();

            wd.p.resize(dd[i].size());
            for(int j=0; j < dd[i].size(); j++)
            {
                wd.p[j].y = dd[i][j]-devSettings.chBaseline[i];
                wd.p[j].x = j;
//                double x_ns = j*dominoData.stepNs - dominoData.trigStatus.trig_in_delay_ns;
//                wd.p[j].x = x_ns / 1e3;
            }
            mvv.push_back(wd);
        }

        channelOffset += dd.size();
//        qDebug() << "channelOffset=" << channelOffset;
    }
//    // print timestamp delta
//    if (trig_tai_ts.size() > 1) {
//        for (int i=1; i<trig_tai_ts.size(); i++) {
//            double delta = trig_tai_ts[i] - trig_tai_ts[0];
//            std::cout << QString("%1").arg(delta, 0, 'f', 15).toStdString() << std::endl;
//        }
//    }
    ui->xPlot->setAllData(mvv);
//    evLabel->setText(QString("Ev %1").arg(eventData.getEventNumberStr()));
    evLabel->setText(QString("Ev %1").arg(eventData.FullEventNumber));
    //    dominoData.PrintRMS();
    scopeDisplayTime.start();
}

void MainWindow::writerStatusUpdated(const QString &st)
{
    writerLabel->setText(st);
}

void MainWindow::deviceUpdated(const DeviceDescription &dd)
{
    if(!config.device_configs.contains(dd.getIndex())) return;

    DominoSettings &ds = config.device_configs[dd.getIndex()];
    if(ds.hwStr != dd.hw_str){
        if(ds.hwStr.isEmpty()){
            ds.hwStr = dd.hw_str;
            config.write_config();
            updateDisplayedChannels();
        } else
            qWarning()<<QString("H/W version was changed from %1 to %2 for %3")
                        .arg(ds.hwStr).arg(dd.hw_str).arg(dd.getSerialIdStr());
    }
}

void MainWindow::updateSparseMarkers()
{
    bool wasAutoReplot = ui->xPlot->autoReplot();
    ui->xPlot->setAutoReplot(false);

    foreach (QwtPlotMarker *m, sparseMarkers) {
        delete m;
    }
    sparseMarkers.clear();

    int period=0;
    int readCellNum=0;
    foreach (const DominoSettings &setting, config.device_configs.values()) {
        if(setting.enabled && setting.sparseParams.en) {
            period=setting.sparseParams.period;
            readCellNum = setting.sparseParams.readCellNum;
            readCellNum = qMin(readCellNum, setting.readCellNumber);
            break;
        }
    }

//    ui->checkBoxSSM->setEnabled(readCellNum);
    if(!readCellNum){
        ui->checkBoxSSM->hide();
    }
    else{
        ui->checkBoxSSM->show();
    }
    if(!config.isShowSparseMarkers())
        readCellNum=0;

    if(readCellNum == 0){
        ui->xPlot->replot();
    }

    for(int i=0; i<readCellNum; ++i) {
        auto * marker = new QwtPlotMarker();
        marker->setLineStyle(QwtPlotMarker::VLine);
        marker->setXValue(i*period);
        marker->label().font().italic();
        QwtText label(QString("sparse: %1").arg(i*period));
        QFont font = label.font();
        font.setItalic(true);
        label.setFont(font);
        marker->setLabel(label);
        marker->setLabelOrientation(Qt::Vertical);
        marker->setLabelAlignment(Qt::AlignRight | Qt::AlignBottom);
        marker->attach(ui->xPlot);
        sparseMarkers.push_back(marker);
    }
    ui->xPlot->setAutoReplot(wasAutoReplot);
}

void MainWindow::setupChanged()
{
    foreach (const DominoSettings &setting, config.device_configs.values()) {
        emit setupChanged(setting);
    }
}

void MainWindow::enableRunControlButtons()
{
    ui->pushButtonStart->setEnabled(!config.isRunning() && deviceMgr->isAllConnected());
    ui->pushButtonStop->setEnabled(config.isRunning() && deviceMgr->isAllConnected());
    ui->actionCalibrate->setEnabled(!config.isRunning() && deviceMgr->isAllConnected());
}

void MainWindow::restoreFormValues()
{
    block_gui_signals(true);

    setWindowTitle(QString("%1   |   Program index: %2   |   Configuration name:  %3")
                   .arg(config.program_type).arg(config.program_index).arg(config.configuration_name));

    ui->spinBoxClockDelay->setValue(config.clock_delay);
    ui->spinBoxDataDelay->setValue(config.data_delay);

    const bool present = config.device_configs.contains(selectedDevice);
    ui->actionCopyConfig->setEnabled(present);
    if(!present){
        block_gui_signals(false);
        return;
    }

    const DominoSettings &settings = config.device_configs[selectedDevice];
    ui->spinBoxSize->setMinimum(4);
    if(devMaxSampleCnt.contains(selectedDevice) && devMaxSampleCnt[selectedDevice]){
        ui->spinBoxSize->setMaximum(devMaxSampleCnt[selectedDevice]);
    }
    else{
        ui->spinBoxSize->setMaximum(settings.readCellNumber);
    }
    ui->spinBoxSize->setValue(settings.readCellNumber);
    int latMin = settings.trigDelay;
    int latMax = settings.trigDelay;

    if(devNegLat.contains(selectedDevice)) {
        latMin = devNegLat[selectedDevice] ? -511:1;
        latMax = devNegLat[selectedDevice] ? 512:65535;
    }

    ui->spinBoxLatency->setMinimum(latMin);
    ui->spinBoxLatency->setMaximum(latMax);
    ui->spinBoxLatency->setValue(settings.trigDelay);
    ui->checkBoxTrigTimer->setChecked(settings.trigsetup.bit.timer_en);
    ui->checkBoxTrigLemo->setChecked(settings.trigsetup.bit.ttl_en);
    ui->checkBoxTrigThreshold->setChecked(settings.trigsetup.bit.threshold_en);
    updateThresholdTrigger(settings.trigsetup.bit.threshold_en);

    ui->groupBoxZS->setChecked(settings.zs_en);

    ui->comboBoxThrTrig->setCurrentIndex(settings.invert_thr_trig ? 1 : 0);
    ui->comboBoxZSThr->setCurrentIndex(settings.invert_zs_thr ? 1 : 0);
    ui->checkBoxSoftwareZS->setChecked(settings.softwareZs);
    ui->checkBoxInvertSignal->setChecked(settings.invertInput);

    ui->actionPreamplifier->setEnabled(true);

    ui->groupBoxDsp->setChecked(settings.dspParams.en);
    ui->groupBoxTailCancellation->setChecked(settings.dspParams.fir.en);

    if(debug_mode){
        ui->groupBoxMaf->setCheckable(false);
    }
    else{
        ui->groupBoxMaf->setChecked(settings.dspParams.maf_enable);
    }
    ui->spinBoxFirRoundoff->setMinimum(0);
    ui->spinBoxFirRoundoff->setMaximum(settings.dspParams.fir.getRoundoffMax());
    ui->spinBoxFirRoundoff->setValue(settings.dspParams.fir.roundoff);
    const int mafTypeIndex = ui->comboBoxDspMafType->findData(settings.dspParams.getMafType());
    Q_ASSERT(mafTypeIndex != -1);

    ui->comboBoxDspMafType->setCurrentIndex(mafTypeIndex);


    ui->spinBoxDspBlcThr->setMaximum(32767);
    ui->spinBoxDspBlcThr->setValue(settings.dspParams.blc_thr);
    ui->spinBoxDspBlcThr->setMinimum(0);
    if (!ui->comboBoxDspMafSel->count()) {
        QStringList sl;
        sl += "2";
        sl += "4";
        sl += "8";
        sl += "16";
        ui->comboBoxDspMafSel->insertItems(0, sl);
    }
    ui->comboBoxDspMafSel->setCurrentIndex(settings.dspParams.maf_tap_sel);
    int firIndex = ui->comboBoxFirPreset->findText(settings.dspParams.fir.presetKey);
    if(firIndex == -1)
        firIndex = 0;
    ui->comboBoxFirPreset->setCurrentIndex(firIndex);
    ui->checkBoxSSM->setChecked(config.isShowSparseMarkers());

    block_gui_signals(false);
}

void MainWindow::updateThresholdTrigger(bool enabled)
{
    ui->comboBoxThrTrig->setEnabled(enabled);
//    ui->labelThresholdTrigger->setEnabled(enabled);
}

void MainWindow::addDeviceRow(const DeviceIndex &index)
{
    if(getDeviceRow(index) != -1){
        return;
    }
    QTableWidget * const table = ui->tableWidgetDevices;
    table->setSortingEnabled(false);
    table->insertRow(0);
    table->setItem(0, COL_ID, new QTableWidgetItem(QString::number(index.getDevId())));
    table->setItem(0, COL_SERIAL, new QTableWidgetItem(QString::number(index.getSerial())));
    table->setItem(0, COL_ID_STR, new QTableWidgetItem(getDeviceTypeName(index.getDevId())));
    table->setItem(0, COL_SERIAL_STR, new QTableWidgetItem(index.getSerialStr()));
    table->setItem(0, COL_FW, new QTableWidgetItem());
    table->setItem(0, COL_WR_TIME, new QTableWidgetItem());
    table->setItem(0, COL_CH, new QTableWidgetItem());
    table->setItem(0, COL_TEMP, new QTableWidgetItem());
    table->setItem(0, COL_STATUS, new QTableWidgetItem("Creating..."));
    table->setItem(0, COL_ADC_LOCK, new QTableWidgetItem());
    table->setItem(0, COL_ADC_TEST, new QTableWidgetItem());
    table->setSortingEnabled(true);
    if(table->rowCount() > 1)
        table->showColumn(COL_CH);
    else
        table->hideColumn(COL_CH);
    if(index.getDevId() == DEVICE_ID_ADC64WR)
        table->showColumn(COL_WR_TIME);
    updateTableSize();
    onDeviceDisconnect(index);
}

int MainWindow::getDeviceRow(const DeviceIndex &index) const
{
    for(int row=0; row< ui->tableWidgetDevices->rowCount(); ++row){
        if(getDeviceIndex(row) == index){
            return row;
        }
    }
    return -1;
}

DeviceIndex MainWindow::getDeviceIndex(int row) const
{
    DeviceIndex index;
    index.first = ui->tableWidgetDevices->item(row, COL_ID)->text().toUShort();
    index.second = ui->tableWidgetDevices->item(row, COL_SERIAL)->text().toULongLong();
    return index;
}

void MainWindow::initTable() const
{
    QTableWidget * const table = ui->tableWidgetDevices;
    table->horizontalHeader()->hide();
    table->verticalHeader()->hide();
    table->setColumnCount(COL_TOTAL);
    table->hideColumn(COL_ID);
    table->hideColumn(COL_SERIAL);
    table->hideColumn(COL_WR_TIME);
}

void MainWindow::updateTableSize() const
{
    QTableWidget * const table = ui->tableWidgetDevices;
    table->resizeColumnsToContents();
    table->resizeRowsToContents();
    int height = table->verticalHeader()->length()+4;
    if(height<20)
        height = 20;
    table->setFixedSize(table->horizontalHeader()->length()+4, height);
    table->updateGeometry();
}

void MainWindow::updateDisplayedChannels()
{
    displayEn.clear();

    bool displayOctet = false;
    int adcOctetNum = 0;

    bool displayAmpX = false;
    QVector<int> chGroupVec(CH_AMP_X_GROUP_SIZE);

    QString str = ui->comboBoxOctalADC->currentText();
    if(str.startsWith(CH_PREFIX_OCTAL)){
        QStringList var = str.remove(0, CH_PREFIX_OCTAL.size()).trimmed().split(' ');
        adcOctetNum = QString(var[0]).toInt(&displayOctet);
    } else if(str.startsWith(CH_PREFIX_AMP_X)){
        AmpGroupIndex ampGroupIndex = ui->comboBoxOctalADC->currentData().value<AmpGroupIndex>();

        displayAmpX = config.device_configs.contains(ampGroupIndex.first);
        if(displayAmpX) {
            const int hwVer = config.device_configs[ampGroupIndex.first].getHwVer();
            const int xChGroup = ampGroupIndex.second;

            switch (hwVer) {
            case 2:
                qCopy(chMapV2[xChGroup-1], chMapV2[xChGroup-1]+CH_AMP_X_GROUP_SIZE, chGroupVec.begin()); // FIXME
                break;
            case 4:
                chGroupVec = QVector<int>::fromList(chMapV4X[xChGroup].keys());
                break;
            default:
                displayAmpX = false;
                break;
            }

            // Shift ch in chGroupVec
            int chOffset = 0;
            foreach (const DominoSettings &ds, config.device_configs) {
                if(ds.devIndex == ampGroupIndex.first) {
                    if(chOffset) {
                        for(int i=0; i<CH_AMP_X_GROUP_SIZE; ++i) {
                            chGroupVec[i] += chOffset;
                        }
                    }
                    break;
                } else
                    chOffset += ds.nch;
            }
        }
    }

    for(int ch=0; ch<totalChNum; ++ch){
        bool v = true;
        if(displayOctet)
            // Octal
            v = ((ch/8)==(adcOctetNum-1));
        else if(displayAmpX)
            // Amp X
            v = chGroupVec.contains(ch+1);
        else
            // All
            v = true;

        displayEn[ch] = v;
    }
}

void MainWindow::updateAdcChGroups()
{
    QMap<int, QString> channelGroups;
    for(int i=0; i<totalChNum/8; i++)
        channelGroups[i+1] = QString("%1 %2 (%3-%4)")
                .arg(CH_PREFIX_OCTAL).arg(i+1)
                .arg(i*8+1).arg((i+1)*8);

    ui->comboBoxOctalADC->clear();
    int id = 0;
    ui->comboBoxOctalADC->insertItem(id++, QString("All channels (%1-%2)").arg(totalChNum>0?1:0).arg(totalChNum), 0);
    Q_FOREACH(int i, channelGroups.keys())
        ui->comboBoxOctalADC->insertItem(id++, channelGroups[i], i);

    foreach (const DominoSettings &ds, config.device_configs) {
        if(ds.enabled && ds.getHwVer()) {
            for(int xGroupIndex=1; xGroupIndex<=ds.nch/CH_AMP_X_GROUP_SIZE; ++xGroupIndex){
                ui->comboBoxOctalADC->insertItem(id++, QString("%1%2 %3")
                                                 .arg(CH_PREFIX_AMP_X).arg(xGroupIndex)
                                                 .arg(ds.devIndex.getSerialStr()),
                                                 QVariant::fromValue(AmpGroupIndex(ds.devIndex, xGroupIndex)));
            }
        }
    }
}

void MainWindow::update_fir_combo_box()
{
    ui->comboBoxFirPreset->blockSignals(true);
    QStringList firPresets = config.fir_map.keys();
    ui->comboBoxFirPreset->clear();
    ui->comboBoxFirPreset->insertItem(0, "None", "");
    foreach (const QString &preset, firPresets) {
        if(!preset.isEmpty()){
            ui->comboBoxFirPreset->insertItem(firPresets.size(), preset, preset);
        }
    }
    ui->comboBoxFirPreset->blockSignals(false);
}

void MainWindow::read_config()
{
    config.read_config();
    config.blockSparseSetup();
    update_fir_combo_box();

    QVector<DeviceDescription> devicesAll;
    QVector<DeviceDescription> devicesEnabled;

    bool first_key = true;
    ui->tableWidgetDevices->setSelectionBehavior(QAbstractItemView::SelectRows);
    foreach (const DeviceIndex &index, config.device_configs.keys()) {
        const DominoSettings &ds = config.device_configs[index];
        DeviceDescription dd;
        dd.device_id = ds.devIndex.getDevId();
        dd.serial_id = ds.devIndex.getSerial();
        dd.hw_str = ds.hwStr;
        dd.enabled = ds.enabled;
        devicesAll.append(dd);
        if(dd.enabled){
            addDeviceRow(index);
            devicesEnabled.push_back(dd);
            if(first_key){
                ui->tableWidgetDevices->selectRow(0);
                first_key = false;
            }
        }
    }

    updateTableSize();
    discoverDialog->setSelected(devicesAll);
    emit deviceListUpdated(devicesEnabled);

    if(config.device_configs.isEmpty()){
        restoreFormValues();
    }
    updateSparseMarkers();
}

void MainWindow::block_gui_signals(bool block)
{
    for(int i=0; i<block_signal_widgets_list.size(); ++i){
        block_signal_widgets_list[i]->blockSignals(block);
    }
}

//--- GUI slots
//--- options
void MainWindow::on_actionChange_device_triggered()
{
    on_pushButtonFindDevice_clicked();
}

void MainWindow::on_actionCalibrate_triggered()
{
    if(config.isRunning()){
        on_pushButtonStop_clicked();
     }

    QMap<DeviceIndex, unsigned int> timer_en_map;
    QMap<DeviceIndex, int> read_cell_number_map;
    QMap<DeviceIndex, int> trig_delay_map;
    QMap<DeviceIndex, bool> zs_en;
    QMap<DeviceIndex, QMap<int, bool>> ch_en_map;

    foreach (const DeviceIndex &index, config.device_configs.keys()){
        timer_en_map[index] = config.device_configs[index].trigsetup.bit.timer_en;
        read_cell_number_map[index] = config.device_configs[index].readCellNumber;
        trig_delay_map[index] = config.device_configs[index].trigDelay;
        zs_en[index] = config.device_configs[index].zs_en;
        ch_en_map[index] = config.device_configs[index].chEn;
    }

    foreach (const DeviceIndex &index, config.device_configs.keys()) {
        config.device_configs[index].trigsetup.bit.timer_en = 1;
        config.device_configs[index].readCellNumber = 1000;
        config.device_configs[index].trigDelay = 0;
        config.device_configs[index].zs_en = false;
        config.device_configs[index].set_en_all(true);
    }

    on_pushButtonStart_clicked();

    CalibrationDialog dialog(&config, this);
    connect(deviceMgr, SIGNAL(dataAcquired(EventData)), &dialog, SLOT(onDataAcquired(EventData)));
    if(dialog.exec() == QDialog::Accepted) {
        foreach (const DeviceIndex &index, config.device_configs.keys())
            if(config.device_configs[index].enabled){
                config.device_configs[index].chBaseline = dialog.getBaseLineByIndex(index);
             }
    }

    on_pushButtonStop_clicked();

    foreach (const DeviceIndex &index, config.device_configs.keys()) {
        config.device_configs[index].trigsetup.bit.timer_en = timer_en_map[index];
        config.device_configs[index].readCellNumber = read_cell_number_map[index];
        config.device_configs[index].trigDelay = trig_delay_map[index];
        config.device_configs[index].zs_en = zs_en[index];
        config.device_configs[index].chEn = ch_en_map[index];
    }

    config.write_config();
    Q_EMIT updateChannelsSetupFormSettings();
}

void MainWindow::on_actionSet_read_out_window_triggered()
{
    if(!config.device_configs.contains(selectedDevice)) return;
    DominoSettings &settings = config.device_configs[selectedDevice];

    ReadoutWindowDialog dialog(settings.readCellNumber, settings.trigDelay, this);
    if(dialog.exec() == QDialog::Accepted){
        settings.readCellNumber = dialog.getReadCount();
        settings.trigDelay = dialog.getTrigDelay();
        restoreFormValues();
    }
}

void MainWindow::on_actionSparse_parameters_triggered()
{
    sparseDialog->show();
}

void MainWindow::on_actionCopyConfig_triggered()
{
    CopyConfigDialog dialog(&config,  this);
    if(dialog.exec() == QDialog::Accepted) {
    }
}

void MainWindow::on_actionShow_PCB_ch_toggled(bool val)
{
    config.setShowPcbCh(val);
    config.write_config();
}

void MainWindow::on_actionPreamplifier_triggered()
{
    preamplifier_dialog->show();
}

void MainWindow::on_actionAdd_FIR_preset_triggered()
{
    AddFirPresetDialog fir_dialog(&config, this);
    if(fir_dialog.exec() == QDialog::Accepted) {
        update_fir_combo_box();
        config.write_config();
    }
}

void MainWindow::on_actionRemove_all_presets_triggered()
{
    QDialog dialog;
    QVBoxLayout layout(&dialog);
    QLabel label("Remove all FIR presets?");
    layout.addWidget(&label);
    QDialogButtonBox * button_box = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(button_box, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(button_box, SIGNAL(rejected()), &dialog, SLOT(reject()));
    layout.addWidget(button_box);

    if(dialog.exec() == QDialog::Accepted)
    {
        config.fir_map.clear();
        for(int i=ui->comboBoxFirPreset->count(); i>0; --i){
            ui->comboBoxFirPreset->removeItem(i);
        }
        config.write_config();
    }
}
//--about
void MainWindow::on_actionDocs_triggered()
{
    QDesktopServices::openUrl(QUrl("https://afi-project.jinr.ru/projects/adc64-sw/wiki/"));
}

void MainWindow::on_actionChannel_map_triggered()
{
    if(!channelMapDialog){
        channelMapDialog = new QDialog(this);
        channelMapDialog->setWindowTitle("Channel map help");
        auto *view = new QTextEdit(channelMapDialog);
        view->setReadOnly(true);
        QFile htmlFile(":/channelMap.html");
        QString html;
        if(htmlFile.open(QIODevice::ReadOnly)){
            html = QString(htmlFile.readAll());
            htmlFile.close();
            if(html.isEmpty())
                html = "<b>Channel map file is empty.</b>";
        } else
            html = "<b>There is no channel map file.</b>";
        view->setText(html);
        channelMapDialog->setLayout(new QVBoxLayout(channelMapDialog));
        channelMapDialog->layout()->addWidget(view);
        channelMapDialog->resize(800, 600);
    }
    channelMapDialog->show();
}

void MainWindow::on_actionCheckForUpdates_triggered()
{
    QDesktopServices::openUrl(QUrl("http://afi.jinr.ru/Downloads/ADC64"));
}

void MainWindow::on_actionAbout_triggered()
{
    AboutBox::about();
}

//--expert
void MainWindow::on_spinBoxClockDelay_valueChanged(int clock_delay)
{
    config.clock_delay = clock_delay;
    emit deserializeDelayChanged(config.clock_delay, config.data_delay);
    config.write_config();
}

void MainWindow::on_spinBoxDataDelay_valueChanged(int data_delay)
{
    config.data_delay = data_delay;
    emit deserializeDelayChanged(config.clock_delay, config.data_delay);
    config.write_config();
}

void MainWindow::on_pushButtonSendDelay_clicked()
{
    emit deserializeDelayChanged(config.clock_delay, config.data_delay);
}

//--readout window
void MainWindow::on_spinBoxSize_valueChanged(int newval)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    DominoSettings &settings = config.device_configs[selectedDevice];
//#ifdef QT_NO_DEBUG
//    // pass only even value if not debug
//    if(newval & 1) {
//        ui->spinBoxReadCNT->setValue(newval+1);
//        return;
//    }
//#endif
    settings.readCellNumber = newval;
    config.write_config();
    setupChanged();
    updateSparseMarkers();
}

void MainWindow::on_spinBoxLatency_valueChanged(int newval)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    config.device_configs[selectedDevice].trigDelay=newval;
    config.write_config();
    setupChanged();
}

//--trigger
void MainWindow::on_checkBoxTrigTimer_toggled(bool checked)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    DominoSettings &settings = config.device_configs[selectedDevice];
    settings.trigsetup.bit.timer_en = checked;
    config.write_config();
    setupChanged();
}


void MainWindow::on_checkBoxTrigThreshold_toggled(bool checked)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    DominoSettings &settings = config.device_configs[selectedDevice];
    settings.trigsetup.bit.threshold_en = checked ? 1:0 ;
    updateThresholdTrigger(checked);
    Q_EMIT updateChannelsSetupFormSettings();
    config.write_config();
    setupChanged();
}

void MainWindow::on_comboBoxThrTrig_currentIndexChanged(const QString &arg1)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    DominoSettings &settings = config.device_configs[selectedDevice];
    settings.invert_thr_trig = (arg1 != "Rising");
    config.write_config();
    setupChanged();
}

void MainWindow::on_checkBoxTrigLemo_toggled(bool checked)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    DominoSettings &settings = config.device_configs[selectedDevice];
    settings.trigsetup.bit.ttl_en = checked ? 1:0 ;
    config.write_config();
    setupChanged();
}

//--dsp roundoff
void MainWindow::on_groupBoxDsp_toggled(bool checked)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    DominoSettings &settings = config.device_configs[selectedDevice];
    settings.dspParams.en = checked;
    config.write_config();
    setupChanged();
//   Q_EMIT updateChannelsSetupFormSettings();
}

void MainWindow::on_checkBoxInvertSignal_toggled(bool invert)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    DominoSettings &settings = config.device_configs[selectedDevice];
    settings.setInvert(invert);
    config.write_config();
    setupChanged();
    restoreFormValues();
    Q_EMIT updateChannelsSetupFormSettings();
}

void MainWindow::on_spinBoxFirRoundoff_valueChanged(int value)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    DominoSettings &settings = config.device_configs[selectedDevice];
    settings.dspParams.fir.roundoff = value;
    config.write_config();
    setupChanged();
}
//--dsp tail
void MainWindow::on_groupBoxTailCancellation_toggled(bool checked)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    DominoSettings &settings = config.device_configs[selectedDevice];
    settings.dspParams.fir.en = checked;
    config.write_config();
    setupChanged();
}

void MainWindow::on_comboBoxFirPreset_currentIndexChanged(int index)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    const QString firPreset = ui->comboBoxFirPreset->itemData(index).toString();
    config.device_configs[selectedDevice].dspParams.fir.presetKey = firPreset;
    config.write_config();
    setupChanged();
}

//--dsp maf
void MainWindow::on_groupBoxMaf_toggled(bool checked)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    if (config.device_configs[selectedDevice].dspParams.maf_enable != checked) {
        config.device_configs[selectedDevice].dspParams.maf_enable = checked;
        config.write_config();
        setupChanged();
    }
}

void MainWindow::on_comboBoxDspMafType_currentIndexChanged(int index)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    const uint v = ui->comboBoxDspMafType->itemData(index).toUInt();
    DominoSettings &settings = config.device_configs[selectedDevice];
    settings.dspParams.maf_enable = v & 1;
    settings.dspParams.test_enable = v & 2;
    config.write_config();
    setupChanged();
}

void MainWindow::on_comboBoxDspMafSel_activated(int index)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    DominoSettings &settings = config.device_configs[selectedDevice];
    settings.dspParams.maf_tap_sel = index;
    config.write_config();
    setupChanged();
}

void MainWindow::on_spinBoxDspBlcThr_valueChanged(int value)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    DominoSettings &settings = config.device_configs[selectedDevice];
    settings.dspParams.blc_thr = value;
    config.write_config();
    setupChanged();
}

//--zero suppr
void MainWindow::on_groupBoxZS_toggled(bool checked)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    DominoSettings &settings = config.device_configs[selectedDevice];
    settings.zs_en = checked;
    config.write_config();
    setupChanged();
    Q_EMIT updateChannelsSetupFormSettings();
}

void MainWindow::on_comboBoxZSThr_currentIndexChanged(const QString &arg1)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    DominoSettings &settings = config.device_configs[selectedDevice];
    settings.invert_zs_thr = (arg1 != "Rising");
    config.write_config();
    setupChanged();
}

void MainWindow::on_checkBoxSoftwareZS_toggled(bool checked)
{
    if(!config.device_configs.contains(selectedDevice)) return;
    DominoSettings &settings = config.device_configs[selectedDevice];
    settings.softwareZs = checked;
    config.write_config();
    setupChanged();
}

//--find device
void MainWindow::on_pushButtonFindDevice_clicked()
{
    if(discoverDialog->exec() == QDialog::Accepted){
        ui->tableWidgetDevices->setRowCount(0);
        ui->tableWidgetDevices->hideColumn(COL_WR_TIME);

        QMap<DeviceIndex, DominoSettings> oldSettings = config.device_configs;
        config.device_configs.clear();

        int index=0;
        QVector<DeviceDescription> devices;
        bool first_key = true;
        foreach (DeviceDescription dd, discoverDialog->getSelectedDevices()) {
            DominoSettings &ds = config.device_configs[dd.getIndex()];
            if(oldSettings.contains(dd.getIndex())){
                ds = oldSettings.take(dd.getIndex());
            } else {
                ds.devIndex = dd.getIndex();
            }
            ds.enabled = dd.enabled;
            ds.hwStr = dd.hw_str;

            if(dd.enabled) {
                addDeviceRow(dd.getIndex());
                devices.push_back(dd);
                if(first_key){
                    ui->tableWidgetDevices->selectRow(0);
                    first_key = false;
                }
            }
            ++index;
        }
        config.blockSparseSetup();
        config.write_config();

        emit deviceListUpdated(devices);
        restoreFormValues();
    }
}

void MainWindow::on_tableWidgetDevices_itemSelectionChanged()
{
    QTableWidget *table = ui->tableWidgetDevices;
    QList<QTableWidgetSelectionRange> ranges = table->selectedRanges();
    DeviceIndex newDevice;
    if(!ranges.empty()){
        const int row = table->currentRow();
        if(ranges.first().leftColumn() != COL_ID_STR || ranges.first().rightColumn() != COL_SERIAL_STR ||
                ranges.first().bottomRow() != ranges.first().topRow()){
            table->blockSignals(true);
            table->clearSelection();
            table->setRangeSelected(QTableWidgetSelectionRange(row, COL_ID_STR, row, COL_SERIAL_STR), true);
            table->blockSignals(false);
        }

        newDevice = getDeviceIndex(row);
    }

    if(selectedDevice != newDevice){
        selectedDevice = newDevice;
        emit selectedDeviceChanged(selectedDevice);
        restoreFormValues();
    }
}

//--start/stop
void MainWindow::on_pushButtonStart_clicked()
{
    if(config.isRunning())
        return;
    config.setRun(true);
    emit clearEventNumber();
    setupChanged();
    enableRunControlButtons();
}

void MainWindow::on_pushButtonStop_clicked()
{
    if (!config.isRunning())
        return;
    config.setRun(false);
    setupChanged();
    enableRunControlButtons();
}

void MainWindow::on_checkBoxW2D_toggled(bool checked)
{
    if (checked)
    {
        QString fileName = QFileDialog::getSaveFileName(this,
                                                        tr("Save File"),
                                                        eventWriter->dataFileName,
                                                        tr("Text files *.txt;;All files *.*"));
        if (!fileName.isNull()) {
            QMetaObject::invokeMethod(eventWriter, "setFileName", Q_ARG(QString, fileName));
            connect(deviceMgr, SIGNAL(dataAcquired(EventData)), eventWriter, SLOT(SaveData(EventData)));
        }
        else ui->checkBoxW2D->setChecked(false);
    } else {
        QMetaObject::invokeMethod(eventWriter, "close");
        disconnect(deviceMgr, SIGNAL(dataAcquired(EventData)), eventWriter, nullptr);
    }
}

void MainWindow::on_pushButtonChannelsSetup_clicked()
{
    channelsSetupForm->show();
}

void MainWindow::on_comboBoxOctalADC_currentIndexChanged(int)
{
    updateDisplayedChannels();

    DrawScope(*lastEventData, true);
}

void MainWindow::on_checkBoxSSM_toggled(bool checked)
{
    config.setShowSparseMarkers(checked);
    config.write_config();
    updateSparseMarkers();
}
