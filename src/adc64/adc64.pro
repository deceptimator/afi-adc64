TEMPLATE = app
QT += core gui widgets

isEmpty(EXTDIR) {
    EXTDIR = $$PWD/../../external
}

CONFIG += device-discover lib-common mlinkip mongo qwt qxw
CONFIG += remote-control-server

include($$EXTDIR/libs.pri)

include(stdconfig.pri)

INCLUDEPATH += $$PWD/..
include(../adc64-common/adc64-common.pri)
include(adc64.pri)

target.path = /bin
INSTALLS = target
TARGET = afi-adc64
