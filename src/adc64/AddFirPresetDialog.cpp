#include "AddFirPresetDialog.h"

#include <climits>

#include <QDebug>
#include <QPushButton>
#include <QSpinBox>
#include <QLabel>

#include "ui_AddFirPresetDialog.h"

const QString DEFAULT = "default";

AddFirPresetDialog::AddFirPresetDialog(Config *config, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddFirPresetDialog),
    config(config)
{
    ui->setupUi(this);
    ui->spinBox->setValue(16);
    setWindowTitle("Add FIR preset");

    QString preset_name_ini;
    if(!config->fir_map.isEmpty()){
        preset_name_ini = config->fir_map.begin().key() + "(1)";
    }
    else{
        preset_name_ini = DEFAULT;
    }
    ui->lineEdit->setText(preset_name_ini);

    if(check_input_validity(preset_name_ini)){
        preset_name = preset_name_ini;
    }
}

AddFirPresetDialog::~AddFirPresetDialog()
{
    delete ui;
}

QVector<qint32> AddFirPresetDialog::getFir() const
{
    QVector<qint32> fir;
    auto layout = ui->formLayout;
    for(int row=0; row<layout->rowCount(); ++row) {
        QLayoutItem *item = layout->itemAt(row, QFormLayout::FieldRole);
        QWidget *w = item->widget();
        if(w) {
            auto *sb = qobject_cast<QSpinBox*>(w);
            if(sb) {
                fir.push_back(static_cast<qint32>(sb->value()));
            }
        }
    }
    return fir;
}

void AddFirPresetDialog::on_spinBox_valueChanged(int num)
{
    auto layout = ui->formLayout;
    while(layout->rowCount() > num)
        layout->removeRow(num);
    while (layout->rowCount() < num){
        int row = layout->rowCount();
        auto sb = new QSpinBox(this);
        sb->setMinimum(std::numeric_limits<qint32>::min());
        sb->setMaximum(std::numeric_limits<qint32>::max());
        layout->addRow(new QLabel(QString("coef %1").arg(row), this),
                       sb);
    }
}

bool AddFirPresetDialog::check_input_validity(const QString& check_this)
{
    QPalette palette;
    QColor red(240,128,128);
    bool check;
    if(config->fir_map.contains(check_this)){
        palette.setColor(QPalette::Base, red);
        palette.setColor(QPalette::Text,Qt::black);
        ui->button_box->button(QDialogButtonBox::Ok)->setEnabled(false);
        check = false;
    }
    else{
        palette.setColor(QPalette::Base,Qt::white);
        palette.setColor(QPalette::Text,Qt::black);
        ui->button_box->button(QDialogButtonBox::Ok)->setEnabled(true);
        check = true;
    }
    ui->lineEdit->setPalette(palette);
    return check;
}

void AddFirPresetDialog::on_lineEdit_textChanged(const QString &arg1)
{
    if(check_input_validity(arg1)){
        preset_name = arg1;
    }
}

void AddFirPresetDialog::on_button_box_accepted()
{
    config->fir_map[preset_name].append(getFir());
}
