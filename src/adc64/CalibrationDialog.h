#ifndef CALIBRATIONDIALOG_H
#define CALIBRATIONDIALOG_H

#include <QDialog>
#include "mldiscover/DeviceIndex.h"
#include "Config.h"
#include "EventData.h"

namespace Ui {
class CalibrationDialog;
}

class CalibrationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CalibrationDialog(Config *Config, QWidget *parent = nullptr);
    ~CalibrationDialog() override;
    QMap<int, int> getBaseLineByIndex(const DeviceIndex &index) const;

private slots:
    void onDataAcquired(const EventData &eventData);
    void on_pushButtonClear_clicked();
    void on_pushButtonStartStop_clicked();

private:
    bool isCompleted();
    void calculateMeanRms();

    Ui::CalibrationDialog * const ui;
    Config * const config;
    QVector<DeviceIndex> tabIndexes;
    QMap<DeviceIndex, QMap<int, QVector<qint16> > > calData;
    QMap<DeviceIndex, QMap<int, QPair<double, double> > > calResults;
    bool completed = false;
    bool running = false;
    int calLimit;
};

#endif // CALIBRATIONDIALOG_H
