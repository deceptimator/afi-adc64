#include "Config.h"

#include <QDebug>
#include <QSettings>
#include <QJsonDocument>

#include "Adc64StartupOptions.h"
#include "mongo/BaseConfig.h"

const QString DEVICE = "device";
const QString FIR_PRESET = "fir_preset";
const QString COEF = "coef";

Config::Config(const Adc64StartupOptions& startup_options) :
    program_type(startup_options.program_type),
    program_index(startup_options.program_index),
    configuration_name(startup_options.configuration_name)
{
    update_root();
    default_root = root;
}

void Config::read_config()
{
    root = BaseConfig::read_config(program_type, program_index, configuration_name);
    if(root.isEmpty()){
        BaseConfig::write_config(default_root);
    }

    window_geometry = root.get_data<QString>("window_geometry");
    clock_delay = root.get_data<int>("clock_delay");
    data_delay = root.get_data<int>("data_delay");
    preamplifier = root.get_data<bool>("preamplifier");
    showSparseMarkers = root.get_data<bool>("showSparseMarkers");
    showPcbCh = root.get_data<bool>("showPcbCh");

    fir_map.clear();
    for(QString child_name : root.children().keys()){
        QString fir_prefix = child_name.split("_").at(0) + "_" + child_name.split("_").at(1);
        if(fir_prefix == FIR_PRESET){
            QString fir_name = child_name.split("_").at(2);
            if(fir_name.isEmpty())
                continue;

            RootConfig root_child  = root.child(child_name);
            QVector<qint32> fir_vect_tmp;
            QMap<qint32,qint32> fir_map_tmp;

            for(QString coef : root_child.data().keys()){
                fir_map_tmp[coef.split("_").at(1).toInt()] = root_child.get_data<qint32>(coef);
            }

            for(int i=0; i<fir_map_tmp.size(); ++i){
                fir_vect_tmp.push_back(fir_map_tmp[i]);
            }
            fir_map[fir_name] = fir_vect_tmp;
        }
    }

    device_configs.clear();
    for(QString child_name : root.children().keys()){
        if(child_name.split("_").at(0) == DEVICE){
            RootConfig root_child  = root.child(child_name);
            DeviceIndex device_index = DeviceIndex(root_child.get_data<quint64>("device_id"),
                                                   root_child.get_data<quint64>("device_serial"));
            DominoSettings device_config;
            device_config.root_to_values(root_child);
            device_configs[device_index] = device_config;
        }
    }
}

void Config::write_config()
{
    update_root();
    BaseConfig::write_config(root);
}

void Config::update_root()
{
    root.remove_all_data();
    root.set_data("program_type", program_type);
    root.set_data("program_index", program_index);
    root.set_data("configuration_name", configuration_name);
    root.set_data("window_geometry", window_geometry);
    root.set_data("clock_delay", clock_delay);
    root.set_data("data_delay", data_delay);
    root.set_data("preamplifier", preamplifier);
    root.set_data("showSparseMarkers", showSparseMarkers);
    root.set_data("showPcbCh", showPcbCh);

    for(const QString& fir_name : fir_map.keys()){
        RootConfig root_fir = RootConfig();
        for(int i=0; i<fir_map[fir_name].size(); ++i){
            QString coef;
            if(i<10){
                coef = COEF + "_0" + QString::number(i);
            }
            else{
                coef = COEF + "_" + QString::number(i);
            }

            root_fir.set_data(coef, fir_map[fir_name][i]);
        }
        root_fir.item_name = FIR_PRESET + "_" + fir_name;
        root.append_child(root_fir);
    }

    for(DeviceIndex device_index : device_configs.keys()){
        RootConfig root_child = device_configs[device_index].values_to_root();
        root_child.item_name = DEVICE + "_" + device_index.getSerialStr();
        root.append_child(root_child);
    }
}

void Config::setRun(bool val)
{
    run = val;

    for(DeviceIndex device_index : device_configs.keys()){
        if(device_configs[device_index].enabled){
            device_configs[device_index].run = run;
        }
    }
}

void Config::blockSparseSetup()
{
    for(DeviceIndex device_index : device_configs.keys()){
        device_configs[device_index].sparseParams.sparseAllowed = false;
    }
}

