#include "Adc64StartupOptions.h"

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDebug>

#include "mongo/BaseConfig.h"

Adc64StartupOptions::Adc64StartupOptions(const QString &program_type_) :
    StartupOptions(program_type_),
    pcb_ch_option("pcb_ch", "enable switching to PCB ch")
{
    addOption(pcb_ch_option);
}

void Adc64StartupOptions::parse_options()
{
    StartupOptions::parse_options();
    pcb_ch = isSet(pcb_ch_option);
}
