#include "CopyConfigDialog.h"

#include <QCheckBox>
#include <QPushButton>
#include <QRadioButton>

#include "ui_CopyConfigDialog.h"

CopyConfigDialog::CopyConfigDialog(Config *config, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CopyConfigDialog),
    config(config)
{
    ui->setupUi(this);

    QVBoxLayout *formFrom = ui->verticalLayoutCopyFrom;
    QVBoxLayout *formTo = ui->verticalLayoutTo;
    all_keys = config->device_configs.keys();

    for(int i=all_keys.size()-1; i>=0; --i) {
        const DeviceIndex &index = all_keys.at(i);
        QString colorStr = config->device_configs[index].enabled ? "darkGreen" : "#505050";
        QRadioButton *rb = new QRadioButton(index.getSerialStr(), this);
        connect(rb, SIGNAL(toggled(bool)), SLOT(radioButtonToggled(bool)));
        formFrom->addWidget(rb);
        rb->setStyleSheet(QString("QRadioButton {background-color:transparent; color:%1;}").arg(colorStr));
        if(i == (all_keys.size()-1)){  
            bool oldState = rb->blockSignals(true);
            rb->setChecked(true);
            rb->blockSignals(oldState);
            from_key = index;
        }

        QCheckBox *cb = new QCheckBox(index.getSerialStr(), this);
        connect(cb, SIGNAL(toggled(bool)), SLOT(checkBoxToggled(bool)));
        formTo->addWidget(cb);
        cb->setStyleSheet(QString("QCheckBox { background-color:transparent; color:%1; }").arg(colorStr));
        bool oldState = cb->blockSignals(true);
        cb->setChecked(true);
        cb->blockSignals(oldState);

        to_keys.append(index);
    }
}

CopyConfigDialog::~CopyConfigDialog()
{
    delete ui;
}

void CopyConfigDialog::on_pushButtonCopy_clicked()
{
    if(to_keys.isEmpty()){
        QTextCharFormat tf = ui->plainTextEdit->currentCharFormat();
        tf.setForeground(QBrush(Qt::red));
        ui->plainTextEdit->setCurrentCharFormat(tf);
        ui->plainTextEdit->appendPlainText("Copy target is empty");
        return;
    }

    DominoSettings defice_config_from = config->device_configs[from_key];

    for(int i=0; i<to_keys.size(); ++i){
        DeviceIndex devIndex = config->device_configs[to_keys[i]].devIndex;
        quint16 device_id = config->device_configs[to_keys[i]].device_id;
        quint32 device_serial = config->device_configs[to_keys[i]].device_serial;
        QString serial = config->device_configs[to_keys[i]].serial;

        config->device_configs[to_keys[i]] = defice_config_from;
        config->device_configs[to_keys[i]].devIndex = devIndex;
        config->device_configs[to_keys[i]].device_id = device_id;
        config->device_configs[to_keys[i]].device_serial = device_serial;
        config->device_configs[to_keys[i]].serial = serial;

        QTextCharFormat tf = ui->plainTextEdit->currentCharFormat();
        tf.setForeground(QBrush(Qt::darkGreen));
        ui->plainTextEdit->setCurrentCharFormat(tf);
        ui->plainTextEdit->appendPlainText(QString("Copying from %1 to %2 successfully")
                                        .arg(from_key.getSerialStr()).arg(to_keys[i].getSerialStr()));
    }

    ui->plainTextEdit->appendPlainText(QString(" "));

    config->write_config();
}

void CopyConfigDialog::radioButtonToggled(bool checked)
{
    if(!checked) return;
    const auto *rb = qobject_cast<const QRadioButton *>(sender());
    if(rb == nullptr) return;

    QString serial_str = rb->text();
    from_key = get_device_index_by_serial(serial_str);
}

void CopyConfigDialog::checkBoxToggled(bool checked)
{
    const auto *cb = qobject_cast<const QCheckBox *>(sender());
    QString serial_str = cb->text();
    DeviceIndex index = get_device_index_by_serial(serial_str);

    if(checked){
        to_keys.append(index);
    }
    else{
        for(int i=0; i<to_keys.size(); ++i){
            if(to_keys[i] == index){
                to_keys.removeAt(i);
            }
        }
    }
}

DeviceIndex CopyConfigDialog::get_device_index_by_serial(const QString& serial) const
{
    for(int i=0; i<all_keys.size(); ++i){
        if(all_keys[i].getSerialStr() == serial){
            return all_keys[i];
        }
    }
    return DeviceIndex();
}


