//
//    Copyright 2011 Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef DEVICEPROPERTIES_H
#define DEVICEPROPERTIES_H

#include "mregdev/device_id.h"

class DeviceProperties
{
//    Q_OBJECT
public:
//    explicit DeviceProperties(QObject *parent = 0);

//signals:

//public slots:

public:
    DeviceId id = DEVICE_ID_UNKNOWN;
    double temperature = 0;
};

#endif // DEVICEPROPERTIES_H
