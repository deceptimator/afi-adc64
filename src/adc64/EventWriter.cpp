//
//    Copyright 2011 Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "EventWriter.h"

#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QTimer>

#include "EventData.h"
#include "util/QDateTimeCompat.h"

const int dataFileFlushPeriodMs = 5000;

EventWriter::EventWriter(QObject *parent) :
    QObject(parent),
    dataFile(new QFile(this)),
    dataStream(new QTextStream(dataFile)),
    flushTimer(new QTimer(this))
{
    dataFileName = QDir::homePath()+QDir::separator()+"adc64.txt";
    connect(flushTimer, SIGNAL(timeout()), SLOT(flush()));
    flushTimer->start(dataFileFlushPeriodMs);
}

EventWriter::~EventWriter()
{
    flush();
    delete dataStream;
    delete dataFile;
    qDebug()<<"EventWriter destroyed";
}

void EventWriter::setFileName(const QString &str)
{
    lastSize = 0;
    dataFileName = str;
    if (dataFile->isOpen())
        dataFile->close();
    dataFile->setFileName(dataFileName);
    QString state;
    if (dataFile->open(QFile::WriteOnly | QFile::Truncate)) {
        dataStream->setDevice(dataFile);
        state = QString("Data file opened:").append(dataFileName);
        qDebug() << state;
    } else {
        state = QString("Can't open data file:").append(dataFileName);
        qCritical() << state;
    }
    emit writerStatus(state);
}

void EventWriter::close()
{
    dataStream->flush();
    dataFile->close();
}

void EventWriter::flush()
{
    if (dataFile->isOpen()){
        dataFile->flush();
        qint64 curSize = dataFile->size();
        if(lastFlashTime.isValid()){
            auto rate = curSize-lastSize;
            rate /= 1e-3 * lastFlashTime.restart();

            emit writerStatus(QString("%1 (%2MB; %3KB/s)")
                              .arg(dataFileName)
                              .arg(1.*curSize/1024/1024)
                              .arg(rate/1024));
        } else
            lastFlashTime.start();
        lastSize = curSize;
    }
}

void EventWriter::SaveData(const EventData &eventData)
{
    if(!dataFile->isOpen()){
        QString mess = QString("EventWriter: event %1 skipped. Data file isn't opened.").arg(eventData.getEventNumberStr());
        qWarning() << mess;
        emit writerStatus(mess);
        return;
    }
    //    qDebug() << QString("EventWriter: %1 channels").arg(dominoData.Data.size());
    int channelOffset = 0;
    *dataStream << "Event " << eventData.FullEventNumber << " " << QString("%1").arg(QDateTime::currentDateTime().toTime_t()) << "\n";
    Q_FOREACH(const DominoData &dominoData, eventData) {
        Q_FOREACH(int i, dominoData.rawData.keys()) {
            if(!dominoData.rawData[i].empty())     //don't save empty channels
            {
                *dataStream << channelOffset+i+1 << " ";
                for (size_t j=0; j<dominoData.rawData[i].size(); j++)
                {
                    //            DataOut << QString("%1").arg(dominoData.Data[i][j], 0, 'e') << " ";  //save data as double
                    *dataStream << QString("%1").arg(dominoData.rawData[i][j], 0) << " ";  //save data as int
                }
                *dataStream << "\n";
            }
        }
        channelOffset += dominoData.rawData.size();
    }
    qDebug() << QString("EventWriter: event %1").arg(eventData.getEventNumberStr());
}
