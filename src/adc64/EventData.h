//
//    Copyright 2011 Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef EVENTDATA_H
#define EVENTDATA_H

#include <QElapsedTimer>
#include <QMap>
#include <QMetaType>
#include <QString>

#include "adc64-common/dominodata.h"

class EventData : public QMap<quint64, DominoData>
{
public:
    QString getEventNumberStr() const;
    bool eventNumbersMatch() const;
    bool timestampTrigMatch() const;
    void clearFullEventNumber() { FullEventNumber = 0; }

    QElapsedTimer timestamp;
    int FullEventNumber = 0;
};

Q_DECLARE_METATYPE(EventData)

#endif // EVENTDATA_H
