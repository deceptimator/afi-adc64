GIT_VERSION = $$system(sh $$PWD/../git_version.sh 2>/dev/null)
# GIT_VERSION ~= s/-/"."
# GIT_VERSION ~= s/g/""
!isEmpty(GIT_VERSION) {
#    DEFINES += GIT_VERSION=\\\"$$GIT_VERSION\\\"
    message(GIT_VERSION=$$GIT_VERSION )
}
git_version.input = $$PWD/git_version.h.in
git_version.output = $$PWD/git_version.h
QMAKE_SUBSTITUTES += git_version
HEADERS     += $$PWD/git_version.h
QMAKE_CLEAN += $$PWD/git_version.h
OTHER_FILES += $$PWD/git_version.h.in
PRE_TARGETDEPS += $$PWD/git_version.h
