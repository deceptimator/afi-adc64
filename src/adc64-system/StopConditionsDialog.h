#ifndef STOPCONDITIONSDIALOG_H
#define STOPCONDITIONSDIALOG_H

#include "Config.h"

#include "QDialog"

namespace Ui {
class StopConditionsDialog;
}

class StopConditionsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit StopConditionsDialog(Config *config, QWidget *parent = nullptr);
    ~StopConditionsDialog();

private slots:
    void on_evNumLimSpinBox_valueChanged(int limit);

private:
    Ui::StopConditionsDialog *ui;
    Config *config;
};

#endif // STOPCONDITIONSDIALOG_H
