#ifndef WRDAQDIALOG_H
#define WRDAQDIALOG_H

#include "Config.h"
#include "QDialog"

namespace Ui {
class WrDaqDialog;
}

class WrDaqDialog : public QDialog
{
    Q_OBJECT

public:
    explicit WrDaqDialog(Config *config, QWidget *parent = nullptr);
    ~WrDaqDialog();

private slots:
    void on_comboBoxDevice_currentIndexChanged(int index);
    void on_spinBoxBaseLine_valueChanged(int baseline);
    void on_spinBoxLevel_valueChanged(int level);
    void on_radioButtonVer1_toggled(bool checked);
    void on_radioButtonVer2_toggled(bool checked);

private:
    void updateCurrentDevice(quint64 serial);
    void block_gui_signals(bool block);

    Ui::WrDaqDialog *ui;
    Config *config;
    QList<QWidget*> widgets_to_block;
    int curIndex;
    quint64 curSerial;
};

#endif // WRDAQDIALOG_H
