//
//    Copyright 2013 Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef GENERICHARDWARE_H
#define GENERICHARDWARE_H

#include <QObject>
#include "regio/regop.h"

class QTimer;
class QtMregDevice;

class GenericHardware : public QObject
{
    Q_OBJECT
public:
    explicit GenericHardware(QtMregDevice *device);
    virtual ~GenericHardware();
    virtual void readStatus() = 0;
    virtual void clearStatistics() = 0;
    virtual void readStatistics() = 0;
    QString getAddressStr() const;
    QString getDeviceInfoStr() const;
    bool isConnected() const;

protected slots:
    virtual void writeSetup() = 0;
    virtual void updateStatus() = 0;
    virtual void on_deviceConnected();
    virtual void on_deviceDisconnected();

signals:
    void log(const QString &);

protected:
    inline quint32 get32(const mlink::RegOpVector &rx, int i){
        return rx.at(2*i).data | (quint32(rx.at(2*i+1).data)<<16);
    }

    QtMregDevice * const dev;
    bool connectedState;
    QTimer * const updateStatusTimer;
};

#endif // GENERICHARDWARE_H
