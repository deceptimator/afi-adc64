//
//    Copyright 2013 Ilja Slepnev
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QString>
#include <QStringList>
#include <QList>
#include <QMainWindow>
#include <QWidgetAction>

#include "pnp-server/PNPMessage.h"
#include "mongo/StartupOptions.h"
#include "configuration-manager/ConfigurationManager.h"
#include "Config.h"
#include "mstream-lib/MStreamDump.h"
#include "mldiscover/MldpListener.h"
#include "m-stream-output/StatisticOutput.h"

class QTableWidget;
class QTimer;
class QElapsedTimer;

class AdcStatus;
class DominoDevice;

class RemoteControlServer;
class RemoteControlServerStatus;
class RcRunId;
class DiscoverDialog;
class DeviceDescription;
class DeviceIndex;
class PNPServer;
class DeviceSettingsDialog;
class EvNumChecker;
class DevFwRevMap;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(StartupOptions &startup_options, QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void addLog(QtMsgType msgType, const QString &log, const QString &threadName);

private slots:
    void init();

    // Gui actions
    void on_actionWR_DAQ_triggered();
    void on_actionFindDevices_triggered();
    void on_actionClear_statistics_triggered();
    void on_actionAllow_Remote_Control_toggled(bool checked);
    void on_actionEnable_write_toggled(bool enabled);
    void on_actionShow_details_triggered(bool show);
    void on_actionSet_stop_conditions_triggered();
    void on_actionStop_run_on_TrigOnXOff_toggled(bool trig_on_xoff_stop);
    void on_actionShow_device_settings_triggered();
    void on_actionSet_MTU_size_triggered();

    void on_actionAbout_triggered();
    void on_radioButtonStart_toggled(bool checked);

    // Other actions
    void onRemoteStartRequest(const RcRunId &runId, quint64 start_event_number=0);
    void onRemoteStopRequest(const RcRunId &runId, quint64 start_event_number=0);
    void adcConnected();
    void adcDisconnected();
    void adcStateChanged(const DeviceIndex &index, bool online);
    void updateAdcStatus(const DeviceIndex &index, const AdcStatus &status);

    void updateDeviceDiscoverInfo(const DeviceDescription &dd);
    void adcSetupWritten(const DominoSettings &s);
    void updateRemoteStatus();
    void remoteStatusUpdated(const RemoteControlServerStatus &status);
    void sendRunError(QString mess=QString());

    void flushMStreamStats();
    void flushWarnings();

    void switch_to(QString program_index, QString configuration_name);
    void on_actionConfiguration_manager_triggered();
    void on_actionSynchronize_configs_with_adc64_triggered();

signals:
    void deleteHardware(const DeviceIndex &index);
    void deleteAll();
    void clearStatistics(const DeviceIndex &index);
    void writeAdcSetup(const DominoSettings &setup);
    void remoteCmdCompleted(bool success, QString cmd, int arg=0);
    void remoteStartCompleted(bool success, const RcRunId &runId);
    void remoteStopCompleted(bool success, const RcRunId &runId);
    void sendRemoteState(QString state, bool ready4Run);
    void runError(const RcRunId &runId, QString errStr);
    void runWarning(const RcRunId &runId, QString warnStr);
    void allowRemoteControl(bool);
    void programDescriptionUpdated(const ProgramDescription &);
    void closeProgram(const ProgramDescription &);
    void config_transfer_to_rc(QByteArray data);
    void msdump_reset(DeviceIndex index, bool bigFragId, quint16 hwBufSize, quint16 ackSize, QString fn=QString(), bool doStatClear = false);
    void mstream_send_connect(DeviceIndex index);
    void set_run_state(bool run);

private:
    void closeEvent(QCloseEvent *event);
    void addDevices();
    void removeAllDevices();
    void setRunState(bool run);

    void setTableLabels();
    void adjustTableHeight(QTableWidget *table);

    void addMStreamDump(const DeviceIndex &index);
    void resetMStream(const DeviceIndex &index, bool doStatClear = false);
    void sendAdcSetup(const DeviceIndex &index);
    bool resetAll(bool doResetMStream = true, bool doEvNumCheck = false, bool doStatClear = false);
    QStringList hostStrList() const;
    DeviceIndex getIndexByRow(int row) const;
    int getRowByIndex(const DeviceIndex &index) const;
    void updateProgramDescription();
    bool readyForRun(bool printErr=false) const;
    bool waitDevSetupKey(const QElapsedTimer &timer, int timeout=1000);
    bool waitDevEvNum(const QElapsedTimer &timer, int timeout=1000);
    bool phasingStop();
    void block_gui_signals(bool block);
    QString getMStreamFileName(DeviceIndex index) const;

    Ui::MainWindow * const ui;

    Config config;
    QMap<DeviceIndex, bool> synch_result;
    ConfigurationManager *configuration_manager;
    QList<QObject*> objects_to_block;
    QMap<DeviceIndex, DominoDevice *> devices;
    QMap<DeviceIndex, int> deviceSetupKeys;
    QMap<DeviceIndex, QThread *> adcThreads;
    QMap<DeviceIndex, int> rowIndexMap; // [cfgIndex] = deviceIndex
    QMap<DeviceIndex, MStreamDump *> dumpers;
    QMap<DeviceIndex, QThread *> dumpersThread;
    QMap<DeviceIndex, AdcStatus> lastAdcStatus;
    RemoteControlServer * const remoteControl;
    PNPServer * const pnpServer;
    ProgramDescription program_description;
    DiscoverDialog * const discoverDialog;
    int setupKey;
    QMap<DeviceIndex, QMap<QString, QString> > mstreamStats;
    QTimer * const statFlushTimer;
    QTimer * const warnFlushTimer;
    DeviceSettingsDialog * const deviceSettingsDialog;
    EvNumChecker * const evNumChecker;
    bool debug_mode;
};


#endif // MAINWINDOW_H
