TEMPLATE = app
QT += core gui widgets
QT += sql network

isEmpty(EXTDIR) {
    EXTDIR = $$PWD/../../external
}

CONFIG += device-discover lib-common mlinkip mongo qwt qxw
CONFIG += mstream-lib pnp-server remote-control-server

include($$EXTDIR/libs.pri)

INCLUDEPATH *= $$PWD/..
include(../adc64-common/adc64-common.pri)
include(adc64-system.pri)

target.path = /bin
INSTALLS += target
TARGET = afi-adc64-system
