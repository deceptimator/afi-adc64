#include "StopConditionsDialog.h"
#include "ui_StopConditionsDialog.h"


StopConditionsDialog::StopConditionsDialog(Config *config, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::StopConditionsDialog),
    config(config)
{
    ui->setupUi(this);
    ui->evNumLimSpinBox->setValue(config->event_number_limit);
}

StopConditionsDialog::~StopConditionsDialog()
{
    delete ui;
}

void StopConditionsDialog::on_evNumLimSpinBox_valueChanged(int limit)
{
    config->event_number_limit = limit;
}
