#ifndef CONFIG_H
#define CONFIG_H

#include "mongo/BaseConfig.h"
#include "mongo/StartupOptions.h"
#include "mongo/RootConfig.h"
#include "adc64-common/dominosettings.h"
#include "base/RcRunId.h"

#include "QString"
#include "QMap"

class Config : BaseConfig
{
public:
    Config(StartupOptions &startup_options);
    void read_config();
    void write_config();
    RootConfig update_root();
    QMap<DeviceIndex, bool> synchronize_with_adc64();
    void setMStreamSetup();
    QString program_type;
    QString program_index;
    QString configuration_name;
    RcRunId runId;
    bool run;
    QString run_time_str;
    bool allow_remote_control;
    bool write_enable;
    QString storage_path;
    bool show_details;
    quint64 event_number_limit;
    quint16 mtu_size;
    bool trig_on_xoff_stop;
    QMap<DeviceIndex, DominoSettings> device_configs;
    QMap<DeviceIndex, bool> synch_result;
    RootConfig default_root;
    RootConfig root;

signals:
    void synchronization_result(const DeviceIndex &index, bool result);
    void test();
};

#endif // CONFIG_H
