#ifndef DEVICESETTINGSDIALOG_H
#define DEVICESETTINGSDIALOG_H

#include <QDialog>
#include "adc64-common/dominosettings.h"

class QListWidgetItem;
namespace Ui {
class DeviceSettingsDialog;
}

class DeviceSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DeviceSettingsDialog(QWidget *parent = nullptr);
    ~DeviceSettingsDialog();
    void deviceListUpdated(QMap<DeviceIndex, DominoSettings> s);
    void setMStreamWrite(QString path);
    void setMtuSize(int mtu);
    void setStopConditions(int limit);

private slots:
    void on_listWidgetDevices_currentItemChanged(QListWidgetItem *current, QListWidgetItem *);

private:
    QString getDevName(const DeviceIndex &index) const;

    Ui::DeviceSettingsDialog *ui;
    QMap<DeviceIndex, DominoSettings> devices;
};

#endif // DEVICESETTINGSDIALOG_H
