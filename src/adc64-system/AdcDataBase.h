#ifndef ADCDATABASE_H
#define ADCDATABASE_H

#include "daq-config/DaqDataBase.h"
#include "mregdev/fw_version.h"
#include "mldiscover/DeviceIndex.h"
#include "Config.h"

#include "QMap"

class QSqlDatabase;
class DaqDataBase;
class SystemSetup;
//class DspParams;
//class FirParams;
//class SparseParams;
//class ad5622operation;
//class DominoSettings;
class DeviceIndex;

class DevFwRevMap : public QMap<DeviceIndex, fw_version_t>{};
Q_DECLARE_METATYPE(DevFwRevMap)

class AdcDataBase : public DaqDataBase
{
    Q_OBJECT

public:
    AdcDataBase(QObject *parent = nullptr);

signals:
    void runWarning(QString runIndex, int runNumber, QString warnMsg);

private slots:
    bool saveRunToDB(QString runIndex, uint runNumber, Config *config, const DevFwRevMap &adcFwRev);

private:
    bool saveCfgToDB(Config *config, QMap<DeviceIndex, int> *adcCfgIdMap = nullptr);
    int saveDspConfig(QSqlDatabase &db, const DspParams &dspParams) const;
    int saveFirConfig(QSqlDatabase &db, const FirParams &fir) const;
    int saveSparseConfig(QSqlDatabase &db, const SparseParams &sparse) const;
    int saveAmpConfig(QSqlDatabase &db, quint16 amp_board_set, const ad5622operation &ad5622op) const;
    int saveChConfig(QSqlDatabase &db, const DominoSettings &ds) const;
    int saveGeneralConfig(QSqlDatabase &db, const DominoSettings &ds, int chId, int dspId, int sparseId, bool hasAmp, int ampId) const;
};

#endif // ADCDATABASE_H
