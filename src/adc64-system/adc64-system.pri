
HEADERS += \
    $$PWD/Config.h \
    $$PWD/MainWindow.h \
    $$PWD/WrDaqDialog.h \
    $$PWD/StopConditionsDialog.h \
    $$PWD/DeviceSettingsDialog.h \
    $$PWD/AdcDataBase.h

SOURCES += \
    $$PWD/Config.cpp \
    $$PWD/adc64-system.cpp \
    $$PWD/MainWindow.cpp \
    $$PWD/WrDaqDialog.cpp \
    $$PWD/StopConditionsDialog.cpp \
    $$PWD/DeviceSettingsDialog.cpp \
    $$PWD/AdcDataBase.cpp

FORMS += \
    $$PWD/MainWindow.ui \
    $$PWD/WrDaqDialog.ui \
    $$PWD/StopConditionsDialog.ui \
    $$PWD/DeviceSettingsDialog.ui

RESOURCES += \
    adc64-system.qrc
