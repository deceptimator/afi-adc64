#include "WrDaqDialog.h"
#include "ui_WrDaqDialog.h"

WrDaqDialog::WrDaqDialog(Config *config, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WrDaqDialog),
    config(config)
{
    ui->setupUi(this);

    widgets_to_block.append(ui->radioButtonVer1);
    widgets_to_block.append(ui->radioButtonVer2);
    widgets_to_block.append(ui->spinBoxBaseLine);
    widgets_to_block.append(ui->spinBoxLevel);

    foreach (const DeviceIndex &index, config->device_configs.keys()) {
        const DominoSettings &ds = config->device_configs[index];
        ui->comboBoxDevice->insertItem(ui->comboBoxDevice->count(),
                                       QString("0x%1").arg(ds.devIndex.getSerial(),
                                       8,
                                       16,
                                       QChar('0')),
                                       ds.devIndex.getSerial());
    }
    ui->comboBoxDevice->setCurrentIndex(0);
}

WrDaqDialog::~WrDaqDialog()
{
    delete ui;
}

void WrDaqDialog::on_comboBoxDevice_currentIndexChanged(int index)
{
    bool ok;
    quint64 serial = ui->comboBoxDevice->itemData(index).toULongLong(&ok);
    Q_ASSERT(ok && serial!=0);
    updateCurrentDevice(serial);
}

void WrDaqDialog::updateCurrentDevice(quint64 serial)
{
    curSerial = serial;
    block_gui_signals(true);
    foreach (const DeviceIndex &index, config->device_configs.keys()) {
        if(index.getSerial() == serial){
            const DominoSettings &setup = config->device_configs[index];
            if(setup.amp_board_set==AMP_BOARD_AD5622v2){
               ui->radioButtonVer2->toggle();
            }
            else{
                ui->radioButtonVer1->toggle();
            }
            ui->spinBoxBaseLine->setValue(setup.ad5622op.data_baseline);
            ui->spinBoxLevel->setValue(setup.ad5622op.data_level);
            block_gui_signals(false);
            return;
        }
    }

    Q_ASSERT(false);
}

void WrDaqDialog::on_spinBoxBaseLine_valueChanged(int baseline)
{
    foreach (const DeviceIndex &index, config->device_configs.keys()) {
        if(index.getSerial() == curSerial){
            config->device_configs[index].ad5622op.data_baseline = baseline;
        }
    }
    config->write_config();
}

void WrDaqDialog::on_spinBoxLevel_valueChanged(int level)
{
    foreach (const DeviceIndex &index, config->device_configs.keys()) {
        if(index.getSerial() == curSerial){
            config->device_configs[index].ad5622op.data_level = level;
        }
    }
    config->write_config();
}

void WrDaqDialog::on_radioButtonVer1_toggled(bool checked)
{
    foreach (const DeviceIndex &index, config->device_configs.keys()) {
        if(index.getSerial() == curSerial){
            config->device_configs[index].amp_board_set = checked ? AMP_BOARD_AD5622 : AMP_BOARD_AD5622v2;
        }
    }
    config->write_config();
}

void WrDaqDialog::on_radioButtonVer2_toggled(bool checked)
{
    foreach (const DeviceIndex &index, config->device_configs.keys()) {
        if(index.getSerial() == curSerial){
            config->device_configs[index].amp_board_set = !checked ? AMP_BOARD_AD5622 : AMP_BOARD_AD5622v2;
        }
    }
    config->write_config();
}

void WrDaqDialog::block_gui_signals(bool block)
{
    for(int i=0; i<widgets_to_block.size(); ++i){
        widgets_to_block[i]->blockSignals(block);
    }
}
