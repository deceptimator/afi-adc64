#ifndef DOMINODEVICEHMCAD1101_H
#define DOMINODEVICEHMCAD1101_H

#include "dominodevice.h"

class DominoDeviceHMCAD1101 : public DominoDevice
{
public:
    DominoDeviceHMCAD1101(quint16 devId, quint64 devSerial, QObject *parent = nullptr);

protected:
    int getDeserializeNum() const override { return 8; }
    void setupAdc() override;
    QMap<int, bool> patternTestByChannel() override;
    void setCustomPatternMode(bool mode) override;
    void setCustomPattern(quint16 data) override;

private:
//    bool ramp_test();
    void spi_write(quint8 addr, quint16 data);
};

#endif // DOMINODEVICEHMCAD1101_H
