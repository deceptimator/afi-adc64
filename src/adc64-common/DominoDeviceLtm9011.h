#ifndef DOMINODEVICELTM9011_H
#define DOMINODEVICELTM9011_H

#include "dominodevice.h"

class DominoDeviceLtm9011 : public DominoDevice
{
public:
    DominoDeviceLtm9011(quint16 devId, quint64 devSerial, QObject *parent = nullptr);

protected:
    int getDeserializeNum() const override { return 4; }
    void setupAdc() override;
    QMap<int, bool> patternTestByChannel() override;
    void setCustomPatternMode(bool mode) override;
    void setCustomPattern(quint16 data) override;
};

#endif // DOMINODEVICELTM9011_H
