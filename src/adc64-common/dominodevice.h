#ifndef DOMINODEVICE_H
#define DOMINODEVICE_H

#include <QObject>
#include <QTimer>
#include <QElapsedTimer>

#include "AdcStatus.h"
#include "cellcalibration.h"
#include "dominodata.h"
#include "dominosettings.h"
#include "mregdev/qtmregdevice.h"

class DominoDevice : public QtMregDevice
{
    Q_OBJECT
public:
    Q_DISABLE_COPY(DominoDevice)
    typedef enum {TRIG_NEGATIVE = 0, TRIG_POSITIVE = 1} TrigPolarity;
    DominoDevice(quint16 devId, quint64 devSerial, QObject *parent = nullptr);
    ~DominoDevice() override;
    bool checkLiveMagic() override;
    int getNumberOfChannels() const;
    double getBaseStepNs() const;

public Q_SLOTS:
    void on_setupChanged(const DominoSettings &s);
    void on_deserializeDelayChanged(int clockDelay, int dataDelay);
    void clearEventNumber() { dominoData.EventNumber=0; }
    void clearStatistics(const DeviceIndex &index); // check me
    void evNumRequest(int checkIndex); // check me

protected:
    bool isRunning();
    quint16 encodeChCtrlReg(const DominoSettings &setup, int i) const;
    void writeChCtrl(int ch, const DominoSettings &setup);
    void ChnSetThreshold(int ch, int thr);
    void ChnSetZsThreshold(int ch, int thr);
    void ChnSetBaseline(int ch, int baseline);
    void WritePCAReg(const reg_pca_struct &reg_pca);
    void WriteAD5622Command(const ad5622operation &ad5622op);
    void SetAmpBoard(const quint16 &board);

    void RestartRun();
    void StartRun();
    void StopRun();
    DigitizerTriggerStatus readTrigStatus();

    void ReadAllChMem();
    void writeSettings();
    void WriteTrigSetup(reg_trig_ctrl trigsetup);
    void ReadChMem(int ch);
    quint16 ReadChReg(int ch, quint16 addr);
    void WriteChReg(int ch, quint16 addr, quint32 data);
    void SetTrigDelay(qint16 delay);
//    void DetectNumberOfAdc64boards();
    bool testChannelRegisters();
    void ChangeAdcClkDelay(quint16, quint16);
    void ResetAdcClkDelay();
    void ResetAdc();
    void ResetAdcStatus();
    void ResetAdcSync();
    void ResetAdcPin();
    QVector<bool> patternTest();
    QMap<int, bool> TestAdcCustomPatternByChannel(const quint16 &pattern);
    void readOutD2Hist();

    quint16 lockTest();
    bool isLockOk(quint16 des_status) const;
    void setAdcTime();
    void detectChannelMaxSampleCount();
    void writeSparseParams();
    void writeFirParams();

    static int FindTruePlateauCenter(const QVector<bool> &);
    QString adcStatusStr(int status);
    virtual int getDeserializeNum() const { return 0; }
    virtual void setupAdc() {}
    virtual QMap<int, bool> patternTestByChannel();
    virtual void setCustomPatternMode(bool) {}
    virtual void setCustomPattern(quint16) {}

    bool readAdcInfo();

protected Q_SLOTS:
    void statusPoll() override;
    void on_deviceConnected() override;
    void on_deviceDisconnected() override;
    void pollData();
    bool FindAdcClkDelay();
    void clearStatistics_impl();

Q_SIGNALS:
    void deviceInfoUpdated(DeviceIndex index, int chNum, int maxSampleCnt, bool blockOffset);
    void dataAcquired(DominoData);
    void adcLockStatusUpdated(DeviceIndex, bool ok, quint8 locked);
    void adcRampStatusUpdated(DeviceIndex, bool ok, quint8 rampError);
    void statusUpdated(const DeviceIndex &devIndex, const AdcStatus &);
    void setupWritten(const DominoSettings &s);
    void evNumResponse(int checkIndex, quint64 serial,quint64 evNum);
    void adcClkDelayUpdated(DeviceIndex, AdcClkStatus);

protected:
    QTimer * const pollDataTimer; // create without parent
    DominoData dominoData;
    DominoSettings setup;
    DominoSettings settingsCache;
    bool settingsCacheValid = false;
    bool saveRunState = false;
    bool adcCalibrationStatus = false;
    quint32 maxSampleCount = 0;
    QElapsedTimer lastStatusTimestamp;
    QVector<quint16> spiPatternSet;
    AdcStatus status;
};

#endif // DOMINODEVICE_H
