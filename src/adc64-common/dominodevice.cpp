
#include "dominodevice.h"

#include <cmath>
#include <iostream>
#include <stdexcept>

#include <QDebug>
#include <QDir>
#include <QString>
#include <QTemporaryFile>
#include <QThread>
#include <QVector>

#include "DominoDeviceRegisters.h"
#include "device_features.h"
#include "dominodata.h"
#include "mregdev/mregdevice.h"
#include "regio/mlinkdevice.h"
#include "regio/regop.h"
#include "util/QDateTimeCompat.h"

using mlink::RegOpVector;
using mlink::RegWrite;
//using mlink::RegRead32;
using mlink::RegRead64;

namespace {
//const int ADC_CHANNELS = 64;
//const int MAX_MEMRD_SIZE = 100;       //in 32bit words

const int POLL_TIME_MS = 10;
const uint D2_HIST_TIMEOUT_MS = 5000;
const int SEND_STATUS_TIMEOUT = 100; // in ms

enum { BOARD_REG_BITS = 13 };
const quint16 BOARD_REG_COUNT = (1<<BOARD_REG_BITS);

quint32 chBaseMemAddr(quint32 ch)
{
    return ch << 14;
}

}


DominoDevice::DominoDevice(quint16 devId, quint64 devSerial, QObject *parent)
    : QtMregDevice(devId, devSerial, parent),
      pollDataTimer(new QTimer(this)),
      setup(DominoSettings())
{
    connect(pollDataTimer, SIGNAL(timeout()), SLOT(pollData()));
    pollDataTimer->setSingleShot(true);
    pollDataTimer->start(POLL_TIME_MS);
    detectChannelMaxSampleCount();
}

DominoDevice::~DominoDevice()
{
    disable();
}

int DominoDevice::getNumberOfChannels() const
{
    int nch;
    switch (getDeviceId()) {
    case DEVICE_ID_BPM4100: nch = 4; break;

    case DEVICE_ID_ADC8E:
    case DEVICE_ID_8CADC:
    case DEVICE_ID_EVADC:
        nch = 8;
        break;

    case DEVICE_ID_TQDC16VS_DIG:
    case DEVICE_ID_TQDC16VS_E:
    case DEVICE_ID_ADCM16:
    case DEVICE_ID_ADC16LTCM:
//    case DEVICE_ID_ADC16LTCS:
    case DEVICE_ID_ADCM163:
        nch = 16;
        break;

    case DEVICE_ID_ADC32P: nch =  32; break;

    case DEVICE_ID_ADC64:
    case DEVICE_ID_ADC64S2:
    case DEVICE_ID_ADC64WR:
    case DEVICE_ID_ADC64VE:
    case DEVICE_ID_ADC64VE_XGE:
    case DEVICE_ID_ADC64ECAL:
        nch =  64; break;

    default: nch = 1; break;
    }
    return nch;
}

double DominoDevice::getBaseStepNs() const
{
    double step = 16.;
    switch (getDeviceId()) {
    case DEVICE_ID_TQDC16VS_E: step = 8.; break;
    case DEVICE_ID_ADCM163: step = 20; break;
    default: step = 16; break;
    }
    return step;
}

bool DominoDevice::readAdcInfo()
{
    if (!isConnected() || !hasAdcInfo(status.fw_ver)) return false;
    quint16 var = regRead(REG_ADC_INFO);
    status.adcBits = 10+2*(var&0x3);
    status.adcPeriod = (var & 0x4) ? 10 : 16;
    return true;
}

void DominoDevice::on_deviceConnected()
{
    QtMregDevice::on_deviceConnected();
    status.fwStr = getFirmwareVersionStr();
    status.fw_ver = getDeviceFwVersion();
    status.mstreamVer2 = hasMstream22(status.fw_ver);
    status.msMultiAck = hasMultiAckSupport();
    status.hwBufSize = getHwBufSize();
    readAdcInfo();

    settingsCacheValid = false;
    status.adcSelfTestOk = false;
    testChannelRegisters();

    setupAdc();

    for(int ch=0; ch<getNumberOfChannels();++ch) {
        WriteChReg(ch, MEM_CH_D2_HIST_CTRL, MEM_CH_D2HIST_CTRL_BIT_CLR);
        WriteChReg(ch, MEM_CH_D2_HIST_TIME, D2_HIST_TIMEOUT_MS*125000);
        WriteChReg(ch, MEM_CH_D2_HIST_CTRL, MEM_CH_D2HIST_CTRL_BIT_EN);
    }

    if(getDeviceId() == DEVICE_ID_ADC64S2){
        setAdcTime();
    }
    detectChannelMaxSampleCount();
    emit deviceInfoUpdated(getDeviceIndex(), getNumberOfChannels(),
                           maxSampleCount, hasStreamRead(status.fw_ver));
}

void DominoDevice::on_deviceDisconnected()
{
    QtMregDevice::on_deviceDisconnected();
}

QString DominoDevice::adcStatusStr(int status)
{
    quint8 adc_locked = status & 0xFF;
    quint8 adc_ramp_error = (status >> 8) & 0xFF;
    return QString("ADC status: error=%1, lock=%2").arg(adc_ramp_error, 8, 2, QChar('0')).arg(adc_locked, 8, 2, QChar('0'));
}

bool DominoDevice::checkLiveMagic()
{
    bool liveMagicCorrect = QtMregDevice::checkLiveMagic();
    if(isConnected() && !liveMagicCorrect){
        settingsCacheValid = false;
        ++status.liveMagicResetCount;
//        setupAdc();
        on_deviceConnected();
        writeSettings();
    }
    return liveMagicCorrect;
}

void DominoDevice::setAdcTime()
{
    quint64 val = QDateTime::currentMSecsSinceEpoch();
    val /= 1000;
    ++val;
    regWrite64(REG_ADC_TIME_SEC, val);
}

void DominoDevice::detectChannelMaxSampleCount()
{
    if(isConnected() && getDeviceFwVersion() >= fw_version_t(1, 0, 21203)) {
        quint32 s = regRead(REG_CH_DPM_KS);
        maxSampleCount = s<<10;
    } else {
        switch (getDeviceId()) {
        case DEVICE_ID_TQDC16VS_DIG:
            maxSampleCount = 0x2000; // 8k
            break;
        case DEVICE_ID_TQDC16VS_E:
            maxSampleCount = 0x2000; // 8k
            break;
        case DEVICE_ID_ADC64:
            maxSampleCount = 0x0800; // 2k
            break;
        case DEVICE_ID_ADC64S2:
            maxSampleCount = 0x2000; // 8k
            break;
        case DEVICE_ID_ADC64WR:
            maxSampleCount = 0x2000; // 8k
            break;
        case DEVICE_ID_ADC64VE:
        case DEVICE_ID_ADC64VE_XGE:
        case DEVICE_ID_ADC64ECAL:
            maxSampleCount = 0x1000; // 4k
            break;
        case DEVICE_ID_ADC8E:
            maxSampleCount = 0x2000; // 8k
            break;
        case DEVICE_ID_ADCM163:
            maxSampleCount = 0x1000; // 4k
            break;
        default:
            maxSampleCount = 0;
            break;
        }
    }
}

void DominoDevice::writeSparseParams()
{
    RegOpVector regOp;
    if (!settingsCacheValid || settingsCache.sparseParams.offset != setup.sparseParams.offset){
        if(!hasStreamRead(status.fw_ver))
            regOp << RegWrite(REG_MSTREAM_SPARSE_OFFSET, setup.sparseParams.offset/2);
    }
    if (!settingsCacheValid || settingsCache.sparseParams.period != setup.sparseParams.period){
        quint16 period = setup.sparseParams.period;
        if(!hasStreamRead(status.fw_ver))
            period /= 2;
        regOp << RegWrite(REG_MSTREAM_SPARSE_PERIOD, period);
    }
    if (!settingsCacheValid || settingsCache.sparseParams.en != setup.sparseParams.en ||
            settingsCache.sparseParams.pointNumber != setup.sparseParams.pointNumber){
        quint16 ctrlVal = setup.sparseParams.pointNumber;
        ctrlVal = qMin(ctrlVal, quint16(3));
        ctrlVal <<= 1;
        if(setup.sparseParams.en && setup.sparseParams.sparseAllowed)
            ctrlVal |= SPARSE_BIT_EN;
        regOp << RegWrite(REG_MSTREAM_SPARSE_CTRL, ctrlVal);
    }
    regOpExec(regOp);
}

void DominoDevice::writeFirParams()
{
    RegOpVector regOp;
    regOp << RegWrite(REG_FIR_CONTROL, setup.dspParams.getFirEnable() ? FIR_ENABLE_BIT : 0);
    regOp << RegWrite(REG_FIR_ROUNDOFF, setup.dspParams.fir.roundoff);
    for (int i=0; i<16; i++) {
        regOp << RegWrite(REG_FIR_COEF_START+i, setup.dspParams.fir.coef[i]);
    }
    regOp << RegWrite(REG_FIR_COEF_CTRL, 1);
    regOp << RegWrite(REG_FIR_COEF_CTRL, 0);
    regOpExec(regOp);
}

void DominoDevice::statusPoll()
{
    QtMregDevice::statusPoll();
    if (isConnected()) {
        if (!lastStatusTimestamp.isValid() || lastStatusTimestamp.elapsed() > SEND_STATUS_TIMEOUT) {
            lockTest();
            lastStatusTimestamp.start();
            status.temp = getTemperature();
            status.bmStatus = readBMStatus();
            status.evNum = regRead32(REG_RUN_EVENT_NUMBER);
            WrEpStatus &wrEpStatus = status.wrEpStatus;
            wrEpStatus.hasWrEpStatus = hasWrEpStatus();
            if (wrEpStatus.hasWrEpStatus) {
                wrEpStatus = readWrEpStatus();
            } else {
                wrEpStatus.wrSyncFail = regRead32(REG_WR_SYNC_LOST_COUNTER);
                wrEpStatus.wrLinkError = regRead32(REG_WR_LINK_ERROR_COUNTER);
            }

            status.trigOnXOff = regRead32(REG_TRIG_ON_XOFF_ERROR_COUNTER);
            status.adcStatus = regRead32(REG_ADC_STATUS);
//            quint16 adcStatusMask = regRead(REG_ADC_STATUS_MASK);
//            qDebug()<<hex<<"adcStatusMask="<<adcStatusMask<<bin<<adcStatusMask;

//            quint16 adcStatus = dev->regRead(REG_ADC_CLK_STATUS);
//            qDebug()<<hex<<"adcStatus="<<adcStatus<<bin<<adcStatus;
            emit statusUpdated(getDeviceIndex(), status);
            readOutD2Hist();
        }
    }
}

bool DominoDevice::testChannelRegisters()
{
    if (!isConnected())
        return false;
    // Channel registers test
    bool test_ok = true;
    QMap<quint16, quint16> m;
    for(int c=0; c<2; c++)
        for(int i=0; i<8; i++) {
            if (i == MEM_CH_WR_ADDR) continue; // read-only register
            if (i == MEM_CH_ADC_DATA) continue; // read-only register
            if (i == MEM_CH_ADC_PATTERN_MISMATCH_CNT) continue; // read-only register
            quint16 a = c*8+i;
            quint16 d = 1+a;
            m[a] = d;
            WriteChReg(c, i, d);
            //            qDebug() << QString("Write channel %1 reg %2 = %3").arg(c).arg(i).arg(d);
        }
    for(int c=0; c<2; c++)
        for(int i=0; i<8; i++) {
            quint16 a = c*8+i;
            quint32 d = ReadChReg(c, i);
            bool ok = (d == m[a]);
            if (i == MEM_CH_WR_ADDR) ok = true; // read-only register
            if (i == MEM_CH_ADC_DATA) ok = true; // read-only register
            if (i == MEM_CH_ADC_PATTERN_MISMATCH_CNT) ok = true; // read-only register
            if (!ok) test_ok = false;
            if (!ok)
                qWarning() << QString("Read channel %1 reg %2 = %3, expected %4").arg(c).arg(i).arg(d).arg(m[a]);
            //            else
            //                qDebug() << QString("Read channel %1 reg %2 = %3").arg(c).arg(i).arg(d);
        }
    if (test_ok)
        qDebug() << "Channel registers test Ok";
    else
        qWarning() << "Channel registers test FAILED";

    return test_ok;
}

void DominoDevice::on_setupChanged(const DominoSettings &s)
{
    if(s.devIndex == getDeviceIndex()){
        setup = s;
        if (isConnected())
            writeSettings();
    }
}

void DominoDevice::on_deserializeDelayChanged(int clockDelay, int dataDelay)
{
    ResetAdcClkDelay();
    ChangeAdcClkDelay(0x5555, clockDelay);
    ChangeAdcClkDelay(0xAAAA, dataDelay);

    ResetAdc();
    lockTest();
}

void DominoDevice::writeSettings()
{
    try {
        if(setup.mstreamEnable && !setup.run) {
            StopRun();
        }

        for(int i=0; i<setup.nch; i++){
            if (!settingsCacheValid
                    || (encodeChCtrlReg(settingsCache, i) != encodeChCtrlReg(setup, i))
                    || (settingsCache.dspParams.blc_thr != setup.dspParams.blc_thr)
                    ) {
                writeChCtrl(i, setup);
            }
        }

        if (!settingsCacheValid || (settingsCache.chEn != setup.chEn)){
            quint64 readout_channel_en_mask = 0;
            for(int i=0; i<setup.nch; i++) {
                if (setup.chEn.value(i))
                    readout_channel_en_mask |= ((quint64)0x1 << i);
            }
            regWrite64(REG_MSTREAM_READOUT_CHANNEL_EN, readout_channel_en_mask);
            //                qDebug()<<"Channel mask:"<<hex<<readout_channel_en_mask;
        }

        if (!settingsCacheValid || settingsCache.chTrigThr != setup.chTrigThr) {
            for(int i=0; i<setup.nch; i++)
                ChnSetThreshold(i, setup.chBaseline.value(i) + setup.chTrigThr.value(i));
//                ChnSetThreshold(i, setup.thr.value(i));
        }
        if (!settingsCacheValid || settingsCache.chBaseline != setup.chBaseline) {
            for(int i=0; i<setup.nch; i++)
                ChnSetBaseline(i, setup.chBaseline.value(i));
        }

        if (!settingsCacheValid || settingsCache.trigsetup.all != setup.trigsetup.all)
            WriteTrigSetup(setup.trigsetup);

        if (!settingsCacheValid || settingsCache.trigDelay != setup.trigDelay)
            SetTrigDelay(setup.trigDelay);

        if (0
                || (getDeviceId() == DEVICE_ID_ADC64)
                || (getDeviceId() == DEVICE_ID_ADC64S2)
                || (getDeviceId() == DEVICE_ID_ADC64WR)
                || (getDeviceId() == DEVICE_ID_ADC64VE)
                || (getDeviceId() == DEVICE_ID_ADC64VE_XGE)
                ) {

            if(!settingsCacheValid || settingsCache.amp_board_set != setup.amp_board_set)
                SetAmpBoard(setup.amp_board_set);

            switch (setup.amp_board_set) {
            case AMP_BOARD_PCA: {
                if (!settingsCacheValid || settingsCache.amp_board_set != setup.amp_board_set
                        || settingsCache.reg_pca.all != setup.reg_pca.all)
                    WritePCAReg(setup.reg_pca);
                break;
            }
            case AMP_BOARD_AD5622:
                // fall through
            case AMP_BOARD_AD5622v2:
            {
                if (!settingsCacheValid || (settingsCache.amp_board_set != setup.amp_board_set)
                        || (settingsCache.ad5622op.data_baseline != setup.ad5622op.data_baseline)
                        || (settingsCache.ad5622op.data_level != setup.ad5622op.data_level))
                    WriteAD5622Command(setup.ad5622op);
                break;
            }
            }
        }

        for(int i=0; i<setup.nch; i++)
            if(!settingsCacheValid ||
                    settingsCache.chZsThr.value(i) != setup.chZsThr.value(i) ||
                    settingsCache.chBaseline.value(i) != setup.chBaseline.value(i))
                ChnSetZsThreshold(i, setup.chBaseline.value(i) + setup.chZsThr.value(i));
//                ChnSetZsThreshold(i, setup.zsThr.value(i));

        if (!settingsCacheValid || settingsCache.readCellNumber != setup.readCellNumber
                || settingsCache.sparseParams.en != setup.sparseParams.en
                || settingsCache.sparseParams.readCellNum != setup.sparseParams.readCellNum){
            quint16 cells;
            if(setup.sparseParams.en && setup.sparseParams.sparseAllowed)
                cells = setup.sparseParams.readCellNum;
            else
                cells = setup.readCellNumber;
            regWrite(REG_MSTREAM_DATA_SIZE_BYTES, (cells/2) * 4); //convert words to bytes
            //                qDebug()<<"Sample size:"<<setup.readcellnumber;
        }

        if(setup.mstreamEnable && setup.run && !setup.skipRestartRun) {
            StartRun();
        }

        if (!settingsCacheValid || settingsCache.mtuSize != setup.mtuSize) {
            if(hasMtuSetting(status.fw_ver))
                regWrite(REG_MSTREAM_MTU_SIZE, setup.mtuSize);
        }

        writeSparseParams();
        writeFirParams();
        quint32 erc_val = 0;
        if(setup.mstreamEnable & setup.run)
            erc_val |= ERC_BIT_EN;
        if(setup.zs_en && !setup.softwareZs)
            erc_val |= ERC_BIT_ZS_EN;
        regWrite(REG_MSTREAM_RUN_CTRL, erc_val); //enable/disable mstream data

        writeLiveMagic();
        settingsCache = setup;
        settingsCacheValid = true;
    }
    catch (std::runtime_error &e) {
        settingsCacheValid = false;
        qCritical() << QString("Communication error: %1").arg(e.what());
        Q_EMIT setStatus(QString("Communication error: %1").arg(e.what()));
    }
    if(settingsCacheValid)
        emit setupWritten(setup);
}

QMap<int, bool> DominoDevice::patternTestByChannel()
{
    return QMap<int, bool>();
}

void DominoDevice::WritePCAReg(const reg_pca_struct &reg_pca)
{
    regWrite(REG_PCA_12, reg_pca.all);
}

void DominoDevice::WriteAD5622Command(const ad5622operation &ad5622op)
{
    regWrite(REG_AD5622_BASELINE, ad5622op.data_baseline);
    regWrite(REG_AD5622_LEVEL, ad5622op.data_level);
    regWrite(REG_AD5622_COMMAND, AD5622_WRITE_COMMAND);
}

void DominoDevice::SetAmpBoard(const quint16 &board_set)
{
    regWrite(REG_AMP_SET, board_set);
}

void DominoDevice::SetTrigDelay(qint16 delay)
{
    //    qDebug() << "trigdelay=" << setup.trigdelay;
    regWrite(REG_DEVICE_RLAT, delay);  //Set number of points saved after incoming trigger
}

void DominoDevice::pollData()
{
    try {
        if (isConnected()) {
            if (setup.run) {
                if (!saveRunState) {
                    //            qDebug() << "Start";
                    StartRun();
                } else {
                    // read status
                    bool running = isRunning();
                    //            qDebug() << running;
                    if (!running) {
                        //                qDebug() << "Readout";
                        ReadAllChMem();
                        //                qDebug() << "Restart";
                        RestartRun();
                    }
                }
            } else {
                if (saveRunState) {
                    StopRun();
                    saveRunState = false;
                }
            }
            if (saveRunState && !setup.run)  //setup.run can change during long ReadAllChMem()
            {
                StopRun();
            }
            saveRunState = setup.run;
        } else {
            saveRunState = false;
        }
    }
    catch (std::runtime_error &e) {
        saveRunState = false;
        Q_EMIT setStatus(QString("Communication error: %1").arg(e.what()));
    }
    if(!setup.mstreamEnable)
        pollDataTimer->start(saveRunState ? 1 : POLL_TIME_MS);
}


void DominoDevice::RestartRun()
{
    mlink::RegOpVector v;
    if (hasRunEnableBitFix(status.fw_ver)) {
        v << RegWrite(REG_DEVICE_CTRL, 0); // clear RUN
        v << RegWrite(REG_DEVICE_CTRL, 0x8000); // set RUN
    } else {
        v << RegWrite(REG_DEVICE_CTRL, 0); // clear RUN
        v << RegWrite(REG_DEVICE_CTRL, REG_DEVICE_CTRL_BIT_RUN); // set RUN
    }
    regOpExec(v);
}

void DominoDevice::StartRun()
{
    RestartRun();
}

void DominoDevice::StopRun()
{
    RegOpVector regOp;
    if (hasRunEnableBitFix(status.fw_ver))
        regOp << RegWrite(REG_DEVICE_CTRL, 1);   // send Soft clear

    regOp << RegWrite(REG_DEVICE_CTRL, 0);   // clear RUN
    regOpExec(regOp);
}

void DominoDevice::ReadChMem(int ch)
{
    if (!isConnected())
        return;
    // two adc words per memory address
    const int ramSize = maxSampleCount / 2;
    const quint32 baseAddr = chBaseMemAddr(ch);
    const quint32 ch_reg_stop_addr = (ReadChReg(ch, MEM_CH_WR_ADDR));
    const bool oddStopAddr = (hasStreamRead(status.fw_ver)) ? false : (ch_reg_stop_addr % 2);
    const bool readOddSamples = setup.readCellNumber % 2;
    const bool drop_first = (hasStreamRead(status.fw_ver)) ? false : (oddStopAddr ^ readOddSamples);
    const bool drop_last = (hasStreamRead(status.fw_ver)) ? false : oddStopAddr;

    int wordsToRead = (setup.readCellNumber / 2);
    if (oddStopAddr || readOddSamples)
        wordsToRead++;
    if (wordsToRead > ramSize) wordsToRead = ramSize;
    int stopAddr = ((ch_reg_stop_addr / 2) + (ch_reg_stop_addr % 2)) % ramSize;
    int startAddr = stopAddr - wordsToRead - 1;
    if (startAddr < 0) startAddr += ramSize;
    //    qDebug() << "Start address " << startAddr;
    //    qDebug() << "Stop  address " << stopAddr;

    QVector<quint32> fullrd;
    fullrd.reserve(wordsToRead); // reserve memory, vector size = 0
    int numRead = 0;
    while (numRead < wordsToRead) {
        int start = (startAddr + numRead) % ramSize;
        int stop = (startAddr + wordsToRead);
        if (start > stop) stop = ramSize;
        int count = stop-start;
        //        qDebug() << QString("%1: Read %2 - %3").arg(numRead).arg(start).arg(stop);
        if(hasStreamRead(status.fw_ver))
        {
            start = 0;
            count = (setup.readCellNumber / 2);
        }
        const QVector<quint32> &rbuf = QVector<quint32>::fromStdVector(memReadBlk(baseAddr | start, count));
        if (count != rbuf.size())
        {
            qCritical() << "read memory size mismatch";
            return;
        }
        // append
        fullrd += rbuf;
        numRead += rbuf.size();
    }

    dominoData.rawData[ch].clear();
    const qint16 offset = hasAdcRawDataSigned(status.fw_ver) ? 0 : 0x8000;
    const int fullrdSize = fullrd.size();
    for (int i=0; i<fullrdSize; i++)
    {
        qint16 veven = (qint16)((fullrd[i] >> 16) & 0xFFFF) - offset;
        qint16 vodd  = (qint16)((fullrd[i] >> 0)  & 0xFFFF) - offset;
        bool first = (i==0);
        bool last = (i+1 == fullrdSize);
        if (!(first && drop_first))
            dominoData.rawData[ch].push_back(veven);
        if (!(last && drop_last))
            dominoData.rawData[ch].push_back(vodd);
    }
}

DigitizerTriggerStatus DominoDevice::readTrigStatus()
{
    DigitizerTriggerStatus trigStatus;
    trigStatus.trig_tai_ts.all =  regRead64(REG_TRIG_CSR_TRIG_TS);
    trigStatus.eventNumber =  regRead64(REG_TRIG_CSR_EV_NUM);
    trigStatus.trig_in_delay_ns =  regRead(REG_TRIG_CSR_TRIG_IN_DELAY);
    trigStatus.trig_code =  regRead(REG_TRIG_CSR_TRIG_CODE);
//    std::cout << QString("evNum=%1  ts=%2  delay=%3 code=%4")
//                 .arg(trigStatus.eventNumber)
//                 .arg(trigStatus.trig_tai_ts.bit.toString())
//                 .arg(trigStatus.trig_in_delay_ns)
//                 .arg(trigStatus.trig_code)
//                 .toStdString() << std::endl;
    return trigStatus;
}

void DominoDevice::ReadAllChMem()
{
    dominoData.rawData.clear();
    dominoData.stepNs = getBaseStepNs();
    dominoData.serial_id = getDeviceSerialNumber();
    dominoData.run_ts = readTimestampRun();
    dominoData.trig_ts = readTimestampTrig();
    dominoData.trigStatus = readTrigStatus();

    quint64 zs_events = 0xFFFFFFFFFFFFFFFFu;
    if(setup.zs_en && !setup.softwareZs)
        zs_events = regRead64(REG_ZS_EVENTS);

    for (int i=0; i<getNumberOfChannels(); i++) {
        if (setup.chEn[i] && ((zs_events >> i)&0x1)) {
            ReadChMem(i);
            if(setup.zs_en && setup.softwareZs) {
                QVector<qint16> &chData = dominoData.rawData[i];
                if(!chData.isEmpty()) {
                    qint16 min = *std::min_element(chData.begin(), chData.end());
                    qint16 max = *std::max_element(chData.begin(), chData.end());
                    if(max-min < setup.chZsThr[i])
                        chData.clear();
                }
            }
        } else {
            dominoData.rawData[i]; // empty entry must be present for correct channel numbering
        }
    }

    //    quint64 current_ts = readTimestampCurrent();
    //    qDebug() << QString("[%1] TS: current %2, run %3, trig %4")
    //                .arg(dominoData.serial_id, 4, 16, QChar('0'))
    //                .arg(current_ts, 16, 16, QChar('0'))
    //                .arg(dominoData.run_ts, 16, 16, QChar('0'))
    //                .arg(dominoData.trig_ts, 16, 16, QChar('0'));
    dominoData.EventNumber++;
    //    dominoData.timestamp = QDateTime::currentDateTime();
//    if(setup.mode == DominoSettings::NormalMode)
        Q_EMIT dataAcquired(dominoData);
//    else {

//    }
}


quint16 DominoDevice::ReadChReg(int ch, quint16 addr)
{
    Q_ASSERT(addr < BOARD_REG_COUNT);
    quint32 read_addr = MEM_BIT_SELECT_CTRL;   // bit13==1 (bus 15:0) - register operation
    read_addr |= addr;
    read_addr |= chBaseMemAddr(ch);
    quint32 value = memRead(read_addr);
    //    qDebug() << QString("ReadChReg: memRead(%1, %2)").arg(read_addr, 8, 16, QChar('0')).arg(value, 8, 16, QChar('0'));
    return value;
}

void DominoDevice::WriteChReg(int ch, quint16 addr, quint32 data)
{
    Q_ASSERT(addr < BOARD_REG_COUNT);
    quint32 write_addr = MEM_BIT_SELECT_CTRL;   // bit13==1 (bus 15:0) - register operation
    write_addr |= addr;
    write_addr |= chBaseMemAddr(ch);
//        qInfo() << QString("WriteChReg: memWrite(%1, %2)").arg(write_addr, 8, 16, QChar('0')).arg(quint32(data), 8, 16, QChar('0'));
    memWrite(write_addr, data);
}

bool DominoDevice::isRunning()
{
    return 0 != (regRead(REG_RUN_STATUS) & REG_RUN_STATUS_BIT_RUNNING);
}

quint16 DominoDevice::encodeChCtrlReg(const DominoSettings &setup, int i) const
{
    reg_dig_ch_ctrl ch_ctrl;
    ch_ctrl.all = 0;

    if (setup.chEn.value(i))
        ch_ctrl.bit.en = 1;
    if (setup.invertInput)
        ch_ctrl.bit.invert = 1;
    if (setup.invert_thr_trig)
        ch_ctrl.bit.thr_trig_invert = 1;
    if (setup.invert_zs_thr)
        ch_ctrl.bit.zs_thr_invert = 1;
    if (setup.chTrigEn.value(i))
        ch_ctrl.bit.ch_thr_trig_en = 1;
    ch_ctrl.bit.blc_out_sel1 = setup.dspParams.getMafEnable() ? 1 : 0;
    ch_ctrl.bit.blc_out_sel2 = setup.dspParams.getTestEnable() ? 1 : 0;
    ch_ctrl.bit.blc_maf_sel = setup.dspParams.maf_tap_sel;
    return ch_ctrl.all;
}

void DominoDevice::writeChCtrl(int ch, const DominoSettings &setup)
{
    WriteChReg(ch, MEM_CH_CTRL, encodeChCtrlReg(setup, ch));
    WriteChReg(ch, MEM_CH_BLC_THR_HI, setup.dspParams.blc_thr);
    WriteChReg(ch, MEM_CH_BLC_THR_LO, -setup.dspParams.blc_thr);
}

void DominoDevice::ChnSetZsThreshold(int ch, int thr)
{
    if (hasAdcRawDataSigned(status.fw_ver)) {
        if (thr < -32768) thr = -32768;
        if (thr > 32767) thr = 32767;
    } else {
        thr += 0x8000;
        if (thr < 0) thr = 0;
        if (thr > 0xFFFF) thr = 0xFFFF;
    }

    WriteChReg(ch, MEM_CH_ZS_THR, thr);
}

void DominoDevice::ChnSetBaseline(int ch, int baseline)
{
    return;
    WriteChReg(ch, MEM_CH_BASELINE, baseline);
    if (ch == 0)
    {
        //return;
        qDebug() << "Baseline & FIR";
        qDebug() << "ADC Status" << hex << regRead(REG_DES_STATUS);
        qDebug() << "ADC Status ad9249" << hex << regRead(REG_AD9249_CSR);
        //regWrite(REG_ADC_CLK_CTRL, 0x1000);
        //ResetAdcClkDelay();
        //ChangeAdcClkDelay(true, 0xff, 0xa);
        return;

        //qDebug() << "spi test read 0" << hex << DominoDeviceAD9249::spi_read(0x01, 0x1);
        //qDebug() << "spi test read 0" << hex << DominoDeviceAD9249::spi_read(0x1, 0x2);
        //qDebug() << "spi test read 21" << hex << DominoDeviceAD9249::spi_read(0x21, 0x1);
        //qDebug() << "spi test read 10" << hex << DominoDeviceAD9249::spi_read(0x10, 0x1);
        //DominoDeviceAD9249::spi_write(0x10, 0x40, 0x1);
        //qDebug() << "spi test read B" << hex << ad9249_spi_read(0xb, 0x1);

/*
        WriteChReg(ch, 0x8, 0x5);
        WriteChReg(ch, 0x9, 0x3);
        WriteChReg(ch, 0xa, 0x1);
        WriteChReg(ch, 0xb, 0x0);
        WriteChReg(ch, 0xc, 0xFFFF);
        WriteChReg(ch, 0xd, 0xFFFD);
        WriteChReg(ch, 0xe, 0xFFFB);
*/
        return;
        WriteChReg(ch, 0x8, 0x1);
        return;
        WriteChReg(ch, 0x9, 0xFFFC);
        WriteChReg(ch, 0xa, 0x3);
        WriteChReg(ch, 0xb, 0x0);
        WriteChReg(ch, 0xc, 0x3);
        WriteChReg(ch, 0xd, 0xFFFC);
        WriteChReg(ch, 0xe, 0x1);

        WriteChReg(ch, 0xf, 0x2);
        WriteChReg(ch, 0xf, 0x0);

        WriteChReg(ch, 0xf, 0x5);
        //WriteChReg(ch, 0xf, 0x1);

        //WriteChReg(ch, 0xf, 0x2);
        //WriteChReg(ch, 0xf, 0x0);
        //WriteChReg(ch, 0xf, 0x1);

        /*
        WriteChReg(ch, 0x8, 0xFFFD);
        WriteChReg(ch, 0x9, 0xFFFE);
        WriteChReg(ch, 0xa, 0xFFFF);
        WriteChReg(ch, 0xb, 0);
        WriteChReg(ch, 0xc, 0);
        WriteChReg(ch, 0xd, 0x1);
        WriteChReg(ch, 0xe, 0x2);
        WriteChReg(ch, 0xf, 0x3);
        */
        /*
        qDebug() << "read reg 0x8 = " << ReadChReg(ch, 0x8);
        qDebug() << "read reg 0x9 = " << ReadChReg(ch, 0x9);
        qDebug() << "read reg 0x1 = " << ReadChReg(ch, 0x1);
        */
    }
}

void DominoDevice::ChnSetThreshold(int ch, int thr)
{
    if (hasAdcRawDataSigned(status.fw_ver)) {
        if (thr < -32768) thr = -32768;
        if (thr > 32767) thr = 32767;
    } else {
        thr += 0x8000;
        if (thr < 0) thr = 0;
        if (thr > 0xFFFF) thr = 0xFFFF;
    }
    WriteChReg(ch, MEM_CH_THR, thr);
}

void DominoDevice::WriteTrigSetup(reg_trig_ctrl trigsetup)
{
    regWrite(REG_TRIG_CTRL, trigsetup.all);
}

bool DominoDevice::FindAdcClkDelay()
{
    QVector< QVector<bool> > ptr; //pattern test result
    QVector<int> adc_delay_value;

    qDebug() << "Starting adc clock delay search...";
    blockSignals(true);
    ResetAdc();
    ResetAdcClkDelay();

    setCustomPatternMode(true);

    spiPatternSet.clear();
    qsrand(QTime::currentTime().msecsSinceStartOfDay());
    for(int n=0; spiPatternSet.size()<1; ++n){
        quint16 val = qrand();
        if(val!=0)
            spiPatternSet.push_back(val);
    }
//    spiPatternSet[0] = 0xffff;


    // [desIndex][clkDelay][dataDelay] = true/false
    QMap<int, QMap<int, QMap<int, bool > > > resultsPattern;
    QMap<int, QMap<int, QMap<int, bool > > > resultsAdcStatus;
    AdcClkStatus delayStatus;
    QStringList statusLines;
    QStringList patternLines;
    for(int clkDelay=0; clkDelay<32; clkDelay++) {
        QString strStatus = QString("%1:").arg(clkDelay, 2, 10, QChar('0'));
        QString strPattern = QString("%1:").arg(clkDelay, 2, 10, QChar('0'));
        for(int dataDelay=0; dataDelay<32; dataDelay++) {
            ChangeAdcClkDelay(0x5555, clkDelay);
            ChangeAdcClkDelay(0xAAAA, dataDelay);
            ResetAdc();
            QThread::usleep(10);
            quint16 adcStatus=lockTest();
            bool isOk = isLockOk(adcStatus);
            if(isOk) {
                strStatus.append(" || ");
            } else {
                strStatus.append(QString(" %1 ").arg(adcStatus, 2, 16, QChar('0')));
                //                    test.push_back(false);
            }
            QMap<int, bool> chPatternRes = patternTestByChannel();
            for(int n=0; n<getDeserializeNum(); ++n) {
                //                    resultsAdcStatus[clkDelay][dataDelay] = (adcStatus == 0x55);
                resultsAdcStatus[n][clkDelay][dataDelay] = (((adcStatus>>(2*n))&0x3) == 0x1);
            }

            bool allChRes=true;
            foreach (int ch, chPatternRes.keys()) {
                bool &v = chPatternRes[ch];
                allChRes &= v;
                resultsPattern[ch][clkDelay][dataDelay] = v;
            }
            isOk &= allChRes;
            strPattern.append(allChRes?"    ":" er ");

            if(ptr.size()!=chPatternRes.size()) ptr.resize(chPatternRes.size());
            for(int j=0; j<ptr.size(); j++) ptr[j].push_back(chPatternRes[j]);
            delayStatus.data[clkDelay][dataDelay] = isOk;
        }
//                    qInfo()<<strStatus;
//                    qInfo()<<strPattern;
        statusLines<<strStatus;
        patternLines<<strPattern;
    }
    blockSignals(false);

//        foreach (int i, resultsAdcStatus.keys()) {
//            qInfo()<<"adc Status:"<<i;
//            foreach(int clkDelay, resultsAdcStatus[i].keys()) {
//                QString str = QString("%1:").arg(clkDelay, 2, 10, QChar('0'));
//                foreach(int dataDelay, resultsAdcStatus[i][clkDelay].keys()) {
//                    if(resultsAdcStatus[i][clkDelay][dataDelay]) {
//                        str.append(" || ");
//                    } else {
//                        str.append(" er ");
//                    }
//                }
//                qInfo()<<str;
//            }
//        }

//        bool hasAnyTrue;
//        foreach (int i, resultsPattern.keys()) {
//            hasAnyTrue = false;
//            qInfo()<<"pattern Test:"<<i;
//            foreach(int clkDelay, resultsPattern[i].keys())
//                foreach(int dataDelay, resultsPattern[i][clkDelay].keys())
//                    hasAnyTrue |= resultsPattern[i][clkDelay][dataDelay];

//            if(!hasAnyTrue){
//                qInfo()<<"All err";
//                continue;
//            }
//            foreach(int clkDelay, resultsPattern[i].keys()) {
//                QString str = QString("%1:").arg(clkDelay, 2, 10, QChar('0'));
//                foreach(int dataDelay, resultsPattern[i][clkDelay].keys()) {
//                    if(resultsPattern[i][clkDelay][dataDelay]) {
//                        str.append(" || ");
//                    } else {
//                        str.append(" er ");
//                    }
//                }
//                qInfo()<<str;
//            }
//        }

//        qInfo()<< "spiPatternSet:";
//        foreach (quint16 val, spiPatternSet)
//            qInfo()<< hex<< val;
    delayStatus.temp = readTemp();
    delayStatus.fwStr = getFirmwareVersionStr();
    qDebug() << "Adc clock delay search finished.";
    emit adcClkDelayUpdated(getDeviceIndex(), delayStatus);

    setCustomPatternMode(false);
    if(getDeviceId() == DEVICE_ID_ADC64) {
        for(int i=0; i<ptr.size(); i++)
            qDebug() << "Test result vector " << i << " = " << ptr[i];

        for (int i=0; i<ptr.size(); i++)
            adc_delay_value.push_back(FindTruePlateauCenter(ptr[i]));
        qDebug() << "adc_delay_value vector" << adc_delay_value;
        for (int i=0; i<adc_delay_value.size(); i++)
            if (adc_delay_value[i] == 0){
                adcCalibrationStatus = false;
                return adcCalibrationStatus;  //if at least one center of true plateau was not found, return false
            }
//        SetAdcClkDelay(adc_delay_value);
    }

    adcCalibrationStatus = true;
    return adcCalibrationStatus;
}

void DominoDevice::clearStatistics(const DeviceIndex &index)
{
    if(!index.isNull() && getDeviceIndex() != index)
        return;
    clearStatistics_impl();
    status.clear();
}

void DominoDevice::clearStatistics_impl()
{
    regWrite(REG_STATISTIC_CONTROL,0xFFFF); // implement separate counters reset later
    regWrite(REG_STATISTIC_CONTROL,0);
    qDebug()<<"clear stat";
}

void DominoDevice::evNumRequest(int checkIndex)
{
    if (!isConnected())
        return;
    try {
        // We needed only Run&Spill Edge counters (it is 8th counter block)
        // And in this block we read "Accepted trigger count" (the last one)
        quint64 globEvNum=0;
        if (hasCountersLock(status.fw_ver)) {
            RegOpVector v;
            quint16 oldCtrlVal = setup.run ? 0x8000 : 0;
//            oldCtrlVal = regRead(REG_DEVICE_CTRL);
            v << RegWrite(REG_DEVICE_CTRL, oldCtrlVal | 0x2);
            v << RegRead64(REG_RUN_EVENT_NUMBER_64);
            v << RegWrite(REG_DEVICE_CTRL, oldCtrlVal);
            RegOpVector result = regOpExec(v);
            foreach (mlink::RegOp regOp, result) {
                switch (regOp.addr) {
                case REG_RUN_EVENT_NUMBER_64:
                case REG_RUN_EVENT_NUMBER_64+1:
                case REG_RUN_EVENT_NUMBER_64+2:
                case REG_RUN_EVENT_NUMBER_64+3:
                {
                    const int byte = regOp.addr % 4;
                    globEvNum |= (quint64)regOp.data << 16*byte;
                    break;
                }
                }
            }
        } else
            globEvNum = regRead32(REG_RUN_EVENT_NUMBER);

//        const quint64 evNum = regRead64(REG64_CNT_BASE + (7*REG64_CNT_BLOCK_SIZE+15)*4);
//        if(DEBUG_INFO)
//        qInfo()<<"TrigDevice::evNumRequest:"<<checkIndex<<globEvNum;

        emit evNumResponse(checkIndex, getDeviceSerialNumber(), globEvNum);
    } catch (std::exception &e){
        qWarning() << "try/catch error evNumRequest";
    }
}

QVector<bool> DominoDevice::patternTest()
{
    const int octal_adc_channels = 8;
    QMap<int, bool> channelRes = patternTestByChannel();
    QVector<bool> desRes;
    for (int desIndex=0; desIndex<(getNumberOfChannels()/octal_adc_channels); desIndex++)
    {
        bool res=true;
        // Work with single deserializer
        for(int chInDes=0; chInDes<octal_adc_channels; chInDes++)
        {
            int ch = desIndex*octal_adc_channels+chInDes;
            if(channelRes.contains(ch))
                res &= channelRes[ch];
        }
        desRes.push_back(res);
    }
    return desRes;
}

QMap<int, bool> DominoDevice::TestAdcCustomPatternByChannel(const quint16 &pattern)
{
    const int pattern_cnt_reread_number = 1;  //reading pattern mismatch counters 2 times
    QMap<int, bool> test_ok;
    QVector <quint16> read_pattern_cnt;

    for (int ch=0; ch<getNumberOfChannels(); ch++) {
        read_pattern_cnt.clear();
        test_ok[ch] = true;

        WriteChReg(ch, MEM_CH_ADC_PATTERN, pattern);
        for (int j=0; j<pattern_cnt_reread_number+1; j++){
                read_pattern_cnt.push_back(ReadChReg(ch, MEM_CH_ADC_PATTERN_MISMATCH_CNT));
        }
        for (int j=1; j<read_pattern_cnt.size(); j++)
        {
            if (read_pattern_cnt[j]!=read_pattern_cnt[0])
            {
                //qDebug() << "i=" << i << hex << "pattern vector" << read_pattern_cnt;
                test_ok[ch] = false;
                break;
            }else {
                //                    qInfo()<<"test_ok[i] = true;"<<i;
            }
        }
    }
    return test_ok;
}

void DominoDevice::readOutD2Hist()
{
    const int maxCh = getNumberOfChannels()-1;
    if(maxCh<0 || (ReadChReg(maxCh, MEM_CH_D2_HIST_ST) & 0x1) == 0)
        return;
    QTemporaryFile tmpFile;
    if(!tmpFile.open()){
        qWarning()<<"Can't opentemporary file for d2hist";
        return;
    }
    QTextStream tmpOut(&tmpFile);

    for(int ch=0;ch<getNumberOfChannels();++ch) {
        if((ReadChReg(ch, MEM_CH_D2_HIST_ST) & 0x1) == 0){
            qWarning()<<"D2Hist is not ready for ch="<<ch;
            continue;
        }
        // Readout histogram
        const int HIST_SIZE = 0x40;
        quint32 histStartAddr = MEM_BIT_SELECT_CTRL;   // bit13==1 (bus 15:0) - register operation
        histStartAddr |= MEM_CH_D2_HIST;
        histStartAddr |= chBaseMemAddr(ch);

        const QVector<quint32> &hist = QVector<quint32>::fromStdVector(memReadBlk(histStartAddr, HIST_SIZE));
        WriteChReg(ch, MEM_CH_D2_HIST_CTRL, MEM_CH_D2HIST_CTRL_BIT_CLR);
        WriteChReg(ch, MEM_CH_D2_HIST_CTRL, MEM_CH_D2HIST_CTRL_BIT_EN);
        if (HIST_SIZE != hist.size()) {
            qCritical() << "read memory size mismatch; ch="<<ch;
            break;
        }

        quint32 binMaxVal=0;
        qint64 mean64=0;
        quint64 sumOfBins =0;
        for(qint64 d2Val=0-HIST_SIZE/2; d2Val<HIST_SIZE/2; ++d2Val) {
            const qint64 bin = (d2Val+HIST_SIZE/2)^(HIST_SIZE/2);
            const quint32 &binVal = hist[bin];

            binMaxVal = qMax(binMaxVal, binVal);
            mean64 += d2Val*(qint64)binVal;
            sumOfBins += binVal;
            //        qInfo()<<"d2Val="<<d2Val<<"; binNum="<<bin<<"; val="<<binVal<<"; mean="<<mean64<<" sumOfBins="<<sumOfBins;
        }
        if(binMaxVal == 0) {
            qCritical() << "D2Histogram is empty; ch="<<ch;
            break;
        }

        tmpOut << "ch:"<<ch<<"; timeout(ms):"<<D2_HIST_TIMEOUT_MS<<endl;
        const double mean = 1.*mean64/sumOfBins;
        double sigma = 0;

        const double maxLength=60;
        int binRangeMin=HIST_SIZE, binRangeMax=0-HIST_SIZE;
        for(int d2Val=0-HIST_SIZE/2; d2Val<HIST_SIZE/2; ++d2Val) {
            const qint64 bin = (d2Val+HIST_SIZE/2)^(HIST_SIZE/2);
            const quint32 binVal = hist[bin];

            sigma += (mean-d2Val)*(mean-d2Val)*binVal;
            if(binVal) {
                binRangeMin = qMin(binRangeMin, d2Val);
                binRangeMax = qMax(binRangeMax, d2Val);

                int length = maxLength*log10(binVal)/log10(binMaxVal)+1;
                QString str = QString("%1 (%2) %4%: %3")
                        .arg(d2Val,3)
                        .arg(binVal, 8, 16, QChar('0'))
                        .arg("", length, QChar('*'))
                        .arg(100.*binVal/sumOfBins, 6, 'f', 3);
//                qInfo(str.toStdString().c_str());
                tmpOut << str <<endl;

            }
        }
        sigma = sqrt(sigma/sumOfBins);

//        qInfo()<<QString("Non-empty range from %1 to %2")
//                 .arg(binRangeMin, 6)
//                 .arg(binRangeMax, 6);
//        qInfo()<<QString("  3-sigma range from %1 to %2")
//                 .arg(mean-3*sigma, 6, 'f', 2)
//                 .arg(mean+3*sigma, 6, 'f', 2);
//        qInfo()<<QString("  5-sigma range from %1 to %2")
//                 .arg(mean-5*sigma, 6, 'f', 2)
//                 .arg(mean+5*sigma, 6, 'f', 2);
//        qInfo()<<QString("  Mean: %1; Sigma: %2")
//                 .arg(mean, 4, 'f', 2)
//                 .arg(sigma, 4, 'f', 2);

        tmpOut<<QString("Non-empty range from %1 to %2")
                 .arg(binRangeMin, 6)
                 .arg(binRangeMax, 6)<<endl;
        tmpOut<<QString("  3-sigma range from %1 to %2")
                 .arg(mean-3*sigma, 6, 'f', 2)
                 .arg(mean+3*sigma, 6, 'f', 2)<<endl;
        tmpOut<<QString("  5-sigma range from %1 to %2")
                 .arg(mean-5*sigma, 6, 'f', 2)
                 .arg(mean+5*sigma, 6, 'f', 2)<<endl;
        tmpOut<<QString("  Mean: %1; Sigma: %2")
                 .arg(mean, 4, 'f', 2)
                 .arg(sigma, 4, 'f', 2)<<endl<<endl;
    }
    tmpOut.flush();
    tmpFile.close();


    QString fileName;
#ifdef Q_OS_LINUX
    fileName = QDir::homePath();
#else
    fileName = QDir::rootPath();
#endif
    fileName.append(QString("%1d2hist_%2.txt").arg(QDir::separator()).arg(getSerialIdStr()));
    if(QFile(fileName).exists()){
        QFile(fileName).remove();
    }
    if(!tmpFile.copy(fileName))
        qWarning()<<"Failed to save d2hist file";
}

quint16 DominoDevice::lockTest()
{
    quint16 des_status = regRead(REG_DES_STATUS);
    bool lock_ok = isLockOk(des_status);

    status.adcDesStatus = des_status;
    status.adcSelfTestOk = lock_ok;

    emit adcLockStatusUpdated(getDeviceIndex(), lock_ok, des_status);

    return des_status;

    if (!lock_ok) qCritical() << "ADC data lock error. " << adcStatusStr(des_status);
    else
        qDebug() << adcStatusStr(des_status);
}

bool DominoDevice::isLockOk(quint16 des_status) const
{
    bool lock_ok = true;
    for(int n=0; n<getDeserializeNum(); ++n)
        if(((des_status>>(2*n)) & 3) != 1)
            lock_ok = false;
    return lock_ok;
}

int DominoDevice::FindTruePlateauCenter(const QVector<bool> &v)
{
    int start_search_index = 0;
    if (v.size()<2) return 0;
    for(int i=2; i<v.size(); i++)  //looking for 2 false, then start search for true plateau
        if (!v[i] && !v[i-1])
        {
            start_search_index = i;
            break;
        }
    if (start_search_index == 0) return 0;  //if it was unable to detect 2 seq. false, then search failed
    int true_plateau_start = 0;
    int true_plateau_center = 0;
    bool true_plateau_start_found = false;
    bool true_plateau_end_found = false;
    for(int i=start_search_index; i<v.size(); i++)
    {
        if(v[i] && v[i-1] && v[i-2] && !true_plateau_start_found)  //looking for 3 seq. true - start of true plateau
        {
            true_plateau_start_found = true;
            true_plateau_start = i-2;
        }
        if (true_plateau_start_found && !v[i])  //looking for first false after at least 3 true
        {
            true_plateau_end_found = true;
            true_plateau_center = (i-1+true_plateau_start) / 2;
            break;
        }
    }
    if (true_plateau_start_found && true_plateau_end_found) return true_plateau_center;
    else return 0;
}

void DominoDevice::ChangeAdcClkDelay(quint16 channel_mask, quint16 taps)
{
    regWrite(REG_DES_IDELAY_TAP_VAL, taps);
    regWrite(REG_DES_IDELAY_LOAD_MASK, channel_mask);
}

void DominoDevice::ResetAdcClkDelay()
{
    regWrite(REG_DES_CTRL, ADC_CLK_CTRL_BIT_RESET);
    regWrite(REG_DES_CTRL, 0);  //work on edge
}

void DominoDevice::ResetAdc()
{
    regWrite(REG_DES_CTRL, ADC_CLK_CTRL_BIT_ADC_RESET);
    regWrite(REG_DES_CTRL, 0);  //work on edge
}

void DominoDevice::ResetAdcStatus()
{
    regWrite(REG_DES_CTRL, ADC_CLK_CTRL_BIT_STATUS_RESET);
    regWrite(REG_DES_CTRL, 0);  //work on edge
}

void DominoDevice::ResetAdcSync()
{
    regWrite(REG_DES_CTRL, ADC_CLK_CTRL_BIT_SYNC_RESET);
    regWrite(REG_DES_CTRL, 0);  //work on edge
}

void DominoDevice::ResetAdcPin()
{
    regWrite(REG_DES_CTRL, ADC_CLK_CTRL_BIT_INC);
    regWrite(REG_DES_CTRL, 0);  //work on edge
}
