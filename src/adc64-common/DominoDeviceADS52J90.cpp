#include "DominoDeviceADS52J90.h"

#include "DominoDeviceRegisters.h"

DominoDeviceADS52J90::DominoDeviceADS52J90(quint16 devId, quint64 devSerial, QObject *parent)
    : DominoDevice(devId, devSerial, parent)
{
    Q_ASSERT(getDeviceId() == DEVICE_ID_ADC64VE ||
             getDeviceId() == DEVICE_ID_ADC64VE_XGE ||
             getDeviceId() == DEVICE_ID_ADC64ECAL ||
             getDeviceId() == DEVICE_ID_ADC64S2 ||
             getDeviceId() == DEVICE_ID_ADC64WR);
}

void DominoDeviceADS52J90::spi_test()
{
    qInfo() << "ADS52J90 SPI TEST";

    qsrand(QTime::currentTime().msecsSinceStartOfDay());

    quint16 active_adc_mask = 0xf;
    quint16 error_number = 0;

    QSet<quint16> randVal;
    randVal << 0xa5a5;

    for(int j=0; j<100; j++)
    {
        quint16 rand_val = qrand();
        spi_write(0x5, rand_val, active_adc_mask);
        quint16 readregval = spi_read(0x5, active_adc_mask);
        if (rand_val != readregval)
        {
            error_number++;
            if (error_number < 100)
                qInfo() << "write " << hex << rand_val << "read" << readregval << "XOR" << (rand_val ^ readregval);
        }
    }

    if (error_number == 0)
        qDebug() << "SPI random data test OK";
    else
        qWarning() << "Error number = " << error_number;
}

void DominoDeviceADS52J90::setupAdc()
{
    qInfo() << "ADS52J90 setup";

    ResetAdcPin();

    spi_write(0xA, 0x3000, 0xF); //after reset
    spi_write(0x1, 0x0014, 0xF); //16 adc ch per chip

    quint16 adcSerialization;
    quint16 adcResolution;
    switch(status.adcBits) {
    case 10:
        adcSerialization = 0x6000;
        adcResolution = 0x3;
        if(status.adcPeriod == 10) {
            ChangeAdcClkDelay(0x5555, 3); //1
            ChangeAdcClkDelay(0xAAAA, 8); //5
        }
        break;
    case 16:
        adcSerialization = 0x8000;
        adcResolution = 0x1;
        break;
    case 12:
    default:
        adcSerialization = 0;
        adcResolution = 0;
    }
    spi_test();
    spi_write(0x3, adcSerialization, 0xF);
    spi_write(0x4, adcResolution, 0xF);

    //setCustomPatternMode(true);
    //setCustomPattern(0x40);

    ResetAdcSync();
    ResetAdc();
    lockTest();
}

QMap<int, bool> DominoDeviceADS52J90::patternTestByChannel()
{
    quint16 spi_pattern;
    quint16 adc_pattern;
    QMap<int,bool> testok;

    for (int n=0; n<spiPatternSet.size(); ++n)
    {
        spi_pattern = spiPatternSet[n];
        spi_pattern &= 0xFFF;
        adc_pattern = (spi_pattern << (16-status.adcBits));  //arrange to 16bit data
        //qInfo() << hex << "adc_pattern:" << adc_pattern;
//        adc_pattern = spi_pattern << 4;
        setCustomPattern(adc_pattern);
        QMap<int,bool> test = TestAdcCustomPatternByChannel(adc_pattern);
        if(n==0) {
            testok = test;
        } else {
            foreach (int ch, test.keys())
                testok[ch]&=test[ch];
        }
    }
    return testok;
}

void DominoDeviceADS52J90::setCustomPatternMode(bool mode)
{
    spi_write(0x2, mode ? 0x0180 : 0, 0xFF);
}

void DominoDeviceADS52J90::setCustomPattern(quint16 data)
{
    spi_write(0x5, data, 0xFF);
}

void DominoDeviceADS52J90::spi_write(quint16 addr, quint16 data, quint16 device_mask)
{
    regWrite(REG_ADS52J90_ACTIVE_ADC_SELECT, device_mask);
    regWrite(REG_ADS52J90_SPI_WR_DATA, data);
    regWrite(REG_ADS52J90_SPI_ADDR, addr);

    for(int i=0; i<10; i++)
        regWrite(REG_ADS52J90_SPI_WR_DATA, data);
}

quint16 DominoDeviceADS52J90::spi_read(quint16 addr, quint16 device_mask)
{
    spi_write(0x0, 0x2, device_mask);
    regWrite(REG_ADS52J90_ACTIVE_ADC_SELECT, device_mask);
//    regWrite(REG_ADS52J90_SPI_WR_DATA, 0);
    regWrite(REG_ADS52J90_SPI_ADDR, addr);
    quint16 readval = regRead(REG_ADS52J90_SPI_READ_DATA);
    spi_write(0x0, 0x0, device_mask);

    return readval;
}
