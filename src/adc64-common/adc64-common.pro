TEMPLATE = lib
CONFIG += staticlib

EXTDIR = $$PWD/../../external

include(adc64-common.pri)

include($${EXTDIR}/lib-common/bmc/bmc.pri)
include($${EXTDIR}/mlinkip/src/mlinkip.pri)
include($${EXTDIR}/device-discover/mldiscover/mldiscover.pri)
include($${EXTDIR}/mongo/mongo.pri)
