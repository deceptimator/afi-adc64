#include "dominodata.h"

#include <cmath>

#include <QDebug>

void DominoData::PrintRMS() const
{

    Q_FOREACH(int i, rawData.keys())
    {
       double rms=0;
       double mean=0;
       for (int j=0; j<rawData[i].size(); j++)
            mean+=rawData[i][j];
       mean/=rawData[i].size();
       for (int j=0; j<rawData[i].size(); j++)
            rms+=pow((rawData[i][j]-mean),2);
       rms/=rawData[i].size();
       rms=sqrt(rms);
       qDebug() << "channel " << i << "  mean=" << mean << "  rms=" << rms;
    }
}
