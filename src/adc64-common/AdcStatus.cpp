#include "AdcStatus.h"
#include <QDebug>

static const int nx = 32;
static const int ny = 32;

static int vindex(int x, int y)
{
    return x + ny * y;
}

static void printMap(const QVector<bool> &map)
{
    QString s;
    for (int y=0; y<ny; y++) {
        s += QString("%1").arg(y, 2);
        for (int x=0; x<nx; x++) {
            s += QString(" %1").arg(map[vindex(x, y)]);
        }
        s += "\n";
    }
    qDebug().noquote() << s;
}

static QVector<bool> shrinkFilter(const QVector<bool> &v)
{
    QVector<bool> result(v.size(), false);
    for (int x=1; x<nx-1; x++)
        for (int y=1; y<ny-1; y++) {
            result[vindex(x, y)] = (
                v[vindex(x, y)] &
                v[vindex(x-1, y)] &
                v[vindex(x+1, y)] &
                v[vindex(x, y-1)] &
                v[vindex(x, y+1)]);
        }
    return result;
}

static QVector<bool> shrinkUntilEmpty(const QVector<bool> &data)
{
    QVector<bool> result(data);
    QVector<bool> map(data);
    while (true) {
        bool found = false;
        QVector<QPair<int, int> > v;
        for (int i=0; i<map.size(); i++)
            if (map[i]) {
                found = true;
                break;
            }
        if (!found)
            break;
        result = map;
        map = shrinkFilter(map);
    }
    return result;
}

AdcClkStatus::WPoint AdcClkStatus::getOptimalDelays() const
{
    WPoint result {0, 0, 1e10, false};
    if (data.isEmpty())
        return result;
    //    QMap<int, QMap<int, bool > > data; // delayStatus <clockDelay, <dataDelay, isOk> >
    QVector<bool> map(nx*ny);
    for (int x=0; x<nx; x++)
        for (int y=0; y<ny; y++)
            map[vindex(x, y)] = data[y][x];
    map = shrinkUntilEmpty(map);

    // printMap(map);
    QVector<WPoint> wpoints;
    for (int i=0; i<map.size(); i++)
        if (map[i]) {
            int x = i % ny;
            int y = i / ny;
            // more weight to delay delta, less to delays sum
            wpoints.push_back({x, y, 1./4. * abs(x-y) + 1./64. * (x+y), true});
        }
    for (const auto &i : wpoints)
        if (i.weight < result.weight)
            result = i;
    return result;
}

AdcClkStatus &AdcClkStatus::operator&=(const AdcClkStatus &r)
{
    for (int clockDelay : r.data.keys()) {
        QString devStatusStr = QString("%1: ").arg(clockDelay, 2);
        for (int dataDelay : r.data[clockDelay].keys()) {
            data[clockDelay][dataDelay] &= r.data[clockDelay][dataDelay];
        }
    }
    return *this;
}

QString AdcClkStatus::toString() const
{
    const auto &optPoint = getOptimalDelays();
    QString s;
    QString headerStr = "    ";
    for(int i=0; i<32; ++i)
        headerStr += QString("%1").arg(i%10, 2);
    s += headerStr + "\n";
    for (int clockDelay : data.keys()) {
        QString devStatusStr = QString("%1: ").arg(clockDelay, 2);
        for (int dataDelay : data[clockDelay].keys()) {
            bool opt = (optPoint.y == clockDelay && optPoint.x == dataDelay);
            bool good = data[clockDelay][dataDelay];
            devStatusStr.append(good ? (opt ? " █" : " +") : " ·");
        }
        s += devStatusStr + "\n";
    }
    return s;
}
