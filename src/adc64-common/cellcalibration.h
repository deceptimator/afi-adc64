#ifndef CELLCALIBRATION_H
#define CELLCALIBRATION_H

#include <vector>

class CellCalibration
{
public:
    CellCalibration();
    void addCellAmpData(int channel, const std::vector<int> &data, int startcellnumber);
    std::vector<double> getCellAmlCalibration(int channel) const;
    void clear();

private:
    std::vector<std::vector<double> >cellAmpSum;
    std::vector<std::vector<double> >cellAmpCalibration;
    std::vector<int> cellAmpEntries;
    void calcCellAmpCalibration(int channel);
};

#endif // CELLCALIBRATION_H
