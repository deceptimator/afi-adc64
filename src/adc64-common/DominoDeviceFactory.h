#ifndef DOMINODEVICEFACTORY_H
#define DOMINODEVICEFACTORY_H

#include <QString>

class DominoDevice;
class DeviceDescription;
class DeviceIndex;
class QObject;

class DominoDeviceFactory
{
public:
    static DominoDevice *create(const DeviceDescription &dd, QObject *parent = nullptr);
    static DominoDevice *create(const DeviceIndex &index,
                                const QString &hwStr=QString(), QObject *parent = nullptr);
};

#endif // DOMINODEVICEFACTORY_H
