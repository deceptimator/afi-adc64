#ifndef DOMINODEVICEAD9252_H
#define DOMINODEVICEAD9252_H

#include "dominodevice.h"

class DominoDeviceAD9252 : public DominoDevice
{
public:
    DominoDeviceAD9252(quint16 devId, quint64 devSerial, QObject *parent = nullptr);

protected:
    void setupAdc() override;

private:
    void spi_write(quint16 addr, quint8 data, int device);
    quint16 spi_read(quint16 addr, int device);
    void setup_adc_ad9252(int device);
};

#endif // DOMINODEVICEAD9252_H
