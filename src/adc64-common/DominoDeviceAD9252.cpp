#include "DominoDeviceAD9252.h"

#include "DominoDeviceRegisters.h"
#include "device_features.h"

namespace {
// AD9252
enum {AD9252_CHIP_ID = 0x09};
enum {
    AD9252_SPI_12BITS_MODE = 0x3
};

enum {
    AD9252_SPI_DEVICE_UPDATE_BIT = 0x1
};

enum {
    AD9252_SPI_RESET = 0x3C,
    AD9252_SPI_RUN = 0x18
};


enum {
    AD9252_SPI_CHIP_PORT_CONFIG_REG = 0x0,
    AD9252_SPI_CHIP_ID_REG = 0x1,
    AD9252_SPI_CHIP_GRADE_REG = 0x2,
    AD9252_SPI_SERIAL_CONTROL_REG = 0x21,
    AD9252_SPI_DEVICE_UPDATE_REG = 0xFF
};

}

DominoDeviceAD9252::DominoDeviceAD9252(quint16 devId, quint64 devSerial, QObject *parent)
    : DominoDevice(devId, devSerial, parent)
{
    Q_ASSERT(getDeviceId() == DEVICE_ID_ADCM163);
}

void DominoDeviceAD9252::setupAdc()
{
    setup_adc_ad9252(0);
    setup_adc_ad9252(1);
}

void DominoDeviceAD9252::spi_write(quint16 addr, quint8 data, int device)
{
    regWrite(device*4 + REG_AD9252_SPI_WR_DATA, data);
    regWrite(device*4 + REG_AD9252_SPI_ADDR, addr & 0x1FFF);   //bits 13,14,15 = 0 (write and W0=W1=0 - 1 byte operation)
}

quint16 DominoDeviceAD9252::spi_read(quint16 addr, int device)
{
    regWrite(device*4 + REG_AD9252_SPI_ADDR, ((addr&0x9FFF) | 0x8000));   //bits 13,14 = 0 , bit15 = 1 (read and W0=W1=0 - 1 byte operation)
    return regRead(device*4 + REG_AD9252_SPI_READ_DATA);
}

void DominoDeviceAD9252::setup_adc_ad9252(int device)
{
    // read ID
    quint8 chip_id = spi_read(AD9252_SPI_CHIP_ID_REG, device);
    if (chip_id != AD9252_CHIP_ID) {
        qCritical() << QString("AD9252 SPI [%1]: failed to read ID: expected %2, got %3")
                       .arg(device)
                       .arg(AD9252_CHIP_ID)
                       .arg(chip_id);
        return;
    }
    quint8 chip_grade = spi_read(AD9252_SPI_CHIP_GRADE_REG, device);
    qDebug() << QString("AD9252 SPI [%1]: chip id %2h, grade %3h")
                .arg(device)
                .arg(chip_id, 2, 16, QChar('0'))
                .arg(chip_grade, 2, 16, QChar('0'));
    // Reset
    spi_write(AD9252_SPI_CHIP_PORT_CONFIG_REG, AD9252_SPI_RESET, device);
    const int reset_watchdog = 10;
    int watchdog = 0;
    while(true)
    {
        if (spi_read(AD9252_SPI_CHIP_PORT_CONFIG_REG, device) == AD9252_SPI_RUN) break;
        watchdog++;
        if(watchdog > reset_watchdog)
        {
            qCritical() << QString("AD9252 SPI [%1]: reset failed").arg(device);
            return;
        }
    }
    qDebug() << QString("AD9252 SPI [%1]: reset").arg(device);

    /*
      spi_write(0xD,  0x48, device);
      spi_write(0x19, 0x21, device);
      spi_write(0x1A, 0xF3, device);
      spi_write(0x1B, 0x54, device);
      spi_write(0x1C, 0xF6, device);
      spi_write(AD9252_SPI_DEVICE_UPDATE_REG, AD9252_SPI_DEVICE_UPDATE_BIT, device);
*/

    quint8 serial_ctrl = spi_read(AD9252_SPI_SERIAL_CONTROL_REG, device);
    if (serial_ctrl != AD9252_SPI_12BITS_MODE) {
        spi_write(AD9252_SPI_SERIAL_CONTROL_REG, AD9252_SPI_12BITS_MODE, device);
        spi_write(AD9252_SPI_DEVICE_UPDATE_REG, AD9252_SPI_DEVICE_UPDATE_BIT, device);
        if (spi_read(AD9252_SPI_SERIAL_CONTROL_REG, device) != AD9252_SPI_12BITS_MODE) {
            qCritical() << QString("AD9252 SPI [%1]: failed to set serial mode").arg(device);
            return;
        } else {
            qDebug() << QString("AD9252 SPI [%1]: set serial mode").arg(device);
        }
    }
    if (hasAdcRawDataSigned(status.fw_ver))
        spi_write(0x14, 0x1, device); // two's complement (signed int)
    else
        spi_write(0x14, 0x0, device); // offset binary (unsigned int)
    spi_write(AD9252_SPI_DEVICE_UPDATE_REG, AD9252_SPI_DEVICE_UPDATE_BIT, device);

    // reset deserializer
    regWrite(device*4 + REG_AD9252_CSR, 1);
    regWrite(device*4 + REG_AD9252_CSR, 0);
    quint8 adc_status = regRead(device*4 + REG_AD9252_CSR) >> 8;
    if (adc_status != 0xa1) {
        qCritical() << QString("AD9252 SPI [%1]: serdes unlocked (%2)").arg(device).arg(adc_status, 2, 16, QChar('0'));
        return;
    } else {
        qDebug() << QString("AD9252 SPI [%1]: serdes locked").arg(device);
    }
    qDebug() << QString("AD9252 SPI [%1]: setup Ok").arg(device);
    quint16 tmp = regRead(REG_DEVICE_ID);
    qDebug() << QString("status: %1").arg(tmp, 2, 16, QChar('0'));
}
