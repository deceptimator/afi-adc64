#ifndef DOMINODATA_H
#define DOMINODATA_H

#include <QMap>
#include <QMetaType>
#include <QVector>

//typedef QMap<int, std::vector<double> > DominoRealData;
//typedef QMap<int, std::vector<qint16> > DominoRawData;
typedef QMap<int, QVector<qint16> > DominoRawData;

typedef union {
    quint64 all = {};
    struct {
        bool isValid() const { return code == 2; }
        double toSeconds() const { return ts_sec + 1e-9*ts_nsec; }
        QString toString() const {
            return QString("%1.%2 %3")
                    .arg(ts_sec)
                    .arg(ts_nsec, 9, 10, QChar('0'))
                    .arg(isValid()? "Valid":"Invalid");
                           }
        unsigned int code: 2;
        quint32 ts_nsec: 30;
        quint32 ts_sec: 32;
    } bit;
} tai_timestamp_t;

struct DigitizerTriggerStatus
{
    qint64 eventNumber = 0;
    quint8 trig_in_delay_ns = 0;
    quint8 trig_code = 0;
    tai_timestamp_t trig_tai_ts = {};
};

class DominoData
{
public:
    quint64 serial_id = 0;
    quint64 run_ts = 0;
    quint64 trig_ts = 0;
    int EventNumber = 0;
    double stepNs = 1;
    DigitizerTriggerStatus trigStatus;
    DominoRawData rawData;

//    DominoRealData Data;
//    int StartCell;

    void PrintRMS() const;

};
Q_DECLARE_METATYPE(DominoData)

#endif // DOMINODATA_H
