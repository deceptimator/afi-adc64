#ifndef DOMINODEVICEAD9249_H
#define DOMINODEVICEAD9249_H

#include "dominodevice.h"

class DominoDeviceAD9249 : public DominoDevice
{
public:
    DominoDeviceAD9249(quint16 devId, quint64 devSerial, QObject *parent = nullptr);

protected:
    void setupAdc() override;
    QMap<int, bool> patternTestByChannel() override;
    void setCustomPatternMode(bool mode) override;
    void setCustomPattern(quint16 data) override;
    int getDeserializeNum() const override { return 8; }

private:
    void spi_write(quint16 addr, quint8 data, quint16 device_mask);
    quint16 spi_read(quint16 addr, quint16 device_mask);
};

#endif // DOMINODEVICEAD9249_H
