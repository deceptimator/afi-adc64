#include "cellcalibration.h"

#include <vector>
#include <QDebug>

const int DRS_CELL_NUMBER = 1024;

CellCalibration::CellCalibration()
{
    clear();
}


void CellCalibration::addCellAmpData(int channel, const std::vector<int> &data, int startcellnumber)
{
    cellAmpEntries[channel]++;
    if (data.size() < DRS_CELL_NUMBER) { qDebug() << "AmpCalib error"; return; }
//    qDebug() << "CalcAmpCalib() StartCell - " << startcellnumber;
    for (int i=0; i<DRS_CELL_NUMBER; i++)
       {
        if((i+startcellnumber)<DRS_CELL_NUMBER) cellAmpSum[channel][i+startcellnumber] += data[i];
        else cellAmpSum[channel][i+startcellnumber-DRS_CELL_NUMBER] += data[i];
       }
//    qDebug() << "CalcAmpCalib() complete";
    calcCellAmpCalibration(channel);
}

void CellCalibration::calcCellAmpCalibration(int channel)
{
    for (int i=0; i<DRS_CELL_NUMBER; i++)
    {
        cellAmpCalibration[channel][i] = (double)(0 - ((double)cellAmpSum[channel][i]/cellAmpEntries[channel]));
    }
}

std::vector<double> CellCalibration::getCellAmlCalibration(int channel) const
{
    return cellAmpCalibration[channel];
}

void CellCalibration::clear()
{
    cellAmpCalibration = std::vector<std::vector<double> >(8, std::vector<double>(1024,0.));
    cellAmpSum = std::vector<std::vector<double> >(8, std::vector<double>(1024,0.));
    cellAmpEntries = std::vector<int>(8, 0);
}
