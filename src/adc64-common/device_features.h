/*
**    Copyright 2020 Ilja Slepnev
**
**    This program is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    This program is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You should have received a copy of the GNU General Public License
**    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DEVICE_FEATURES_H
#define DEVICE_FEATURES_H

#include "mregdev/fw_version.h"

bool hasRunEnableBitFix(const fw_version_t &v);
bool hasCountersLock(const fw_version_t &v);
bool hasMstream22(const fw_version_t &v);
bool hasMtuSetting(const fw_version_t &v);
bool hasAdcRawDataSigned(const fw_version_t &v);
bool hasStreamRead(const fw_version_t &v);
bool hasAdcInfo(const fw_version_t &v);

#endif // DEVICE_FEATURES_H
