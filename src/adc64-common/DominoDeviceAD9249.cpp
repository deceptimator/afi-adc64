#include "DominoDeviceAD9249.h"

#include "DominoDeviceRegisters.h"
#include "device_features.h"

DominoDeviceAD9249::DominoDeviceAD9249(quint16 devId, quint64 devSerial, QObject *parent)
    : DominoDevice(devId, devSerial, parent)
{
    Q_ASSERT(getDeviceId() == DEVICE_ID_ADC64VE ||
             getDeviceId() == DEVICE_ID_ADC64VE_XGE ||
             getDeviceId() == DEVICE_ID_ADC64S2 ||
             getDeviceId() == DEVICE_ID_ADC64WR);
}

void DominoDeviceAD9249::setupAdc()
{
    qDebug() << "AD9249 setup";

    for(int i=0; i<4; i++)
    {
        spi_write(0x8, 0x3, (0x11 << i*2)); //digital reset
        spi_write(0x8, 0x0, (0x11 << i*2)); //digital reset
    }

    for(int i=0; i<8; i++)
    {
        switch(status.adcBits) {
        case 14:
            spi_write(0x21, 0xC1, (0x1 << i)); //MSB serial first, 14bit
            break;
        default:
            spi_write(0x21, 0xC2, (0x1 << i)); //MSB serial first, 12bit
        }
        if (hasAdcRawDataSigned(status.fw_ver))
            spi_write(0x14, 0x1, (0x1 << i)); // two's complement (signed int)
        else
            spi_write(0x14, 0x0, (0x1 << i)); // offset binary (unsigned int)
    }

    ResetAdc();

/*
    quint16 adc_status = regRead(REG_DES_STATUS);

    bool lock_ok = (adc_status & 0xFF) == 0xFF;
    bool test_error = ((adc_status >> 8) & 0xFF) != 0;

    emit adcLockStatusUpdated(getDeviceIndex(), lock_ok, adc_status & 0xFF);
    emit adcRampStatusUpdated(getDeviceIndex(), !test_error, adc_status >> 8);
    if (!lock_ok) qCritical() << "ADC data lock error. " << adcStatusStr(adc_status);
    else
    if (test_error) qCritical() << "ADC data test error. " << adcStatusStr(adc_status);
    else
        qDebug() << adcStatusStr(adc_status);
*/
}

QMap<int, bool> DominoDeviceAD9249::patternTestByChannel()
{
    quint16 spi_pattern;
    quint16 adc_pattern;
    QMap<int,bool> testok;

    for (int n=0; n<spiPatternSet.size(); ++n)
    {
        spi_pattern = spiPatternSet[n];
        spi_pattern &= 0xFFF; // 12-bit mask
        adc_pattern = (spi_pattern << 4);  //arrange to 16bit data
        setCustomPattern(adc_pattern);
        QMap<int,bool> test = TestAdcCustomPatternByChannel(adc_pattern);
        if(n==0) {
            testok = test;
        } else {
            foreach (int ch, test.keys())
                testok[ch]&=test[ch];
        }
    }
    return testok;
}

void DominoDeviceAD9249::setCustomPatternMode(bool mode)
{
    spi_write(0x0D, mode ? 0x08 : 0, 0xFF);
}

void DominoDeviceAD9249::setCustomPattern(quint16 data)
{
    spi_write(0x19, data&0xFF, 0xFF);
    spi_write(0x1A, (data>>8)&0xFF, 0xFF);
    spi_write(0x1B, data&0xFF, 0xFF);
    spi_write(0x1C, (data>>8)&0xFF, 0xFF);
}

void DominoDeviceAD9249::spi_write(quint16 addr, quint8 data, quint16 device_mask)
{
    regWrite(REG_AD9249_ACTIVE_ADC_SELECT, device_mask);
    regWrite(REG_AD9249_SPI_WR_DATA, data);
    regWrite(REG_AD9249_SPI_ADDR, addr & 0x1FFF);   //bits 13,14,15 = 0 (write and W0=W1=0 - 1 byte operation)
}

quint16 DominoDeviceAD9249::spi_read(quint16 addr, quint16 device_mask)
{
    regWrite(REG_AD9249_ACTIVE_ADC_SELECT, device_mask);
    regWrite(REG_AD9249_SPI_ADDR, ((addr&0x9FFF) | 0x8000));   //bits 13,14 = 0 , bit15 = 1 (read and W0=W1=0 - 1 byte operation)
    return regRead(REG_AD9249_SPI_READ_DATA);
}
