#ifndef DOMINODEVICE12EU050_H
#define DOMINODEVICE12EU050_H

#include "dominodevice.h"

class DominoDevice12EU050 :public DominoDevice
{
public:
    DominoDevice12EU050(quint16 devId, quint64 devSerial, QObject *parent = nullptr);
protected:
    QMap<int, bool> patternTestByChannel() override;
    void setCustomPattern(quint16 data) override;
    void setCustomPatternMode(bool mode) override;
private:
    bool spi_write(quint8 reg, quint8 data, bool readback);
    QVector<quint16> spi_read(quint8 reg);
    QVector<bool> PatternTest();
};

#endif // DOMINODEVICE12EU050_H
