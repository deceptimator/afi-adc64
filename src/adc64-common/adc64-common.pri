HEADERS += \
        $$PWD/AdcStatus.h \
        $$PWD/DominoDevice12EU050.h \
        $$PWD/DominoDeviceAD9249.h \
        $$PWD/DominoDeviceAD9252.h \
        $$PWD/DominoDeviceADS52J90.h  \
        $$PWD/DominoDeviceFactory.h  \
        $$PWD/DominoDeviceHMCAD1101.h \
        $$PWD/DominoDeviceLtm9011.h \
        $$PWD/DominoDeviceRegisters.h\
        $$PWD/cellcalibration.h \
        $$PWD/device_features.h \
        $$PWD/dominodata.h \
        $$PWD/dominodevice.h  \
        $$PWD/dominosettings.h \


SOURCES += \
        $$PWD/AdcStatus.cpp \
        $$PWD/DominoDevice12EU050.cpp \
        $$PWD/DominoDeviceAD9249.cpp \
        $$PWD/DominoDeviceAD9252.cpp \
        $$PWD/DominoDeviceADS52J90.cpp  \
        $$PWD/DominoDeviceFactory.cpp  \
        $$PWD/DominoDeviceHMCAD1101.cpp \
        $$PWD/DominoDeviceLtm9011.cpp \
        $$PWD/cellcalibration.cpp \
        $$PWD/device_features.cpp \
        $$PWD/dominodata.cpp \
        $$PWD/dominodevice.cpp  \
        $$PWD/dominosettings.cpp \

