#include "DominoDeviceFactory.h"

#include "DominoDevice12EU050.h"
#include "DominoDeviceAD9249.h"
#include "DominoDeviceAD9252.h"
#include "DominoDeviceADS52J90.h"
#include "DominoDeviceHMCAD1101.h"
#include "DominoDeviceLtm9011.h"
#include "dominodevice.h"


DominoDevice *DominoDeviceFactory::create(const DeviceDescription &dd, QObject *parent)
{
    return create(dd.getIndex(), dd.hw_str, parent);
}

DominoDevice *DominoDeviceFactory::create(const DeviceIndex &index, const QString &hwStr, QObject *parent)
{
    const quint16 devId = index.getDevId();
    const quint32 devSerial = index.getSerial();

    DominoDevice * dev;
    switch (devId) {
    case DEVICE_ID_ADC64:
        dev = new DominoDevice12EU050(devId, devSerial, parent);
        break;
    case DEVICE_ID_ADC64S2:
    case DEVICE_ID_ADC64WR:
            dev = new DominoDeviceAD9249(devId, devSerial, parent);
            // dev = new DominoDeviceHMCAD1101(devId, devSerial, parent);
        break;
    case DEVICE_ID_ADC64VE2_XGE:
        dev = new DominoDeviceADS52J90(devId, devSerial, parent);
        break;
    case DEVICE_ID_ADC64VE_XGE:
        dev = new DominoDeviceAD9249(devId, devSerial, parent);
        break;
    case DEVICE_ID_ADC64ECAL:
        dev = new DominoDeviceADS52J90(devId, devSerial, parent);
        break;
    case DEVICE_ID_ADC64VE:
        dev = new DominoDeviceAD9249(devId, devSerial, parent);
        break;
    case DEVICE_ID_ADCM163:
        dev = new DominoDeviceAD9252(devId, devSerial, parent);
        break;
    case DEVICE_ID_TQDC16VS_DIG:
        dev = new DominoDeviceLtm9011(devId, devSerial, parent);
        break;
    default:
        dev = new DominoDevice(devId, devSerial, parent);
        break;
    }
    return dev;
}
