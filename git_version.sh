#!/bin/sh

if [ -n "$GIT_VERSION" ]; then
    echo "$GIT_VERSION"
    exit 0
fi

DIR=$(dirname $0)

git --git-dir $DIR/.git --work-tree $DIR describe --tags --match [0-9]*.[0-9]* --long --dirty --always 2>/dev/null
