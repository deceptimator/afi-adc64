#!/bin/bash

set -e

GIT_VERSION=$(sh git_version.sh)

VERSION=${GIT_VERSION%%-*}
RELEASE=${GIT_VERSION#*-}
RELEASE=${RELEASE%%-*}

for f in $@; do
    if [ -f "$f.in" ]; then
        sed -e " \
            s/@VERSION@/$VERSION/g; \
            s/@RELEASE@/$RELEASE/g; \
            s/@GIT_VERSION@/$GIT_VERSION/g; \
            "  "$f.in" > "$f"
    else
        echo "File not found: $f.in" >&2
    fi
done
