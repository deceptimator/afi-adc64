
NAME = afi-adc64

# make substitutions
$(shell sh preprocess.sh printrelease.sh printversion.sh)

# Project version
VERSION     ?= $(shell sh printversion.sh)
RELEASE     ?= $(shell sh printrelease.sh)
GIT_VERSION ?= $(shell sh git_version.sh)

NAME_VER := $(NAME)-$(VERSION)
SOURCE := $(NAME_VER).tar.gz

QMAKE := qmake-qt5

RPMBUILDDIR := rpms

PWD := $(shell pwd)

BUILDOPTS := $(RPMBUILDOPTS)

BUILDOPTS += --define "_topdir $(PWD)/rpms"
ifneq ($(GIT_VERSION),)
BUILDOPTS += --define "GIT_VERSION $(GIT_VERSION)"
endif

.PHONY: all
all:
	mkdir -p build
	cd build && [ -f Makefile ] || $(QMAKE) CONFIG+=release $(PWD)/src
	$(MAKE) $@ -C build

.PHONY: test-bin
test-bin:
	mkdir -p build/test
	cd build/test && [ -f Makefile ] || $(QMAKE) $(PWD)/src/test.pro
	$(MAKE) -C build/test

.PHONY: test
test: test-bin
	find build/test -executable -name '*_test' | while read f; do \
	    "$$f" -tickcounter -txt || true; \
	done

.PHONY: test-xml
test-xml: test-bin
	find build/test -executable -name '*_test' | while read f; do \
	    "$$f" -tickcounter -xml -o "$$f.xml"|| true; \
	done

.PHONY: test-xunit
test-xunit: test-bin
	find build/test -executable -name '*_test' | while read f; do \
	    "$$f" -tickcounter -xunitxml -o "$$f.xunit.xml"|| true; \
	done

.PHONY: clean
clean:
	rm -rf $(PWD)/build
	rm -rf $(PWD)/rpms
	rm -f packages/rpm/$(NAME).spec
	rm -f printrelease.sh printversion.sh

.PHONY: distclean
distclean: clean

install: all
	$(MAKE) $@ -C build

.PHONY: $(SOURCE)
$(SOURCE):
	mkdir -p build && cd build && rm -rf archive && mkdir archive
	git archive --format=tar --prefix=$(NAME_VER)/ HEAD | (cd build/archive/ && tar xf -)
	p=`pwd` && (echo .; git submodule foreach) | while read entering path; do \
	    temp="$${path%\'}"; temp="$${temp#\'}"; path=$$temp; \
	    [ "$$path" = "" ] && continue; \
	    (cd $$path && git archive --format=tar --prefix=$(NAME_VER)/$$path/ HEAD | (cd $$p/build/archive/ && tar xf -) ); \
	done
	rm -f $(NAME_VER).tar.gz
	(cd build/archive/ && tar -c $(NAME_VER) ) | gzip -c > $(NAME_VER).tar.gz
	rm -rf build/archive

release: $(SOURCE)

.PHONY: packages/rpm/$(NAME).spec
packages/rpm/$(NAME).spec: packages/rpm/$(NAME).spec.in
	$(shell sh preprocess.sh $@)

.PHONY: rpm
rpm: $(SOURCE) packages/rpm/$(NAME).spec
	mkdir -p $(RPMBUILDDIR)/BUILD/
	mkdir -p $(RPMBUILDDIR)/RPMS/$(shell uname -m)
	mkdir -p $(RPMBUILDDIR)/SOURCES/
	mkdir -p $(RPMBUILDDIR)/SPECS/
	mkdir -p $(RPMBUILDDIR)/SRPMS/
	cp -f packages/rpm/$(NAME).spec $(RPMBUILDDIR)/SPECS/
	cp -f $(SOURCE) $(RPMBUILDDIR)/SOURCES/
	rpmbuild $(BUILDOPTS) -ba $(RPMBUILDDIR)/SPECS/$(NAME).spec

.PHONY: deb
deb: distclean $(SOURCE)
	mv -f $(SOURCE) ../$(NAME)_$(VERSION).orig.tar.gz
	dpkg-buildpackage -rfakeroot

