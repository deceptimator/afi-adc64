PATH=%PATH%;C:\Qt\5.5\mingw492_32\bin
PATH=%PATH%;C:\Qt\5.6\mingw49_32\bin
PATH=%PATH%;C:\Qt\Tools\mingw492_32\bin

qmake -makefile ../../src/src.pro
if %errorlevel% neq 0 exit /b %errorlevel%

mingw32-make
if %errorlevel% neq 0 exit /b %errorlevel%

subwcrev ../../ adc64-setup.iss adc64-setup-svn.iss -ef
if %errorlevel% neq 0 exit /b %errorlevel%

"%ProgramFiles(x86)%\Inno Setup 5\ISCC.exe" adc64-setup-svn.iss
